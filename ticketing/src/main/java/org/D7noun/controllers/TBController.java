package org.D7noun.controllers;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.D7noun.dto.TB;
import org.D7noun.dto.TBReport;
import org.D7noun.facades.AccountTreeFacade;
import org.D7noun.facades.CompanyConfigurationFacade;
import org.D7noun.facades.VoucherDataFacade;
import org.D7noun.models.AccountTree;
import org.D7noun.models.CompanyConfiguration.CompanyConfigurationTypes;
import org.D7noun.models.VoucherData;
import org.D7noun.models.VoucherData.DC;
import org.D7noun.models.accounts.Account.Currency;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Ajax;
import org.omnifaces.util.Faces;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;

@Named
@ViewScoped
public class TBController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<TB> reportData = new ArrayList<TB>();

	@Inject
	private VoucherDataFacade voucherDataFacade;
	@Inject
	private AccountTreeFacade accountTreeFacade;
	@EJB
	private CompanyConfigurationFacade companyConfigurationFacade;

	private List<VoucherData> allVoucherData = new ArrayList<VoucherData>();
	private Map<String, TB> reportMap = new HashMap<>();
	private List<AccountTree> allAccountTree = new ArrayList<>();
	private Map<String, AccountTree> atHashMap = new HashMap<>();

	private Date fromDate;
	private Date toDate;
	private String fromType;
	private String toType;
	private Currency currency;

	private double debitTotal;
	private double creditTotal;

	@PostConstruct
	public void init() {
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.MONTH, 0);
			fromDate = calendar.getTime();
			toDate = new Date();
			allAccountTree = accountTreeFacade.findAll();
			allAccountTree.forEach(at -> atHashMap.put(at.getCode(), at));
			currency = Currency.DOLAR;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void search() {
		try {
			debitTotal = 0;
			creditTotal = 0;
			reportMap = new HashMap<>();
			reportData = new ArrayList<>();
			if (fromType != null && toType != null) {
				allVoucherData = voucherDataFacade.findBetweenTwoDatesAndTwoType(fromType, toType, fromDate, toDate);
			} else {
				allVoucherData = voucherDataFacade.findBetweenTwoDates(fromDate, toDate);
			}
			if (currency == Currency.DOLAR) {
				allVoucherData.forEach(vd -> {
					TB trialBalance = new TB();
					double debit = 0;
					double credit = 0;
					String type = vd.getAccount().getAccountNumber();
					if (reportMap.containsKey(type)) {
						trialBalance = reportMap.get(type);
						debit = trialBalance.getDebit();
						credit = trialBalance.getCredit();
					} else {
						trialBalance.setCode(vd.getAccount().getAccountNumber());
						trialBalance.setName(vd.getAccount().getName());
					}
					if (vd.getDc() == DC.D) {
						debit += vd.getUsdEquivalent();
						if (debit < 0) {
							debitTotal += (-1 * vd.getUsdEquivalent());
						} else {
							debitTotal += vd.getUsdEquivalent();
						}
					} else {
						credit += vd.getUsdEquivalent();
						if (debit < 0) {
							creditTotal += (-1 * vd.getUsdEquivalent());
						} else {
							creditTotal += vd.getUsdEquivalent();
						}
					}
					trialBalance.setDebit(debit);
					trialBalance.setCredit(credit);
					reportMap.put(vd.getAccount().getAccountNumber(), trialBalance);
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					while (type.length() > 0) {
						if (atHashMap.containsKey(type)) {
							AccountTree accountTree = atHashMap.get(type);
							TB trialBalance2 = new TB();
							double d2 = 0;
							double c2 = 0;
							if (reportMap.containsKey(type)) {
								trialBalance2 = reportMap.get(type);
								d2 = trialBalance2.getDebit();
								c2 = trialBalance2.getCredit();
							} else {
								trialBalance2 = new TB();
								trialBalance2.setCode(accountTree.getCode());
								trialBalance2.setName(accountTree.getName());
							}
							if (vd.getDc() == DC.D) {
								d2 += vd.getUsdEquivalent();
							} else {
								c2 += vd.getUsdEquivalent();
							}
							trialBalance2.setDebit(d2);
							trialBalance2.setCredit(c2);
							reportMap.put(type, trialBalance2);
						}
						type = removeLastCharacter(type);
					}
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
				});
			} else {

				allVoucherData.forEach(vd -> {
					TB trialBalance = new TB();
					double debit = 0;
					double credit = 0;
					String type = vd.getAccount().getAccountNumber();
					if (reportMap.containsKey(type)) {
						trialBalance = reportMap.get(type);
						debit = trialBalance.getDebit();
						credit = trialBalance.getCredit();
					} else {
						trialBalance.setCode(vd.getAccount().getAccountNumber());
						trialBalance.setName(vd.getAccount().getName());
					}
					if (vd.getDc() == DC.D) {
						debit += vd.getLlEquivalent();
						if (debit < 0) {
							debitTotal += (-1 * vd.getLlEquivalent());
						} else {
							debitTotal += vd.getLlEquivalent();
						}
					} else {
						credit += vd.getLlEquivalent();
						if (debit < 0) {
							creditTotal += (-1 * vd.getLlEquivalent());
						} else {
							creditTotal += vd.getLlEquivalent();
						}
					}
					trialBalance.setDebit(debit);
					trialBalance.setCredit(credit);
					reportMap.put(vd.getAccount().getAccountNumber(), trialBalance);
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					while (type.length() > 0) {
						if (atHashMap.containsKey(type)) {
							AccountTree accountTree = atHashMap.get(type);
							TB trialBalance2 = new TB();
							double d2 = 0;
							double c2 = 0;
							if (reportMap.containsKey(type)) {
								trialBalance2 = reportMap.get(type);
								d2 = trialBalance2.getDebit();
								c2 = trialBalance2.getCredit();
							} else {
								trialBalance2 = new TB();
								trialBalance2.setCode(accountTree.getCode());
								trialBalance2.setName(accountTree.getName());
							}
							if (vd.getDc() == DC.D) {
								d2 += vd.getLlEquivalent();
							} else {
								c2 += vd.getLlEquivalent();
							}
							trialBalance2.setDebit(d2);
							trialBalance2.setCredit(c2);
							reportMap.put(type, trialBalance2);
						}
						type = removeLastCharacter(type);
					}
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
				});

			}
			reportData = new ArrayList<TB>(reportMap.values());
			Collections.sort(reportData, new Comparator<TB>() {
				@Override
				public int compare(TB o1, TB o2) {
					return o1.getCode().compareTo(o2.getCode());
				}
			});
			Ajax.oncomplete("PF('itemVar').filter()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void exportToExcel() {

		try {

			// Create a Workbook
			XSSFWorkbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for
			// generating `.xls` file

			// Create a Sheet
			XSSFSheet sheet = workbook.createSheet("Trial Balance");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 5));
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 5));
			sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, 5));
			int rowNum = 0;
			////////////////////////
			////////////////////////
			////////////////////////
			String companyName = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.CompanyDescription);
			XSSFFont companyNameFont = workbook.createFont();
			companyNameFont.setFontHeightInPoints((short) 20);
			XSSFCellStyle companyCellStyle = workbook.createCellStyle();
			companyCellStyle.setFont(companyNameFont);
			companyCellStyle.setAlignment(HorizontalAlignment.CENTER);
			companyCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
			companyCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			Row companyRow = sheet.createRow(rowNum);
			rowNum++;
			Cell companyCell = companyRow.createCell(0);
			companyCell.setCellStyle(companyCellStyle);
			companyCell.setCellValue(companyName);
			////////////////////////
			////////////////////////
			////////////////////////
			String reportName = "TRAIL BALANCE REPORT / ";
			if (currency == Currency.DOLAR) {
				reportName += "USD";
			} else {
				reportName += "L.L.";
			}
			XSSFFont reportNameFont = workbook.createFont();
			reportNameFont.setFontHeightInPoints((short) 20);
			XSSFCellStyle reportCellStyle = workbook.createCellStyle();
			reportCellStyle.setFont(reportNameFont);
			reportCellStyle.setAlignment(HorizontalAlignment.CENTER);
			Row reportRow = sheet.createRow(rowNum);
			rowNum++;
			Cell reportCell = reportRow.createCell(0);
			reportCell.setCellStyle(reportCellStyle);
			reportCell.setCellValue(reportName);
			////////////////////////
			////////////////////////
			////////////////////////
			String difAccount = "[FROM ACNT " + (fromType == null ? "1" : fromType) + " TO "
					+ (toType == null ? "99999" : toType) + "]";
			String space = "               ";
			String difDate = "[FROM " + (fromDate == null ? "NO FROM DATE" : getStringDateFromDate(fromDate)) + " TO "
					+ (toDate == null ? "NO TO DATE" : getStringDateFromDate(toDate)) + "]";
			String dif = difAccount + space + difDate;
			XSSFFont difFont = workbook.createFont();
			difFont.setFontHeightInPoints((short) 18);
			XSSFCellStyle difCellStyle = workbook.createCellStyle();
			difCellStyle.setFont(reportNameFont);
			difCellStyle.setAlignment(HorizontalAlignment.CENTER);
			Row difRow = sheet.createRow(rowNum);
			rowNum++;
			Cell difCell = difRow.createCell(0);
			difCell.setCellStyle(difCellStyle);
			difCell.setCellValue(dif);
			////////////////////////
			////////////////////////
			////////////////////////

			// Create a Font for styling header cells
			rowNum++;
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 18);
			headerFont.setColor(IndexedColors.RED.getIndex());
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			Row headerRow = sheet.createRow(rowNum);
			rowNum++;
			Cell headerCode = headerRow.createCell(0);
			Cell headerName = headerRow.createCell(1);
			Cell headerDebit = headerRow.createCell(2);
			Cell headerCredit = headerRow.createCell(3);
			Cell headerBalance = headerRow.createCell(4);
			Cell headerCRDR = headerRow.createCell(5);

			headerCode.setCellValue("Code");
			headerName.setCellValue("Name");
			headerDebit.setCellValue("Debit");
			headerCredit.setCellValue("Credit");
			headerBalance.setCellValue("Balance");
			headerCRDR.setCellValue("DC");

			headerCode.setCellStyle(headerCellStyle);
			headerName.setCellStyle(headerCellStyle);
			headerDebit.setCellStyle(headerCellStyle);
			headerCredit.setCellStyle(headerCellStyle);
			headerBalance.setCellStyle(headerCellStyle);
			headerCRDR.setCellStyle(headerCellStyle);
			headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
			////////////////////////
			////////////////////////
			////////////////////////
			// Create Other rows and cells with data

			XSSFFont dataFont = workbook.createFont();
			dataFont.setFontHeightInPoints((short) 14);
			XSSFCellStyle dataStyle = workbook.createCellStyle();
			dataStyle.setAlignment(HorizontalAlignment.LEFT);
			dataStyle.setFont(dataFont);
			for (TB trialBalance : reportData) {
				Row row = sheet.createRow(rowNum++);

				Cell code = row.createCell(0);
				Cell name = row.createCell(1);
				Cell debit = row.createCell(2);
				Cell credit = row.createCell(3);
				Cell balance = row.createCell(4);
				Cell crdr = row.createCell(5);
				code.setCellStyle(dataStyle);
				name.setCellStyle(dataStyle);
				debit.setCellStyle(dataStyle);
				credit.setCellStyle(dataStyle);
				balance.setCellStyle(dataStyle);
				crdr.setCellStyle(dataStyle);

				code.setCellValue(trialBalance.getCode());
				name.setCellValue(trialBalance.getName());
				debit.setCellValue(trialBalance.getDebitStr());
				credit.setCellValue(trialBalance.getCreditStr());
				String bComma = trialBalance.getBalanceStr();
				balance.setCellValue(bComma);
				crdr.setCellValue(trialBalance.getCrDr());

			}
			Row row = sheet.createRow(rowNum++);

			Cell code = row.createCell(0);
			Cell name = row.createCell(1);
			Cell debit = row.createCell(2);
			Cell credit = row.createCell(3);
			Cell balance = row.createCell(4);
			Cell crdr = row.createCell(5);
			code.setCellStyle(dataStyle);
			name.setCellStyle(dataStyle);
			debit.setCellStyle(dataStyle);
			credit.setCellStyle(dataStyle);
			balance.setCellStyle(dataStyle);
			crdr.setCellStyle(dataStyle);

			code.setCellValue("TT");
			name.setCellValue("**GENERAL TOTALS**");
			debit.setCellValue(getDebitTotalStr());
			credit.setCellValue(getCreditTotalStr());
			balance.setCellValue(getBalanceStr());
			crdr.setCellValue(getCrDr());

			// Resize all columns to fit the content size
			for (int i = 0; i < reportData.size(); i++) {
				sheet.autoSizeColumn(i);
			}

//			sheet.setRightToLeft(true);

			// Write the output to a file
			ByteArrayOutputStream fileOut = new ByteArrayOutputStream();
			workbook.write(fileOut);

			Faces.sendFile(fileOut.toByteArray(), "trial_balance.xlsx", true);

			fileOut.close();
			// Closing the workbook
			workbook.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: exportToExcel");
		}

	}

	public void printOrder() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			ServletOutputStream servletOutputStream = response.getOutputStream();

			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			InputStream is = classloader.getResourceAsStream("trial-balance.jrxml");
			JasperReport report = JasperCompileManager.compileReport(is);

			Map<String, Object> parameters = new HashMap<>();

			List<TBReport> dtos = new ArrayList<TBReport>();
			for (TB tb : reportData) {
				TBReport tbReport = new TBReport();
				tbReport.setCode(tb.getCode());
				tbReport.setName(tb.getName());
				tbReport.setDebit(tb.getDebitStr());
				tbReport.setCredit(tb.getCreditStr());
				tbReport.setBalance(tb.getBalanceStr());
				tbReport.setCrdr(tb.getCrDr());
				dtos.add(tbReport);
			}
			dtos.add(new TBReport("T", "", "", "", "", ""));
			TBReport tbReport = new TBReport();
			tbReport.setCode("TT");
			tbReport.setName("**GENERAL TOTALS**");
			tbReport.setDebit(getDebitTotalStr());
			tbReport.setCredit(getCreditTotalStr());
			tbReport.setBalance(getBalanceStr());
			tbReport.setCrdr(getCrDr());
			dtos.add(tbReport);

			Calendar cFromDate = Calendar.getInstance();
			cFromDate.setTime(fromDate);
			Calendar cToDate = Calendar.getInstance();
			cToDate.setTime(toDate);
			Calendar cDate = Calendar.getInstance();
			cDate.setTime(new Date());

			String currencyandyear = "";
			if (currency == Currency.DOLAR) {
				currencyandyear += "$ / ";
			} else {
				currencyandyear += "L.L / ";
			}
			currencyandyear += cFromDate.get(Calendar.YEAR);

			parameters.put("companyName",
					companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.CompanyDescription));
			parameters.put("currencyandyear", currencyandyear);
			parameters.put("datas", dtos);
			if (fromType == null || toType == null) {
				parameters.put("fromAccount", "1");
				parameters.put("toAccount", "99999/9999/9");
			} else {
				parameters.put("fromAccount", fromType);
				parameters.put("toAccount", toType);
			}
			parameters.put("fromDate", getStringDateFromDate(fromDate));
			parameters.put("toDate", getStringDateFromDate(toDate));
			parameters.put("date", getStringDateFromDate(new Date()));
			parameters.put("time", getTimeDateFromDate(new Date()));

			byte[] bytes = null;
			bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource(1));

			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);

			servletOutputStream.write(bytes, 0, bytes.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the reportData
	 */
	public List<TB> getReportData() {
		return reportData;
	}

	/**
	 * @param reportData the reportData to set
	 */
	public void setReportData(List<TB> reportData) {
		this.reportData = reportData;
	}

	/**
	 * @return the allVoucherData
	 */
	public List<VoucherData> getAllVoucherData() {
		return allVoucherData;
	}

	/**
	 * @param allVoucherData the allVoucherData to set
	 */
	public void setAllVoucherData(List<VoucherData> allVoucherData) {
		this.allVoucherData = allVoucherData;
	}

	private String removeLastCharacter(String str) {
		if (str != null && str.length() > 0) {
			str = str.substring(0, str.length() - 1);
		}
		return str;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	private String getStringDateFromDate(Date date) {
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(date);
	}

	private String getTimeDateFromDate(Date date) {
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("hh:mm:ss").format(date);
	}

	public Map<String, TB> getReportMap() {
		return reportMap;
	}

	public void setReportMap(Map<String, TB> reportMap) {
		this.reportMap = reportMap;
	}

	public Map<String, AccountTree> getAtHashMap() {
		return atHashMap;
	}

	public void setAtHashMap(Map<String, AccountTree> atHashMap) {
		this.atHashMap = atHashMap;
	}

	public String getFromType() {
		return fromType;
	}

	public void setFromType(String fromType) {
		this.fromType = fromType;
	}

	public String getToType() {
		return toType;
	}

	public void setToType(String toType) {
		this.toType = toType;
	}

	public List<AccountTree> getAllAccountTree() {
		return allAccountTree;
	}

	public void setAllAccountTree(List<AccountTree> allAccountTree) {
		this.allAccountTree = allAccountTree;
	}

	public double getDebitTotal() {
		return debitTotal;
	}

	public String getDebitTotalStr() {
		DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
		return decimalFormat.format(getDebitTotal());
	}

	public double getCreditTotal() {
		return creditTotal;
	}

	public String getCreditTotalStr() {
		DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
		return decimalFormat.format(getCreditTotal());
	}

	public double getBalance() {
		return getDebitTotal() - getCreditTotal();
	}

	public String getBalanceStr() {
		double balance = getBalance();
		if (balance <= 0) {
			balance = -1 * balance;
		}
		DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
		return decimalFormat.format(balance);
	}

	public String getCrDr() {
		if (getBalance() >= 0) {
			return "DR";
		}
		return "CR";
	}

}
