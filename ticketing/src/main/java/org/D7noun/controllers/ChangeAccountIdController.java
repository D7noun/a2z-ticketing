package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.AccountFacade;
import org.D7noun.models.accounts.Account;
import org.D7noun.models.accounts.Customer;
import org.D7noun.service.ChangeAccountIdService;
import org.omnifaces.util.Ajax;

@ManagedBean
@ViewScoped
public class ChangeAccountIdController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private AccountFacade accountFacade;
	@EJB
	private ChangeAccountIdService changeAccountIdService;
	private List<Account> allAccounts = new ArrayList<Account>();
	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	private Account selectedAccount;
	private long selectedAccountId;
	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////

	@PostConstruct
	public void init() {
		try {
			allAccounts = accountFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("ChangeAccountId.init");
		}
	}

	public void onDialogOpen(Account account) {
		selectedAccount = account;
		if (selectedAccount != null) {
			selectedAccountId = selectedAccount.getAccountId();
		}
		Ajax.update("accountDialog-panelGroup");
		Ajax.oncomplete("PF('accountDialogVar').show()");
	}

	public void onDialogClose() {
		if (selectedAccount != null && selectedAccountId != 0) {
			Account reserved = accountFacade.getAccountByAccountIdAndCodeAndCurrency(selectedAccount.getType(),
					selectedAccountId, selectedAccount.getCurrency());
			if (reserved == null) {
				selectedAccount.setAccountId(selectedAccountId);
				selectedAccount = accountFacade.save(selectedAccount);
				Ajax.oncomplete("PF('itemVar').filter()");
				Ajax.oncomplete("PF('accountDialogVar').hide()");
			} else {
				GeneralUtility.showWarningMessage("reserved_account_id");
				long accountId = accountFacade.getMaxAccountIdByCode(selectedAccount.getType());
				boolean reservedId = true;
				while (reservedId) {
					Account checkAccount = (Customer) accountFacade.getAccountByAccountIdAndCodeAndCurrency(
							selectedAccount.getType(), ++accountId, selectedAccount.getCurrency());
					if (checkAccount == null) {
						reservedId = false;
					}
				}
				selectedAccountId = accountId;
				Ajax.update("growl");
				Ajax.update("accountDialog-panelGroup");
			}
		}
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */
	/**
	 * @return the accountFacade
	 */
	public AccountFacade getAccountFacade() {
		return accountFacade;
	}

	/**
	 * @param accountFacade the accountFacade to set
	 */
	public void setAccountFacade(AccountFacade accountFacade) {
		this.accountFacade = accountFacade;
	}

	/**
	 * @return the allAccounts
	 */
	public List<Account> getAllAccounts() {
		return allAccounts;
	}

	/**
	 * @param allAccounts the allAccounts to set
	 */
	public void setAllAccounts(List<Account> allAccounts) {
		this.allAccounts = allAccounts;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the selectedAccount
	 */
	public Account getSelectedAccount() {
		return selectedAccount;
	}

	/**
	 * @param selectedAccount the selectedAccount to set
	 */
	public void setSelectedAccount(Account selectedAccount) {
		this.selectedAccount = selectedAccount;
	}

	/**
	 * @return the selectedAccountId
	 */
	public long getSelectedAccountId() {
		return selectedAccountId;
	}

	/**
	 * @param selectedAccountId the selectedAccountId to set
	 */
	public void setSelectedAccountId(long selectedAccountId) {
		this.selectedAccountId = selectedAccountId;
	}

}
