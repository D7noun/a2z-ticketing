package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.AccountConfigurationFacade;
import org.D7noun.facades.AccountTreeFacade;
import org.D7noun.models.AccountConfiguration;
import org.D7noun.models.AccountTree;
import org.omnifaces.util.Faces;

@ManagedBean
@ViewScoped
public class AccountConfigurationController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private AccountConfigurationFacade accountConfigurationFacade;
	@EJB
	private AccountTreeFacade accountTreeFacade;

	private List<AccountConfiguration> allAccountConfigurations = new ArrayList<AccountConfiguration>();
	private List<AccountTree> allAccountTrees = new ArrayList<AccountTree>();

	private boolean canEdit = true;

	@PostConstruct
	public void init() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountConfigurationController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				allAccountTrees = accountTreeFacade.findAll();
				allAccountConfigurations = accountConfigurationFacade.findAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountConfigurationController.preRenderView");
		}
	}

	public void save() {
		try {
			accountConfigurationFacade.save(allAccountConfigurations);
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			GeneralUtility.showErrorMessage(GeneralUtility.getResourceBundleString("save_error"));
			e.printStackTrace();
			System.err.println("D7noun: AccountConfigurationController.save");
		}
	}

	/**
	 * @return the accountConfigurationFacade
	 */
	public AccountConfigurationFacade getAccountConfigurationFacade() {
		return accountConfigurationFacade;
	}

	/**
	 * @param accountConfigurationFacade the accountConfigurationFacade to set
	 */
	public void setAccountConfigurationFacade(AccountConfigurationFacade accountConfigurationFacade) {
		this.accountConfigurationFacade = accountConfigurationFacade;
	}

	/**
	 * @return the accountTreeFacade
	 */
	public AccountTreeFacade getAccountTreeFacade() {
		return accountTreeFacade;
	}

	/**
	 * @param accountTreeFacade the accountTreeFacade to set
	 */
	public void setAccountTreeFacade(AccountTreeFacade accountTreeFacade) {
		this.accountTreeFacade = accountTreeFacade;
	}

	/**
	 * @return the allAccountConfigurations
	 */
	public List<AccountConfiguration> getAllAccountConfigurations() {
		return allAccountConfigurations;
	}

	/**
	 * @param allAccountConfigurations the allAccountConfigurations to set
	 */
	public void setAllAccountConfigurations(List<AccountConfiguration> allAccountConfigurations) {
		this.allAccountConfigurations = allAccountConfigurations;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allAccountTrees
	 */
	public List<AccountTree> getAllAccountTrees() {
		return allAccountTrees;
	}

	/**
	 * @param allAccountTrees the allAccountTrees to set
	 */
	public void setAllAccountTrees(List<AccountTree> allAccountTrees) {
		this.allAccountTrees = allAccountTrees;
	}

}