package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.AccountFacade;
import org.D7noun.facades.AccountSerialFacade;
import org.D7noun.facades.AccountTreeFacade;
import org.D7noun.facades.GeneralFacade;
import org.D7noun.models.AccountSerial;
import org.D7noun.models.AccountTree;
import org.D7noun.models.accounts.General;
import org.omnifaces.util.Faces;

@ManagedBean
@ViewScoped
public class GeneralController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private GeneralFacade generalFacade;
	@EJB
	private AccountTreeFacade accountTreeFacade;
	@EJB
	private AccountFacade accountFacade;
	@EJB
	private AccountSerialFacade accountSerialFacade;

	private AccountTree selectedAccountTree;
	private List<AccountTree> allAccountTrees = new ArrayList<AccountTree>();

	private General general;

	private String id;

	private boolean canEdit = true;

	@PostConstruct
	public void init() {
		try {
			allAccountTrees = accountTreeFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: GeneralController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				if (id != null) {
					general = generalFacade.find(Long.parseLong(id));
				} else {
					general = new General();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: GeneralController.preRenderView");
		}
	}

	public void save() {
		try {
			boolean redirect = false;
			/////////////////////////
			/////////////////////////
			/////////////////////////
			/////////////////////////
			if (general.getId() == null || general.getId() <= 0) {
				if (selectedAccountTree != null) {
					redirect = true;
					general.setType(selectedAccountTree.getCode());
					AccountSerial accountSerial = accountSerialFacade
							.getByAccountCodeAndCurrency(selectedAccountTree.getCode(), general.getCurrency());

					long accountId = 0;
					if (accountSerial == null) {
						accountSerial = new AccountSerial();
						accountSerial.setAccountCode(selectedAccountTree.getCode());
						accountSerial.setCurrency(general.getCurrency());
					} else {
						accountId = accountSerial.getValue();
					}
					boolean reservedId = true;
					while (reservedId) {
						General reserved = (General) accountFacade.getAccountByAccountIdAndCodeAndCurrency(
								selectedAccountTree.getCode(), ++accountId, general.getCurrency());
						if (reserved == null) {
							reservedId = false;
						}
					}
					accountSerial.setValue(accountId);
					general.setAccountId(accountId);
					accountSerial = accountSerialFacade.save(accountSerial);

				}
			}
			general = generalFacade.save(general);

			if (redirect) {
				Faces.redirect(GeneralUtility.getOutcomeURL("general-card") + "?id=%s", Long.toString(general.getId()));
			}
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: GeneralController.save");
		}
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the generalFacade
	 */
	public GeneralFacade getGeneralFacade() {
		return generalFacade;
	}

	/**
	 * @param generalFacade the generalFacade to set
	 */
	public void setGeneralFacade(GeneralFacade generalFacade) {
		this.generalFacade = generalFacade;
	}

	/**
	 * @return the general
	 */
	public General getGeneral() {
		return general;
	}

	/**
	 * @param general the general to set
	 */
	public void setGeneral(General general) {
		this.general = general;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the accountTreeFacade
	 */
	public AccountTreeFacade getAccountTreeFacade() {
		return accountTreeFacade;
	}

	/**
	 * @param accountTreeFacade the accountTreeFacade to set
	 */
	public void setAccountTreeFacade(AccountTreeFacade accountTreeFacade) {
		this.accountTreeFacade = accountTreeFacade;
	}

	/**
	 * @return the accountFacade
	 */
	public AccountFacade getAccountFacade() {
		return accountFacade;
	}

	/**
	 * @param accountFacade the accountFacade to set
	 */
	public void setAccountFacade(AccountFacade accountFacade) {
		this.accountFacade = accountFacade;
	}

	/**
	 * @return the accountSerialFacade
	 */
	public AccountSerialFacade getAccountSerialFacade() {
		return accountSerialFacade;
	}

	/**
	 * @param accountSerialFacade the accountSerialFacade to set
	 */
	public void setAccountSerialFacade(AccountSerialFacade accountSerialFacade) {
		this.accountSerialFacade = accountSerialFacade;
	}

	/**
	 * @return the allAccountTrees
	 */
	public List<AccountTree> getAllAccountTrees() {
		return allAccountTrees;
	}

	/**
	 * @param allAccountTrees the allAccountTrees to set
	 */
	public void setAllAccountTrees(List<AccountTree> allAccountTrees) {
		this.allAccountTrees = allAccountTrees;
	}

	/**
	 * @return the selectedAccountTree
	 */
	public AccountTree getSelectedAccountTree() {
		return selectedAccountTree;
	}

	/**
	 * @param selectedAccountTree the selectedAccountTree to set
	 */
	public void setSelectedAccountTree(AccountTree selectedAccountTree) {
		this.selectedAccountTree = selectedAccountTree;
	}

}
