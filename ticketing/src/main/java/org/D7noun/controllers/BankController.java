package org.D7noun.controllers;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.AccountConfigurationFacade;
import org.D7noun.facades.AccountFacade;
import org.D7noun.facades.AccountSerialFacade;
import org.D7noun.facades.BankFacade;
import org.D7noun.models.AccountConfiguration;
import org.D7noun.models.AccountConfiguration.AccountTypes;
import org.D7noun.models.AccountSerial;
import org.D7noun.models.AccountTree;
import org.D7noun.models.accounts.Bank;
import org.omnifaces.util.Faces;

@ManagedBean
@ViewScoped
public class BankController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private BankFacade bankFacade;
	@EJB
	private AccountConfigurationFacade accountConfigurationFacade;
	@EJB
	private AccountFacade accountFacade;
	@EJB
	private AccountSerialFacade accountSerialFacade;

	private Bank bank;

	private String id;

	private boolean canEdit = true;

	@PostConstruct
	public void init() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: BankController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				if (id != null) {
					bank = bankFacade.find(Long.parseLong(id));
				} else {
					bank = new Bank();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: BankController.preRenderView");
		}
	}

	public void save() {
		try {
			boolean redirect = false;
			/////////////////////////
			/////////////////////////
			AccountConfiguration accountConfiguration = accountConfigurationFacade
					.getAccountConfigurationByAccountType(AccountTypes.Banks.toString());
			AccountTree accountTree = accountConfiguration.getAccountTree();
			bank.setType(accountTree.getCode());
			/////////////////////////
			/////////////////////////
			if (bank.getId() == null || bank.getId() <= 0) {
				redirect = true;
				AccountSerial accountSerial = accountSerialFacade.getByAccountCodeAndCurrency(accountTree.getCode(),
						bank.getCurrency());

				long accountId = 0;
				if (accountSerial == null) {
					accountSerial = new AccountSerial();
					accountSerial.setAccountCode(accountTree.getCode());
					accountSerial.setCurrency(bank.getCurrency());
				} else {
					accountId = accountSerial.getValue();
				}
				boolean reservedId = true;
				while (reservedId) {
					Bank reserved = (Bank) accountFacade.getAccountByAccountIdAndCodeAndCurrency(accountTree.getCode(),
							++accountId, bank.getCurrency());
					if (reserved == null) {
						reservedId = false;
					}
				}
				accountSerial.setValue(accountId);
				bank.setAccountId(accountId);
				accountSerial = accountSerialFacade.save(accountSerial);
			}
			bank = bankFacade.save(bank);

			if (redirect) {
				Faces.redirect(GeneralUtility.getOutcomeURL("bank-card") + "?id=%s", Long.toString(bank.getId()));
			}
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: BankController.save");
		}
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the bankFacade
	 */
	public BankFacade getBankFacade() {
		return bankFacade;
	}

	/**
	 * @param bankFacade the bankFacade to set
	 */
	public void setBankFacade(BankFacade bankFacade) {
		this.bankFacade = bankFacade;
	}

	/**
	 * @return the bank
	 */
	public Bank getBank() {
		return bank;
	}

	/**
	 * @param bank the bank to set
	 */
	public void setBank(Bank bank) {
		this.bank = bank;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

}
