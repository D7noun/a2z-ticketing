package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.AccountTreeFacade;
import org.D7noun.models.AccountTree;
import org.omnifaces.util.Ajax;

@ManagedBean
@ViewScoped
public class AccountTreeController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private AccountTreeFacade accountTreeFacade;
	private List<AccountTree> allAccountTrees = new ArrayList<AccountTree>();
	private AccountTree newAccountTree = new AccountTree();
	////////////////////////////////////////

	private boolean canEdit;

	@PostConstruct
	public void init() {
		try {
			canEdit = true;
			allAccountTrees = accountTreeFacade.findAll();
			Collections.sort(allAccountTrees, new Comparator<AccountTree>() {
				@Override
				public int compare(AccountTree o1, AccountTree o2) {
					return o1.getCode().compareTo(o2.getCode());
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addNewAccountTree() {
		AccountTree accountTree = new AccountTree();
		allAccountTrees.add(accountTree);
		Ajax.oncomplete("PF('itemVar').filter()");
	}

	public void deleteAccountTree(AccountTree accountTree) {
		try {
			accountTreeFacade.remove(accountTree);
			allAccountTrees.remove(accountTree);
		} catch (Exception e) {
			GeneralUtility.showErrorMessage("value_connected_to_configuration");
			Ajax.update("growl");
			e.printStackTrace();
		}
	}

	public void save() {
		try {
			accountTreeFacade.save(allAccountTrees);
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			GeneralUtility.showErrorMessage("save_error");
			e.printStackTrace();
			System.err.println("D7noun: AccountTreeController.save");
		}
	}

	public void importFromExcel() {

	}

	/**
	 * @return the accountTreeFacade
	 */
	public AccountTreeFacade getAccountTreeFacade() {
		return accountTreeFacade;
	}

	/**
	 * @param accountTreeFacade the accountTreeFacade to set
	 */
	public void setAccountTreeFacade(AccountTreeFacade accountTreeFacade) {
		this.accountTreeFacade = accountTreeFacade;
	}

	/**
	 * @return the allAccountTrees
	 */
	public List<AccountTree> getAllAccountTrees() {
		return allAccountTrees;
	}

	/**
	 * @param allAccountTrees the allAccountTrees to set
	 */
	public void setAllAccountTrees(List<AccountTree> allAccountTrees) {
		this.allAccountTrees = allAccountTrees;
	}

	/**
	 * @return the newAccountTree
	 */
	public AccountTree getNewAccountTree() {
		return newAccountTree;
	}

	/**
	 * @param newAccountTree the newAccountTree to set
	 */
	public void setNewAccountTree(AccountTree newAccountTree) {
		this.newAccountTree = newAccountTree;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
