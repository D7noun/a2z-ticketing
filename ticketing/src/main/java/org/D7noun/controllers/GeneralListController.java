package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.D7noun.facades.GeneralFacade;
import org.D7noun.models.accounts.General;

@Named
@ViewScoped
public class GeneralListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private GeneralFacade generalFacade;
	private List<General> allGenerals = new ArrayList<General>();

	///////////////////////////
	///////////////////////////
	///////////////////////////
	private General generalForDelete;
	///////////////////////////
	///////////////////////////
	///////////////////////////

	@PostConstruct
	public void init() {
		try {
			allGenerals = generalFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: GeneralListController.init");
		}
	}

	public void delete(General general) {
		generalFacade.remove(general);
		allGenerals = generalFacade.findAll();
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the generalFacade
	 */
	public GeneralFacade getGeneralFacade() {
		return generalFacade;
	}

	/**
	 * @param generalFacade the generalFacade to set
	 */
	public void setGeneralFacade(GeneralFacade generalFacade) {
		this.generalFacade = generalFacade;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allGenerals
	 */
	public List<General> getAllGenerals() {
		return allGenerals;
	}

	/**
	 * @param allGenerals the allGenerals to set
	 */
	public void setAllGenerals(List<General> allGenerals) {
		this.allGenerals = allGenerals;
	}

	/**
	 * @return the generalForDelete
	 */
	public General getGeneralForDelete() {
		return generalForDelete;
	}

	/**
	 * @param generalForDelete the generalForDelete to set
	 */
	public void setGeneralForDelete(General generalForDelete) {
		this.generalForDelete = generalForDelete;
	}

}
