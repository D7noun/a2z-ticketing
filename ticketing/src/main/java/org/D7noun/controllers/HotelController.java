package org.D7noun.controllers;

import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.dto.ItemDto;
import org.D7noun.facades.CompanyConfigurationFacade;
import org.D7noun.facades.CurrencyConfigurationFacade;
import org.D7noun.facades.CustomerFacade;
import org.D7noun.facades.HotelFacade;
import org.D7noun.facades.SalesPurchaseFacade;
import org.D7noun.facades.SupplierFacade;
import org.D7noun.facades.VoucherDataFacade;
import org.D7noun.facades.VoucherFacade;
import org.D7noun.models.SalesPurchase;
import org.D7noun.models.SalesPurchase.SP;
import org.D7noun.models.Voucher;
import org.D7noun.models.Voucher.VoucherType;
import org.D7noun.models.VoucherData;
import org.D7noun.models.CompanyConfiguration.CompanyConfigurationTypes;
import org.D7noun.models.VoucherData.DC;
import org.D7noun.models.accounts.Account;
import org.D7noun.models.accounts.Account.Currency;
import org.D7noun.models.accounts.Customer;
import org.D7noun.models.accounts.Supplier;
import org.D7noun.models.items.Hotel;
import org.D7noun.service.CashWordConverter;
import org.omnifaces.util.Faces;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;

@ManagedBean
@ViewScoped
public class HotelController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private HotelFacade hotelFacade;
	@EJB
	private CustomerFacade customerFacade;
	@EJB
	private SupplierFacade supplierFacade;
	@EJB
	private VoucherFacade voucherFacade;
	@EJB
	private VoucherDataFacade voucherDataFacade;
	@EJB
	private SalesPurchaseFacade salesPurchaseFacade;
	@EJB
	private CurrencyConfigurationFacade currencyConfigurationFacade;
	@EJB
	private CompanyConfigurationFacade companyConfigurationFacade;
	///////////////////////////////////////
	///////////////////////////////////////
//	private int 1507.5;
	//////////////////////////////////
	//////////////////////////////////

	private List<Customer> allCustomers = new ArrayList<Customer>();
	private List<Supplier> allSuppliers = new ArrayList<Supplier>();

	///////////////////////////////////////
	///////////////////////////////////////
	private Hotel hotel;

	private String id;

	private boolean canEdit = true;

	@PostConstruct
	public void init() {
		try {
			allCustomers = customerFacade.findAll();
			allSuppliers = supplierFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: HotelController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				if (id != null) {
					hotel = hotelFacade.find(Long.parseLong(id));
				} else {
					hotel = new Hotel();
					hotel.setQuantity(1);
				}
//				1507.5 = currencyConfigurationFacade.getLiraByDate(new Date());
//				if (1507.5 == 0) {
//					1507.5 = 1507;
//				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: HotelController.preRenderView");
		}
	}

	public void updateFromDiscountAmount() {
		double total = (hotel.getSelling() - hotel.getDiscountAmount()) * hotel.getQuantity();
		int dicountPercentage = 100 - ((int) ((total * 100) / hotel.getSelling()));
		hotel.setTotal(total);
		hotel.setDiscountPercentage(dicountPercentage);
	}

	public void updateSellingAndTotal() {
		double selling = (hotel.getCost() + hotel.getProfit() + hotel.getTax()) * hotel.getQuantity();
		double total = (selling * (100 - hotel.getDiscountPercentage())) / 100;
		double discountAmount = total - selling;
		hotel.setDiscountAmount(discountAmount * -1);
		hotel.setSelling(selling);
		hotel.setTotal(total);
	}

	public void save() {
		try {
			boolean redirect = false;
			Voucher voucher = new Voucher();
			List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
			if (hotel.getId() == null || hotel.getId() <= 0) {
				redirect = true;
				///////////////////////////////
				voucher.setVoucherType(VoucherType.HS);
				voucher.setVoucherDate(hotel.getDate());
				long maxVoucherNumber = voucherFacade.getMaxVoucherNumberByType(voucher.getVoucherType());
				voucher.setVoucherNumber(++maxVoucherNumber);
				voucherDatas = initVoucherDatas(hotel);
				if (voucherDatas != null && !voucherDatas.isEmpty()) {
					for (VoucherData voucherData : voucherDatas) {
						voucher.addNewVoucherData(voucherData);
					}
					voucher.addNewItem(hotel);
				}
				voucher = voucherFacade.save(voucher);
			} else {
				voucher = voucherFacade.findVoucherByItem(hotel);
				List<VoucherData> deletedData = voucherDataFacade.findVoucherDataByItem(hotel);
				voucher.deleteVoucherData(deletedData);
				voucher.setVoucherDate(hotel.getDate());
				voucherDatas = initVoucherDatas(hotel);
				for (VoucherData voucherData : voucherDatas) {
					voucher.addNewVoucherData(voucherData);
				}
				voucher = voucherFacade.save(voucher);
				hotel = hotelFacade.save(hotel);
			}
			if (redirect) {
				Faces.redirect(GeneralUtility.getOutcomeURL("hotel-list"));
			}
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			e.printStackTrace();
			GeneralUtility.showErrorMessage(e);
			System.err.println("D7noun: HotelController.save");
		}
	}

	public List<VoucherData> initVoucherDatas(Hotel hotel) {
		List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
		try {

			double totalCustomer = hotel.getTotal();
			double totalSupplier = (hotel.getSelling() - hotel.getProfit() * hotel.getQuantity());
			VoucherData firstVoucherData = new VoucherData();
			VoucherData secondVoucherData = new VoucherData();
			VoucherData thirdVoucherData = new VoucherData();
			VoucherData fourthVoucherData = new VoucherData();
			firstVoucherData.setItem(hotel);
			secondVoucherData.setItem(hotel);
			thirdVoucherData.setItem(hotel);
			fourthVoucherData.setItem(hotel);
			firstVoucherData.setEntryDescription(hotel.getDescription());
			secondVoucherData.setEntryDescription(hotel.getDescription());
			thirdVoucherData.setEntryDescription(hotel.getDescription());
			fourthVoucherData.setEntryDescription(hotel.getDescription());
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			firstVoucherData.setDc(DC.D);
			firstVoucherData.setAccount(hotel.getFrom());
			firstVoucherData.setUsdEquivalent(totalCustomer);
			firstVoucherData.setLlEquivalent(totalCustomer * 1507.5);
			if (hotel.getFrom().getCurrency() == Currency.DOLAR) {
				firstVoucherData.setAcAmount(totalCustomer);
			} else {
				firstVoucherData.setAcAmount(totalCustomer * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchasePurchase = salesPurchaseFacade.getByTypeAndSP(SP.S, "Hotel");
			secondVoucherData.setDc(DC.C);
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null) {
				Account account = salesPurchasePurchase.getAccount();
				secondVoucherData.setAccount(account);
				secondVoucherData.setUsdEquivalent(totalCustomer);
				secondVoucherData.setLlEquivalent(totalCustomer * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					secondVoucherData.setAcAmount(totalCustomer);
				} else {
					secondVoucherData.setAcAmount(totalCustomer * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			thirdVoucherData.setDc(DC.C);
			thirdVoucherData.setAccount(hotel.getTo());
			thirdVoucherData.setUsdEquivalent(totalSupplier);
			thirdVoucherData.setLlEquivalent(totalSupplier * 1507.5);
			if (hotel.getTo().getCurrency() == Currency.DOLAR) {
				thirdVoucherData.setAcAmount(totalSupplier);
			} else {
				thirdVoucherData.setAcAmount(totalSupplier * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchaseSales = salesPurchaseFacade.getByTypeAndSP(SP.P, "Hotel");
			fourthVoucherData.setDc(DC.D);
			if (salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				Account account = salesPurchaseSales.getAccount();
				fourthVoucherData.setAccount(account);
				fourthVoucherData.setUsdEquivalent(totalSupplier);
				fourthVoucherData.setLlEquivalent(totalSupplier * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					fourthVoucherData.setAcAmount(totalSupplier);
				} else {
					fourthVoucherData.setAcAmount(totalSupplier * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null
					&& salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				voucherDatas.add(firstVoucherData);
				voucherDatas.add(secondVoucherData);
				voucherDatas.add(thirdVoucherData);
				voucherDatas.add(fourthVoucherData);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: HotelController.initVoucherDatas");
		}
		return voucherDatas;
	}

	public String voucherHeaderId() {
		if (hotel == null || hotel.getVoucher() == null) {
			return "No Voucher";
		} else {
			return String.valueOf(hotel.getVoucher().getVoucherNumber());
		}
	}

	public void printItem() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			ServletOutputStream servletOutputStream = response.getOutputStream();

			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			InputStream is = classloader.getResourceAsStream("item.jrxml");
			JasperReport report = JasperCompileManager.compileReport(is);

			Map<String, Object> parameters = new HashMap<>();

			List<ItemDto> dtos = new ArrayList<>();

			ItemDto itemDto = new ItemDto();
			itemDto.setTax(String.valueOf(hotel.getTax()));
			itemDto.setAmount(String.valueOf(hotel.getTotal()));

			String description = "";
			if (hotel.getDescription() != null) {
				description += hotel.getDescription() + " / ";
			}
			description += "ISSUE DATE: ";
			if (hotel.getDate() != null) {
				description += getStringDateFromDate(hotel.getDate());
			} else {
				description += "NO DATE";
			}
			itemDto.setDescription(description);
			dtos.add(itemDto);

			String companyName = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.CompanyDescription);
			String addressLineOne = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.AddressLineOne);
			String addressLineTwo = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.AddressLineTwo);
			String addressLineThree = companyConfigurationFacade
					.getValueByKey(CompanyConfigurationTypes.AddressLineThree);
			String voucherNumber = companyConfigurationFacade
					.getValueByKey(CompanyConfigurationTypes.MOFRegistrationNo);
			if (companyName != null) {
				parameters.put("companyName", companyName);
			} else {
				parameters.put("companyName", "NO COMPANY NAME");
			}
			if (companyName != null) {
				parameters.put("address", addressLineOne);
			} else {
				parameters.put("address", "NO ADDRESS LINE ONE");
			}
			if (companyName != null) {
				parameters.put("phoneNumber", addressLineTwo);
			} else {
				parameters.put("phoneNumber", "NO ADDRESS LINE TWO");
			}
			if (companyName != null) {
				parameters.put("email", addressLineThree);
			} else {
				parameters.put("email", "NO ADDRESS LINE THREE");
			}
			if (companyName != null) {
				parameters.put("voucherNumber", voucherNumber);
			} else {
				parameters.put("voucherNumber", "NO MOF REGISTRATION NO");
			}
			///////////////////////////////
			///////////////////////////////
			///////////////////////////////
			parameters.put("customerName",
					hotel.getFrom() == null ? "NO CUSTOMER NAME" : hotel.getFrom().displayName());
			parameters.put("customerFinNumber", hotel.getFrom().getFinancialNumber() == null ? "NO FINANCIAL NUMBER"
					: hotel.getFrom().getFinancialNumber());
			parameters.put("customerPhoneNumber",
					hotel.getFrom().getPhoneNumber() == null ? "NO PHONE NUMBER" : hotel.getFrom().getPhoneNumber());
			parameters.put("customerAddress",
					hotel.getFrom().getTitle() == null ? "NO TITLE" : hotel.getFrom().getTitle());
			parameters.put("invoiceNumber", hotel.displayName());
			parameters.put("invoiceDate",
					hotel.getDate() == null ? "NO INVOICE DATE" : getStringDateFromDate(hotel.getDate()));
			parameters.put("salesMan", hotel.getSalesMan() == null ? "NO SALES MAN" : hotel.getSalesMan());
			parameters.put("branch", "1");
			parameters.put("hotels", String.valueOf(hotel.getTotal()));
			parameters.put("taxes", String.valueOf(hotel.getTax()));
			parameters.put("vat", String.valueOf(hotel.getTax()));
			parameters.put("insur", "");
			parameters.put("netUSD", String.valueOf(hotel.getTotal()));
			parameters.put("netLL", String.valueOf(hotel.getTotal() * 1507.5));
			parameters.put("only", CashWordConverter.doubleConvert(hotel.getTotal()));
			parameters.put("voucherDatas", dtos);

			byte[] bytes = null;
			bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource(1));

			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);

			servletOutputStream.write(bytes, 0, bytes.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the hotelFacade
	 */
	public HotelFacade getHotelFacade() {
		return hotelFacade;
	}

	/**
	 * @param hotelFacade the hotelFacade to set
	 */
	public void setHotelFacade(HotelFacade hotelFacade) {
		this.hotelFacade = hotelFacade;
	}

	/**
	 * @return the hotel
	 */
	public Hotel getHotel() {
		return hotel;
	}

	/**
	 * @param hotel the hotel to set
	 */
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the customerFacade
	 */
	public CustomerFacade getCustomerFacade() {
		return customerFacade;
	}

	/**
	 * @param customerFacade the customerFacade to set
	 */
	public void setCustomerFacade(CustomerFacade customerFacade) {
		this.customerFacade = customerFacade;
	}

	/**
	 * @return the supplierFacade
	 */
	public SupplierFacade getSupplierFacade() {
		return supplierFacade;
	}

	/**
	 * @param supplierFacade the supplierFacade to set
	 */
	public void setSupplierFacade(SupplierFacade supplierFacade) {
		this.supplierFacade = supplierFacade;
	}

	/**
	 * @return the allCustomers
	 */
	public List<Customer> getAllCustomers() {
		return allCustomers;
	}

	/**
	 * @param allCustomers the allCustomers to set
	 */
	public void setAllCustomers(List<Customer> allCustomers) {
		this.allCustomers = allCustomers;
	}

	/**
	 * @return the allSuppliers
	 */
	public List<Supplier> getAllSuppliers() {
		return allSuppliers;
	}

	/**
	 * @param allSuppliers the allSuppliers to set
	 */
	public void setAllSuppliers(List<Supplier> allSuppliers) {
		this.allSuppliers = allSuppliers;
	}

	private String getStringDateFromDate(Date date) {
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(date);
	}
}
