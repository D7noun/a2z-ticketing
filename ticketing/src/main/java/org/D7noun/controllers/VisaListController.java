package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.D7noun.facades.VisaFacade;
import org.D7noun.models.items.Visa;

@Named
@ViewScoped
public class VisaListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private VisaFacade visaFacade;
	private List<Visa> allVisas = new ArrayList<Visa>();

	///////////////////////////
	///////////////////////////
	///////////////////////////
	private Visa visaForDelete;
	///////////////////////////
	///////////////////////////
	///////////////////////////

	@PostConstruct
	public void init() {
		try {
			allVisas = visaFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VisaListController.init");
		}
	}

	public void delete(Visa visa) {
		visaFacade.remove(visa);
		allVisas = visaFacade.findAll();
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the visaFacade
	 */
	public VisaFacade getVisaFacade() {
		return visaFacade;
	}

	/**
	 * @param visaFacade the visaFacade to set
	 */
	public void setVisaFacade(VisaFacade visaFacade) {
		this.visaFacade = visaFacade;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allVisas
	 */
	public List<Visa> getAllVisas() {
		return allVisas;
	}

	/**
	 * @param allVisas the allVisas to set
	 */
	public void setAllVisas(List<Visa> allVisas) {
		this.allVisas = allVisas;
	}

	/**
	 * @return the visaForDelete
	 */
	public Visa getVisaForDelete() {
		return visaForDelete;
	}

	/**
	 * @param visaForDelete the visaForDelete to set
	 */
	public void setVisaForDelete(Visa visaForDelete) {
		this.visaForDelete = visaForDelete;
	}

}
