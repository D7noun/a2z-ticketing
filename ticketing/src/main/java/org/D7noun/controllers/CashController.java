package org.D7noun.controllers;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.AccountConfigurationFacade;
import org.D7noun.facades.AccountFacade;
import org.D7noun.facades.AccountSerialFacade;
import org.D7noun.facades.CashFacade;
import org.D7noun.models.AccountConfiguration;
import org.D7noun.models.AccountSerial;
import org.D7noun.models.AccountConfiguration.AccountTypes;
import org.D7noun.models.AccountTree;
import org.D7noun.models.accounts.Bank;
import org.D7noun.models.accounts.Cash;
import org.omnifaces.util.Faces;

@ManagedBean
@ViewScoped
public class CashController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private CashFacade cashFacade;
	@EJB
	private AccountConfigurationFacade accountConfigurationFacade;
	@EJB
	private AccountFacade accountFacade;
	@EJB
	private AccountSerialFacade accountSerialFacade;
	private Cash cash;

	private String id;

	private boolean canEdit = true;

	@PostConstruct
	public void init() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CashController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				if (id != null) {
					cash = cashFacade.find(Long.parseLong(id));
				} else {
					cash = new Cash();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CashController.preRenderView");
		}
	}

	public void save() {
		try {
			boolean redirect = false;
			/////////////////////////
			/////////////////////////
			AccountConfiguration accountConfiguration = accountConfigurationFacade
					.getAccountConfigurationByAccountType(AccountTypes.Cashs.toString());
			AccountTree accountTree = accountConfiguration.getAccountTree();
			cash.setType(accountTree.getCode());
			/////////////////////////
			/////////////////////////
			if (cash.getId() == null || cash.getId() <= 0) {
				redirect = true;
				AccountSerial accountSerial = accountSerialFacade.getByAccountCodeAndCurrency(accountTree.getCode(),
						cash.getCurrency());

				long accountId = 0;
				if (accountSerial == null) {
					accountSerial = new AccountSerial();
					accountSerial.setAccountCode(accountTree.getCode());
					accountSerial.setCurrency(cash.getCurrency());
				} else {
					accountId = accountSerial.getValue();
				}
				boolean reservedId = true;
				while (reservedId) {
					Bank reserved = (Bank) accountFacade.getAccountByAccountIdAndCodeAndCurrency(accountTree.getCode(),
							++accountId, cash.getCurrency());
					if (reserved == null) {
						reservedId = false;
					}
				}
				accountSerial.setValue(accountId);
				cash.setAccountId(accountId);
				accountSerial = accountSerialFacade.save(accountSerial);
			}
			cash = cashFacade.save(cash);

			if (redirect) {
				Faces.redirect(GeneralUtility.getOutcomeURL("cash-card") + "?id=%s", Long.toString(cash.getId()));
			}
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CashController.save");
		}
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the cashFacade
	 */
	public CashFacade getCashFacade() {
		return cashFacade;
	}

	/**
	 * @param cashFacade the cashFacade to set
	 */
	public void setCashFacade(CashFacade cashFacade) {
		this.cashFacade = cashFacade;
	}

	/**
	 * @return the cash
	 */
	public Cash getCash() {
		return cash;
	}

	/**
	 * @param cash the cash to set
	 */
	public void setCash(Cash cash) {
		this.cash = cash;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

}
