package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.D7noun.facades.SupplierFacade;
import org.D7noun.models.accounts.Supplier;

@Named
@ViewScoped
public class SupplierListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private SupplierFacade supplierFacade;
	private List<Supplier> allSuppliers = new ArrayList<Supplier>();

	///////////////////////////
	///////////////////////////
	///////////////////////////
	private Supplier supplierForDelete;
	///////////////////////////
	///////////////////////////
	///////////////////////////

	@PostConstruct
	public void init() {
		try {
			allSuppliers = supplierFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: SupplierListController.init");
		}
	}

	public void delete(Supplier supplier) {
		supplierFacade.remove(supplier);
		allSuppliers = supplierFacade.findAll();
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the supplierFacade
	 */
	public SupplierFacade getSupplierFacade() {
		return supplierFacade;
	}

	/**
	 * @param supplierFacade the supplierFacade to set
	 */
	public void setSupplierFacade(SupplierFacade supplierFacade) {
		this.supplierFacade = supplierFacade;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allSuppliers
	 */
	public List<Supplier> getAllSuppliers() {
		return allSuppliers;
	}

	/**
	 * @param allSuppliers the allSuppliers to set
	 */
	public void setAllSuppliers(List<Supplier> allSuppliers) {
		this.allSuppliers = allSuppliers;
	}

	/**
	 * @return the supplierForDelete
	 */
	public Supplier getSupplierForDelete() {
		return supplierForDelete;
	}

	/**
	 * @param supplierForDelete the supplierForDelete to set
	 */
	public void setSupplierForDelete(Supplier supplierForDelete) {
		this.supplierForDelete = supplierForDelete;
	}

}
