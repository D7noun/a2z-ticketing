package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.D7noun.facades.BankFacade;
import org.D7noun.models.accounts.Bank;

@Named
@ViewScoped
public class BankListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private BankFacade bankFacade;
	private List<Bank> allBanks = new ArrayList<Bank>();

	///////////////////////////
	///////////////////////////
	///////////////////////////
	private Bank bankForDelete;
	///////////////////////////
	///////////////////////////
	///////////////////////////

	@PostConstruct
	public void init() {
		try {
			allBanks = bankFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: BankListController.init");
		}
	}

	public void delete(Bank bank) {
		bankFacade.remove(bank);
		allBanks = bankFacade.findAll();
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the bankFacade
	 */
	public BankFacade getBankFacade() {
		return bankFacade;
	}

	/**
	 * @param bankFacade the bankFacade to set
	 */
	public void setBankFacade(BankFacade bankFacade) {
		this.bankFacade = bankFacade;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allBanks
	 */
	public List<Bank> getAllBanks() {
		return allBanks;
	}

	/**
	 * @param allBanks the allBanks to set
	 */
	public void setAllBanks(List<Bank> allBanks) {
		this.allBanks = allBanks;
	}

	/**
	 * @return the bankForDelete
	 */
	public Bank getBankForDelete() {
		return bankForDelete;
	}

	/**
	 * @param bankForDelete the bankForDelete to set
	 */
	public void setBankForDelete(Bank bankForDelete) {
		this.bankForDelete = bankForDelete;
	}

}
