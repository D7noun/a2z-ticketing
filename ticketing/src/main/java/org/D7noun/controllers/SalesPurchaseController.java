package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.AccountFacade;
import org.D7noun.facades.SalesPurchaseFacade;
import org.D7noun.models.SalesPurchase;
import org.D7noun.models.accounts.Account;
import org.omnifaces.util.Faces;

@ManagedBean
@ViewScoped
public class SalesPurchaseController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private SalesPurchaseFacade salesPurchaseFacade;

	@EJB
	private AccountFacade accountFacade;

	private List<SalesPurchase> allSalesPurchase = new ArrayList<SalesPurchase>();
	private List<Account> allAccounts = new ArrayList<Account>();

	@PostConstruct
	public void init() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: SalesPurchaseController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				allSalesPurchase = salesPurchaseFacade.findAll();
				allAccounts = accountFacade.findAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: SalesPurchaseController.preRenderView");
		}
	}

	public void save() {
		try {
			salesPurchaseFacade.save(allSalesPurchase);
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			GeneralUtility.showErrorMessage(GeneralUtility.getResourceBundleString("save_error"));
			e.printStackTrace();
			System.err.println("D7noun: SalesPurchaseController.save");
		}
	}

	/**
	 * @return the salesPurchaseFacade
	 */
	public SalesPurchaseFacade getSalesPurchaseFacade() {
		return salesPurchaseFacade;
	}

	/**
	 * @param salesPurchaseFacade the salesPurchaseFacade to set
	 */
	public void setSalesPurchaseFacade(SalesPurchaseFacade salesPurchaseFacade) {
		this.salesPurchaseFacade = salesPurchaseFacade;
	}

	/**
	 * @return the allSalesPurchase
	 */
	public List<SalesPurchase> getAllSalesPurchase() {
		return allSalesPurchase;
	}

	/**
	 * @param allSalesPurchase the allSalesPurchase to set
	 */
	public void setAllSalesPurchase(List<SalesPurchase> allSalesPurchase) {
		this.allSalesPurchase = allSalesPurchase;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the accountFacade
	 */
	public AccountFacade getAccountFacade() {
		return accountFacade;
	}

	/**
	 * @param accountFacade the accountFacade to set
	 */
	public void setAccountFacade(AccountFacade accountFacade) {
		this.accountFacade = accountFacade;
	}

	/**
	 * @return the allAccounts
	 */
	public List<Account> getAllAccounts() {
		return allAccounts;
	}

	/**
	 * @param allAccounts the allAccounts to set
	 */
	public void setAllAccounts(List<Account> allAccounts) {
		this.allAccounts = allAccounts;
	}

}