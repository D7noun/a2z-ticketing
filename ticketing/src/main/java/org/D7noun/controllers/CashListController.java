package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.D7noun.facades.CashFacade;
import org.D7noun.models.accounts.Cash;

@Named
@ViewScoped
public class CashListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private CashFacade cashFacade;
	private List<Cash> allCashs = new ArrayList<Cash>();

	///////////////////////////
	///////////////////////////
	///////////////////////////
	private Cash cashForDelete;
	///////////////////////////
	///////////////////////////
	///////////////////////////

	@PostConstruct
	public void init() {
		try {
			allCashs = cashFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CashListController.init");
		}
	}

	public void delete(Cash cash) {
		cashFacade.remove(cash);
		allCashs = cashFacade.findAll();
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the cashFacade
	 */
	public CashFacade getCashFacade() {
		return cashFacade;
	}

	/**
	 * @param cashFacade the cashFacade to set
	 */
	public void setCashFacade(CashFacade cashFacade) {
		this.cashFacade = cashFacade;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allCashs
	 */
	public List<Cash> getAllCashs() {
		return allCashs;
	}

	/**
	 * @param allCashs the allCashs to set
	 */
	public void setAllCashs(List<Cash> allCashs) {
		this.allCashs = allCashs;
	}

	/**
	 * @return the cashForDelete
	 */
	public Cash getCashForDelete() {
		return cashForDelete;
	}

	/**
	 * @param cashForDelete the cashForDelete to set
	 */
	public void setCashForDelete(Cash cashForDelete) {
		this.cashForDelete = cashForDelete;
	}

}
