package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.D7noun.facades.TransportationFacade;
import org.D7noun.models.items.Transportation;

@Named
@ViewScoped
public class TransportationListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private TransportationFacade transportationFacade;
	private List<Transportation> allTransportations = new ArrayList<Transportation>();

	///////////////////////////
	///////////////////////////
	///////////////////////////
	private Transportation transportationForDelete;
	///////////////////////////
	///////////////////////////
	///////////////////////////

	@PostConstruct
	public void init() {
		try {
			allTransportations = transportationFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: TransportationListController.init");
		}
	}

	public void delete(Transportation transportation) {
		transportationFacade.remove(transportation);
		allTransportations = transportationFacade.findAll();
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the transportationFacade
	 */
	public TransportationFacade getTransportationFacade() {
		return transportationFacade;
	}

	/**
	 * @param transportationFacade the transportationFacade to set
	 */
	public void setTransportationFacade(TransportationFacade transportationFacade) {
		this.transportationFacade = transportationFacade;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allTransportations
	 */
	public List<Transportation> getAllTransportations() {
		return allTransportations;
	}

	/**
	 * @param allTransportations the allTransportations to set
	 */
	public void setAllTransportations(List<Transportation> allTransportations) {
		this.allTransportations = allTransportations;
	}

	/**
	 * @return the transportationForDelete
	 */
	public Transportation getTransportationForDelete() {
		return transportationForDelete;
	}

	/**
	 * @param transportationForDelete the transportationForDelete to set
	 */
	public void setTransportationForDelete(Transportation transportationForDelete) {
		this.transportationForDelete = transportationForDelete;
	}

}
