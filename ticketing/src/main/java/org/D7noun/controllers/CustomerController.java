package org.D7noun.controllers;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.AccountConfigurationFacade;
import org.D7noun.facades.AccountFacade;
import org.D7noun.facades.AccountSerialFacade;
import org.D7noun.facades.CustomerFacade;
import org.D7noun.models.AccountConfiguration;
import org.D7noun.models.AccountConfiguration.AccountTypes;
import org.D7noun.models.AccountSerial;
import org.D7noun.models.AccountTree;
import org.D7noun.models.accounts.Customer;
import org.omnifaces.util.Faces;

@ManagedBean
@ViewScoped
public class CustomerController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private CustomerFacade customerFacade;
	@EJB
	private AccountConfigurationFacade accountConfigurationFacade;
	@EJB
	private AccountFacade accountFacade;
	@EJB
	private AccountSerialFacade accountSerialFacade;
	private Customer customer;

	private String id;

	private boolean canEdit = true;

	@PostConstruct
	public void init() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CustomerController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				if (id != null) {
					customer = customerFacade.find(Long.parseLong(id));
				} else {
					customer = new Customer();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CustomerController.preRenderView");
		}
	}

	public void save() {
		try {
			boolean redirect = false;
			/////////////////////////
			/////////////////////////
			AccountConfiguration accountConfiguration = accountConfigurationFacade
					.getAccountConfigurationByAccountType(AccountTypes.Customers.toString());
			AccountTree accountTree = accountConfiguration.getAccountTree();
			customer.setType(accountTree.getCode());
			/////////////////////////
			/////////////////////////
			if (customer.getId() == null || customer.getId() <= 0) {
				redirect = true;
				AccountSerial accountSerial = accountSerialFacade.getByAccountCodeAndCurrency(accountTree.getCode(),
						customer.getCurrency());

				long accountId = 0;
				if (accountSerial == null) {
					accountSerial = new AccountSerial();
					accountSerial.setAccountCode(accountTree.getCode());
					accountSerial.setCurrency(customer.getCurrency());
				} else {
					accountId = accountSerial.getValue();
				}
				boolean reservedId = true;
				while (reservedId) {
					Customer reserved = (Customer) accountFacade.getAccountByAccountIdAndCodeAndCurrency(
							accountTree.getCode(), ++accountId, customer.getCurrency());
					if (reserved == null) {
						reservedId = false;
					}
				}
				accountSerial.setValue(accountId);
				customer.setAccountId(accountId);
				accountSerial = accountSerialFacade.save(accountSerial);
			}
			customer = customerFacade.save(customer);
			if (redirect) {
				Faces.redirect(GeneralUtility.getOutcomeURL("customer-card") + "?id=%s",
						Long.toString(customer.getId()));
			}
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			GeneralUtility.showErrorMessage(GeneralUtility.getResourceBundleString("save_error"));
			e.printStackTrace();
			System.err.println("D7noun: CustomerController.save");
		}
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the customerFacade
	 */
	public CustomerFacade getCustomerFacade() {
		return customerFacade;
	}

	/**
	 * @param customerFacade the customerFacade to set
	 */
	public void setCustomerFacade(CustomerFacade customerFacade) {
		this.customerFacade = customerFacade;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

}
