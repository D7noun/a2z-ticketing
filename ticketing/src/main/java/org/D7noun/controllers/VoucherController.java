package org.D7noun.controllers;

import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.dto.OrderDto;
import org.D7noun.facades.AccountFacade;
import org.D7noun.facades.CompanyConfigurationFacade;
import org.D7noun.facades.CurrencyConfigurationFacade;
import org.D7noun.facades.VoucherFacade;
import org.D7noun.models.CompanyConfiguration.CompanyConfigurationTypes;
import org.D7noun.models.Voucher;
import org.D7noun.models.Voucher.VoucherType;
import org.D7noun.models.VoucherData;
import org.D7noun.models.VoucherData.DC;
import org.D7noun.models.accounts.Account;
import org.D7noun.models.accounts.Account.Currency;
import org.D7noun.models.accounts.Customer;
import org.D7noun.models.accounts.Supplier;
import org.D7noun.service.CashWordConverter;
import org.omnifaces.util.Ajax;
import org.omnifaces.util.Faces;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;

@ManagedBean
@ViewScoped
public class VoucherController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private VoucherFacade voucherFacade;
	@EJB
	private AccountFacade accountFacade;
	@EJB
	private CurrencyConfigurationFacade currencyConfigurationFacade;
	@EJB
	private CompanyConfigurationFacade companyConfigurationFacade;
	//////////////////////////////////
	//////////////////////////////////
	private Voucher voucher;
	private String id;
	//////////////////////////////////
	//////////////////////////////////
	private List<Account> allAccounts = new ArrayList<Account>();
	//////////////////////////////////
	private boolean canEdit = true;
	//////////////////////////////////
	//////////////////////////////////
	private VoucherData voucherDataForAccountSelection;
	private Account selectedAccount;
	//////////////////////////////////
	//////////////////////////////////
//	private int liraPerDolar;
	private String voucherType;
	//////////////////////////////////
	//////////////////////////////////

	public boolean disableVoucherType() {
		if (voucher != null && (voucher.getVoucherType() == VoucherType.RE || voucher.getVoucherType() == VoucherType.TS
				|| voucher.getVoucherType() == VoucherType.TI || voucher.getVoucherType() == VoucherType.HS
				|| voucher.getVoucherType() == VoucherType.FS || voucher.getVoucherType() == VoucherType.VS)) {
			return true;
		}
		return false;
	}

	@PostConstruct
	public void init() {
		try {
			voucherType = "";
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VoucherController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				if (id != null) {
					voucher = voucherFacade.find(Long.parseLong(id));
					voucherType = voucher.getVoucherType().toString();
					if (voucher.getVoucherType() == VoucherType.TI || voucher.getVoucherType() == VoucherType.TS
							|| voucher.getVoucherType() == VoucherType.RE) {
						canEdit = false;
					}
				} else {
					voucher = new Voucher();
					voucher.setVoucherDate(new Date());
				}
			}
			allAccounts = accountFacade.findAll();
//			liraPerDolar = currencyConfigurationFacade.getLiraByDate(new Date());
//			if (liraPerDolar == 0) {
//				liraPerDolar = 1507;
//			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CustomerController.preRenderView");
		}
	}

	public void onTypeChange() {
		long maxVoucherNumber = voucherFacade.getMaxVoucherNumberByType(this.voucher.getVoucherType());
		this.voucher.setVoucherNumber(++maxVoucherNumber);
	}

	public void onAmountChange(AjaxBehaviorEvent event) {
		try {
			VoucherData data = (VoucherData) event.getComponent().getAttributes().get("item");
			if (data != null && data.getAccount() != null) {
				double amountChanged = (double) ((UIOutput) event.getSource()).getValue();
				if (data.getAccount().getCurrency() == Currency.LIRA) {
					data.setLlEquivalent(amountChanged);
					data.setUsdEquivalent(amountChanged / 1507.5);
				} else {
					data.setLlEquivalent(amountChanged * 1507.5);
					data.setUsdEquivalent(amountChanged);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void save() {
		try {
			if (this.voucher.getVoucherType() == null) {
				GeneralUtility.showWarningMessage("No Voucher Type");
				return;
			}
			Iterator<VoucherData> iterator = voucher.getVoucherDatas().iterator();
			while (iterator.hasNext()) {
				VoucherData voucherData = iterator.next();
				if (voucherData.getDc() == null) {
					GeneralUtility.showWarningMessage("required_dc");
					return;
				}
			}
			boolean redirect = false;
			if (voucher.getId() == null || voucher.getId() <= 0) {
				redirect = true;
			}
			if (voucher.balanceLL() != 0 || voucher.balanceUSD() != 0) {
				GeneralUtility.showErrorMessage("cannot_save_before_empting_totals");
				return;
			}
			voucher.getVoucherDatas().forEach(vd -> {
				vd.setVoucherDate(voucher.getVoucherDate());
				vd.setVoucherNumber(voucher.getVoucherNumber());
				vd.setVoucherType(voucher.getVoucherType());
			});
			voucher = voucherFacade.save(voucher);
			if (redirect) {
				Faces.redirect(GeneralUtility.getOutcomeURL("voucher-card") + "?id=%s", Long.toString(voucher.getId()));
			}
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			GeneralUtility.showErrorMessage("save_error");
			e.printStackTrace();
			System.err.println("D7noun: VoucherController.save");
		}
	}

	public void addNewVoucherData() {
		VoucherData voucherData = new VoucherData();
		if (!voucher.getVoucherDatas().isEmpty()) {
			VoucherData data = voucher.getVoucherDatas().get(voucher.getVoucherDatas().size() - 1);
//			if (data != null) {
//				if (data.getDc() == DC.C) {
//					voucherData.setDc(DC.D);
//				} else {
//					voucherData.setDc(DC.C);
//				}
//			}
			if (voucher.balanceUSD() > 0) {
				voucherData.setDc(DC.C);
				if (data.getAccount().getCurrency() == Currency.DOLAR) {
					voucherData.setAcAmount(voucher.balanceUSD());
				} else {
					voucherData.setAcAmount(voucher.balanceLL());
				}
				voucherData.setLlEquivalent(voucher.balanceLL());
				voucherData.setUsdEquivalent(voucher.balanceUSD());
			} else {
				voucherData.setDc(DC.D);
				if (data.getAccount().getCurrency() == Currency.DOLAR) {
					voucherData.setAcAmount(voucher.balanceUSD() * -1);
				} else {
					voucherData.setAcAmount(voucher.balanceLL() * -1);
				}
				voucherData.setLlEquivalent(voucher.balanceLL() * -1);
				voucherData.setUsdEquivalent(voucher.balanceUSD() * -1);
			}
		}
		voucher.addNewVoucherData(voucherData);
		Ajax.oncomplete("PF('itemVar').filter()");

	}

	public void delete(VoucherData voucherData) {
		voucherFacade.removeVoucherData(voucher, voucherData);
	}

	public void selectVoucherData(VoucherData voucherData) {
		voucherDataForAccountSelection = voucherData;
	}

	public void selectAccountAction() {
		if (voucherDataForAccountSelection != null && selectedAccount != null) {
			voucherDataForAccountSelection.setAccount(selectedAccount);
			Ajax.oncomplete("PF('itemVar').filter()");
		}
	}

	/**
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 */

	/**
	 * @return the voucherFacade
	 */
	public VoucherFacade getVoucherFacade() {
		return voucherFacade;
	}

	/**
	 * @param voucherFacade the voucherFacade to set
	 */
	public void setVoucherFacade(VoucherFacade voucherFacade) {
		this.voucherFacade = voucherFacade;
	}

	/**
	 * @return the voucher
	 */
	public Voucher getVoucher() {
		return voucher;
	}

	/**
	 * @param voucher the voucher to set
	 */
	public void setVoucher(Voucher voucher) {
		this.voucher = voucher;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the accountFacade
	 */
	public AccountFacade getAccountFacade() {
		return accountFacade;
	}

	/**
	 * @param accountFacade the accountFacade to set
	 */
	public void setAccountFacade(AccountFacade accountFacade) {
		this.accountFacade = accountFacade;
	}

	/**
	 * @return the allAccounts
	 */
	public List<Account> getAllAccounts() {
		return allAccounts;
	}

	/**
	 * @param allAccounts the allAccounts to set
	 */
	public void setAllAccounts(List<Account> allAccounts) {
		this.allAccounts = allAccounts;
	}

	/**
	 * @return the voucherDataForAccountSelection
	 */
	public VoucherData getVoucherDataForAccountSelection() {
		return voucherDataForAccountSelection;
	}

	/**
	 * @param voucherDataForAccountSelection the voucherDataForAccountSelection to
	 *                                       set
	 */
	public void setVoucherDataForAccountSelection(VoucherData voucherDataForAccountSelection) {
		this.voucherDataForAccountSelection = voucherDataForAccountSelection;
	}

	/**
	 * @return the selectedAccount
	 */
	public Account getSelectedAccount() {
		return selectedAccount;
	}

	/**
	 * @param selectedAccount the selectedAccount to set
	 */
	public void setSelectedAccount(Account selectedAccount) {
		this.selectedAccount = selectedAccount;
	}

	/**
	 * @return the currencyConfigurationFacade
	 */
	public CurrencyConfigurationFacade getCurrencyConfigurationFacade() {
		return currencyConfigurationFacade;
	}

	/**
	 * @param currencyConfigurationFacade the currencyConfigurationFacade to set
	 */
	public void setCurrencyConfigurationFacade(CurrencyConfigurationFacade currencyConfigurationFacade) {
		this.currencyConfigurationFacade = currencyConfigurationFacade;
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * Printing
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	public boolean isPrintingInvoiceVisible() {
		if (voucher != null && voucher.getVoucherType() != null
				&& (voucher.getVoucherType() == VoucherType.PV || voucher.getVoucherType() == VoucherType.RV)) {
			return true;
		}
		return false;
	}

	public void printOrder() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			ServletOutputStream servletOutputStream = response.getOutputStream();

			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			InputStream is = classloader.getResourceAsStream("order.jrxml");
			JasperReport report = JasperCompileManager.compileReport(is);

			Map<String, Object> parameters = new HashMap<>();

			String voucherType = this.voucherType + " " + voucher.getVoucherNumber();
			List<OrderDto> dtos = new ArrayList<>();
			voucher.getVoucherDatas().forEach(vd -> {
				if (vd.getAccount() instanceof Customer || vd.getAccount() instanceof Supplier)
					dtos.add(new OrderDto(vd));
			});
			parameters.put("companyName",
					companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.CompanyDescription));
			parameters.put("voucherDate", getStringDateFromDate(voucher.getVoucherDate()));
			parameters.put("voucherType", voucherType);
			parameters.put("voucherDatas", dtos);
			parameters.put("balanceU", voucher.balanceUSD());
			parameters.put("totalDebitU", voucher.totalDrUSD());
			parameters.put("totalCreditU", voucher.totalCrUSD());
			parameters.put("balanceL", voucher.balanceLL());
			parameters.put("totalDebitL", voucher.totalDrLL());
			parameters.put("totalCreditL", voucher.totalCrLL());

			byte[] bytes = null;
			bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource(1));

			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);

			servletOutputStream.write(bytes, 0, bytes.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void printInvoice() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			ServletOutputStream servletOutputStream = response.getOutputStream();

			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			Map<String, Object> parameters = new HashMap<>();
			InputStream is = null;
			JasperReport report = null;
			String order = this.voucherType + " " + voucher.getVoucherNumber();
			////////
			Account accountDr = null;
			Account accountCr = null;
			VoucherData voucherDataDR = null;
			VoucherData voucherDataCR = null;
			////////////////////////////////////////////
			parameters.put("companyName",
					companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.CompanyDescription));
			parameters.put("address",
					companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.AddressLineOne));
			parameters.put("phoneNumber",
					companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.AddressLineTwo));
			parameters.put("email",
					companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.AddressLineThree));
			parameters.put("voucherNumber",
					companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.MOFRegistrationNo));
			byte[] bytes = null;
			for (VoucherData voucherData : voucher.getVoucherDatas()) {
				if (voucherData.getDc() == DC.D) {
					voucherDataDR = voucherData;
					accountDr = voucherData.getAccount();
				}
				if (voucherData.getDc() == DC.C) {
					voucherDataCR = voucherData;
					accountCr = voucherData.getAccount();
				}
			}
			////////////////////////////////////////////
			switch (this.voucher.getVoucherType()) {
			case PV:
				is = classloader.getResourceAsStream("pv.jrxml");
				report = JasperCompileManager.compileReport(is);
				if (accountDr != null) {
					parameters.put("payToTheOrderOf", accountDr.getName());
					if (accountDr instanceof Customer) {
						String phoneNumber = ((Customer) accountDr).getPhoneNumber();
						if (phoneNumber != null) {
							parameters.put("addressAndPhone", phoneNumber);
						} else {
							parameters.put("addressAndPhone", "");
						}
					} else if (accountDr instanceof Supplier) {
						String phoneNumber = ((Supplier) accountDr).getPhoneNumber();
						if (phoneNumber != null) {
							parameters.put("addressAndPhone", phoneNumber);
						} else {
							parameters.put("addressAndPhone", "");
						}
					} else {
						parameters.put("addressAndPhone", "");
					}

				} else {
					parameters.put("payToTheOrderOf", "No Account");
					parameters.put("addressAndPhone", "No Account");
				}
				parameters.put("order", order);
				parameters.put("amount", String.valueOf(voucher.totalDrUSD()));
				parameters.put("date", getStringDateFromDate(voucher.getVoucherDate()));
				parameters.put("theSumOf", CashWordConverter.doubleConvert(voucher.totalDrUSD()));
				parameters.put("being", voucherDataCR.getEntryDescription());
				parameters.put("formOfPayment", voucherDataCR.getAccount().displayName());

				bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource(1));

				response.setContentType("application/pdf");
				response.setContentLength(bytes.length);

				servletOutputStream.write(bytes, 0, bytes.length);
				servletOutputStream.flush();
				servletOutputStream.close();

				break;
			case RV:
				is = classloader.getResourceAsStream("rv.jrxml");
				report = JasperCompileManager.compileReport(is);
				if (accountCr != null) {
					parameters.put("payToTheOrderOf", accountCr.getName());
					if (accountCr instanceof Customer) {
						String phoneNumber = ((Customer) accountCr).getPhoneNumber();
						if (phoneNumber != null) {
							parameters.put("addressAndPhone", phoneNumber);
						} else {
							parameters.put("addressAndPhone", "");
						}
					} else if (accountCr instanceof Supplier) {
						String phoneNumber = ((Supplier) accountCr).getPhoneNumber();
						if (phoneNumber != null) {
							parameters.put("addressAndPhone", phoneNumber);
						} else {
							parameters.put("addressAndPhone", "");
						}
					} else {
						parameters.put("addressAndPhone", "");
					}

				} else {
					parameters.put("payToTheOrderOf", "No Account");
					parameters.put("addressAndPhone", "No Account");
				}
				parameters.put("order", order);
				parameters.put("amount", String.valueOf(voucher.totalDrUSD()));
				parameters.put("date", getStringDateFromDate(voucher.getVoucherDate()));
				parameters.put("theSumOf", CashWordConverter.doubleConvert(voucher.totalDrUSD()));
				parameters.put("being", voucherDataDR.getEntryDescription());
				parameters.put("formOfPayment", voucherDataDR.getAccount().displayName());

				bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource(1));

				response.setContentType("application/pdf");
				response.setContentLength(bytes.length);

				servletOutputStream.write(bytes, 0, bytes.length);
				servletOutputStream.flush();
				servletOutputStream.close();
				break;
			case TI:
				break;
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getStringDateFromDate(Date date) {
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(date);
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

}
