package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.D7noun.facades.CustomerFacade;
import org.D7noun.models.accounts.Customer;

@Named
@ViewScoped
public class CustomerListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private CustomerFacade customerFacade;
	private List<Customer> allCustomers = new ArrayList<Customer>();

	///////////////////////////
	///////////////////////////
	///////////////////////////
	private Customer customerForDelete;
	///////////////////////////
	///////////////////////////
	///////////////////////////

	@PostConstruct
	public void init() {
		try {
			allCustomers = customerFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CustomerListController.init");
		}
	}

	public void delete(Customer customer) {
		customerFacade.remove(customer);
		allCustomers = customerFacade.findAll();
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the customerFacade
	 */
	public CustomerFacade getCustomerFacade() {
		return customerFacade;
	}

	/**
	 * @param customerFacade the customerFacade to set
	 */
	public void setCustomerFacade(CustomerFacade customerFacade) {
		this.customerFacade = customerFacade;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allCustomers
	 */
	public List<Customer> getAllCustomers() {
		return allCustomers;
	}

	/**
	 * @param allCustomers the allCustomers to set
	 */
	public void setAllCustomers(List<Customer> allCustomers) {
		this.allCustomers = allCustomers;
	}

	/**
	 * @return the customerForDelete
	 */
	public Customer getCustomerForDelete() {
		return customerForDelete;
	}

	/**
	 * @param customerForDelete the customerForDelete to set
	 */
	public void setCustomerForDelete(Customer customerForDelete) {
		this.customerForDelete = customerForDelete;
	}

}
