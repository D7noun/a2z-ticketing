package org.D7noun.controllers;

import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.dto.ItemDto;
import org.D7noun.facades.CompanyConfigurationFacade;
import org.D7noun.facades.CurrencyConfigurationFacade;
import org.D7noun.facades.CustomerFacade;
import org.D7noun.facades.FoodFacade;
import org.D7noun.facades.SalesPurchaseFacade;
import org.D7noun.facades.SupplierFacade;
import org.D7noun.facades.VoucherDataFacade;
import org.D7noun.facades.VoucherFacade;
import org.D7noun.models.SalesPurchase;
import org.D7noun.models.SalesPurchase.SP;
import org.D7noun.models.Voucher;
import org.D7noun.models.Voucher.VoucherType;
import org.D7noun.models.VoucherData;
import org.D7noun.models.CompanyConfiguration.CompanyConfigurationTypes;
import org.D7noun.models.VoucherData.DC;
import org.D7noun.models.accounts.Account;
import org.D7noun.models.accounts.Account.Currency;
import org.D7noun.models.accounts.Customer;
import org.D7noun.models.accounts.Supplier;
import org.D7noun.models.items.Food;
import org.D7noun.service.CashWordConverter;
import org.omnifaces.util.Faces;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;

@ManagedBean
@ViewScoped
public class FoodController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private FoodFacade foodFacade;
	@EJB
	private CustomerFacade customerFacade;
	@EJB
	private SupplierFacade supplierFacade;
	@EJB
	private VoucherFacade voucherFacade;
	@EJB
	private VoucherDataFacade voucherDataFacade;
	@EJB
	private SalesPurchaseFacade salesPurchaseFacade;
	@EJB
	private CurrencyConfigurationFacade currencyConfigurationFacade;
	@EJB
	private CompanyConfigurationFacade companyConfigurationFacade;
	///////////////////////////////////////
	///////////////////////////////////////
//	private int 1507.5;
	//////////////////////////////////
	//////////////////////////////////

	private List<Customer> allCustomers = new ArrayList<Customer>();
	private List<Supplier> allSuppliers = new ArrayList<Supplier>();

	///////////////////////////////////////
	///////////////////////////////////////
	private Food food;

	private String id;

	private boolean canEdit = true;

	@PostConstruct
	public void init() {
		try {
			allCustomers = customerFacade.findAll();
			allSuppliers = supplierFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: FoodController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				if (id != null) {
					food = foodFacade.find(Long.parseLong(id));
				} else {
					food = new Food();
					food.setQuantity(1);
				}
//				1507.5 = currencyConfigurationFacade.getLiraByDate(new Date());
//				if (1507.5 == 0) {
//					1507.5 = 1507;
//				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: FoodController.preRenderView");
		}
	}

	public void updateFromDiscountAmount() {
		double total = (food.getSelling() - food.getDiscountAmount()) * food.getQuantity();
		int dicountPercentage = 100 - ((int) ((total * 100) / food.getSelling()));
		food.setTotal(total);
		food.setDiscountPercentage(dicountPercentage);
	}

	public void updateSellingAndTotal() {
		double selling = (food.getCost() + food.getProfit() + food.getTax()) * food.getQuantity();
		double total = (selling * (100 - food.getDiscountPercentage())) / 100;
		double discountAmount = total - selling;
		food.setDiscountAmount(discountAmount * -1);
		food.setSelling(selling);
		food.setTotal(total);
	}

	public void save() {
		try {
			boolean redirect = false;
			Voucher voucher = new Voucher();
			List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
			if (food.getId() == null || food.getId() <= 0) {
				redirect = true;
				///////////////////////////////
				voucher.setVoucherType(VoucherType.FS);
				voucher.setVoucherDate(food.getDate());
				long maxVoucherNumber = voucherFacade.getMaxVoucherNumberByType(voucher.getVoucherType());
				voucher.setVoucherNumber(++maxVoucherNumber);
				voucherDatas = initVoucherDatas(food);
				if (voucherDatas != null && !voucherDatas.isEmpty()) {
					for (VoucherData voucherData : voucherDatas) {
						voucher.addNewVoucherData(voucherData);
					}
					voucher.addNewItem(food);
				}
				voucher = voucherFacade.save(voucher);
			} else {
				voucher = voucherFacade.findVoucherByItem(food);
				List<VoucherData> deletedData = voucherDataFacade.findVoucherDataByItem(food);
				voucher.deleteVoucherData(deletedData);
				voucher.setVoucherDate(food.getDate());
				voucherDatas = initVoucherDatas(food);
				for (VoucherData voucherData : voucherDatas) {
					voucher.addNewVoucherData(voucherData);
				}
				voucher = voucherFacade.save(voucher);
				food = foodFacade.save(food);
			}
			if (redirect) {
				Faces.redirect(GeneralUtility.getOutcomeURL("food-list"));
			}
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			e.printStackTrace();
			GeneralUtility.showErrorMessage(e);
			System.err.println("D7noun: FoodController.save");
		}
	}

	public List<VoucherData> initVoucherDatas(Food food) {
		List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
		try {

			double totalCustomer = food.getTotal();
			double totalSupplier = (food.getSelling() - food.getProfit() * food.getQuantity());
			VoucherData firstVoucherData = new VoucherData();
			VoucherData secondVoucherData = new VoucherData();
			VoucherData thirdVoucherData = new VoucherData();
			VoucherData fourthVoucherData = new VoucherData();
			firstVoucherData.setItem(food);
			secondVoucherData.setItem(food);
			thirdVoucherData.setItem(food);
			fourthVoucherData.setItem(food);
			firstVoucherData.setEntryDescription(food.getDescription());
			secondVoucherData.setEntryDescription(food.getDescription());
			thirdVoucherData.setEntryDescription(food.getDescription());
			fourthVoucherData.setEntryDescription(food.getDescription());
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			firstVoucherData.setDc(DC.D);
			firstVoucherData.setAccount(food.getFrom());
			firstVoucherData.setUsdEquivalent(totalCustomer);
			firstVoucherData.setLlEquivalent(totalCustomer * 1507.5);
			if (food.getFrom().getCurrency() == Currency.DOLAR) {
				firstVoucherData.setAcAmount(totalCustomer);
			} else {
				firstVoucherData.setAcAmount(totalCustomer * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchasePurchase = salesPurchaseFacade.getByTypeAndSP(SP.S, "Food");
			secondVoucherData.setDc(DC.C);
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null) {
				Account account = salesPurchasePurchase.getAccount();
				secondVoucherData.setAccount(account);
				secondVoucherData.setUsdEquivalent(totalCustomer);
				secondVoucherData.setLlEquivalent(totalCustomer * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					secondVoucherData.setAcAmount(totalCustomer);
				} else {
					secondVoucherData.setAcAmount(totalCustomer * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			thirdVoucherData.setDc(DC.C);
			thirdVoucherData.setAccount(food.getTo());
			thirdVoucherData.setUsdEquivalent(totalSupplier);
			thirdVoucherData.setLlEquivalent(totalSupplier * 1507.5);
			if (food.getTo().getCurrency() == Currency.DOLAR) {
				thirdVoucherData.setAcAmount(totalSupplier);
			} else {
				thirdVoucherData.setAcAmount(totalSupplier * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchaseSales = salesPurchaseFacade.getByTypeAndSP(SP.P, "Food");
			fourthVoucherData.setDc(DC.D);
			if (salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				Account account = salesPurchaseSales.getAccount();
				fourthVoucherData.setAccount(account);
				fourthVoucherData.setUsdEquivalent(totalSupplier);
				fourthVoucherData.setLlEquivalent(totalSupplier * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					fourthVoucherData.setAcAmount(totalSupplier);
				} else {
					fourthVoucherData.setAcAmount(totalSupplier * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null
					&& salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				voucherDatas.add(firstVoucherData);
				voucherDatas.add(secondVoucherData);
				voucherDatas.add(thirdVoucherData);
				voucherDatas.add(fourthVoucherData);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: FoodController.initVoucherDatas");
		}
		return voucherDatas;
	}

	public String voucherHeaderId() {
		if (food == null || food.getVoucher() == null) {
			return "No Voucher";
		} else {
			return String.valueOf(food.getVoucher().getVoucherNumber());
		}
	}

	public void printItem() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			ServletOutputStream servletOutputStream = response.getOutputStream();

			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			InputStream is = classloader.getResourceAsStream("item.jrxml");
			JasperReport report = JasperCompileManager.compileReport(is);

			Map<String, Object> parameters = new HashMap<>();

			List<ItemDto> dtos = new ArrayList<>();

			ItemDto itemDto = new ItemDto();
			itemDto.setTax(String.valueOf(food.getTax()));
			itemDto.setAmount(String.valueOf(food.getTotal()));

			String description = "";
			if (food.getDescription() != null) {
				description += food.getDescription() + " / ";
			}
			description += "ISSUE DATE: ";
			if (food.getDate() != null) {
				description += getStringDateFromDate(food.getDate());
			} else {
				description += "NO DATE";
			}
			itemDto.setDescription(description);
			dtos.add(itemDto);

			String companyName = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.CompanyDescription);
			String addressLineOne = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.AddressLineOne);
			String addressLineTwo = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.AddressLineTwo);
			String addressLineThree = companyConfigurationFacade
					.getValueByKey(CompanyConfigurationTypes.AddressLineThree);
			String voucherNumber = companyConfigurationFacade
					.getValueByKey(CompanyConfigurationTypes.MOFRegistrationNo);
			if (companyName != null) {
				parameters.put("companyName", companyName);
			} else {
				parameters.put("companyName", "NO COMPANY NAME");
			}
			if (companyName != null) {
				parameters.put("address", addressLineOne);
			} else {
				parameters.put("address", "NO ADDRESS LINE ONE");
			}
			if (companyName != null) {
				parameters.put("phoneNumber", addressLineTwo);
			} else {
				parameters.put("phoneNumber", "NO ADDRESS LINE TWO");
			}
			if (companyName != null) {
				parameters.put("email", addressLineThree);
			} else {
				parameters.put("email", "NO ADDRESS LINE THREE");
			}
			if (companyName != null) {
				parameters.put("voucherNumber", voucherNumber);
			} else {
				parameters.put("voucherNumber", "NO MOF REGISTRATION NO");
			}
			///////////////////////////////
			///////////////////////////////
			///////////////////////////////
			parameters.put("customerName", food.getFrom() == null ? "NO CUSTOMER NAME" : food.getFrom().displayName());
			parameters.put("customerFinNumber", food.getFrom().getFinancialNumber() == null ? "NO FINANCIAL NUMBER"
					: food.getFrom().getFinancialNumber());
			parameters.put("customerPhoneNumber",
					food.getFrom().getPhoneNumber() == null ? "NO PHONE NUMBER" : food.getFrom().getPhoneNumber());
			parameters.put("customerAddress",
					food.getFrom().getTitle() == null ? "NO TITLE" : food.getFrom().getTitle());
			parameters.put("invoiceNumber", food.displayName());
			parameters.put("invoiceDate",
					food.getDate() == null ? "NO INVOICE DATE" : getStringDateFromDate(food.getDate()));
			parameters.put("salesMan", food.getSalesMan() == null ? "NO SALES MAN" : food.getSalesMan());
			parameters.put("branch", "1");
			parameters.put("foods", String.valueOf(food.getTotal()));
			parameters.put("taxes", String.valueOf(food.getTax()));
			parameters.put("vat", String.valueOf(food.getTax()));
			parameters.put("insur", "");
			parameters.put("netUSD", String.valueOf(food.getTotal()));
			parameters.put("netLL", String.valueOf(food.getTotal() * 1507.5));
			parameters.put("only", CashWordConverter.doubleConvert(food.getTotal()));
			parameters.put("voucherDatas", dtos);

			byte[] bytes = null;
			bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource(1));

			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);

			servletOutputStream.write(bytes, 0, bytes.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the foodFacade
	 */
	public FoodFacade getFoodFacade() {
		return foodFacade;
	}

	/**
	 * @param foodFacade the foodFacade to set
	 */
	public void setFoodFacade(FoodFacade foodFacade) {
		this.foodFacade = foodFacade;
	}

	/**
	 * @return the food
	 */
	public Food getFood() {
		return food;
	}

	/**
	 * @param food the food to set
	 */
	public void setFood(Food food) {
		this.food = food;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the customerFacade
	 */
	public CustomerFacade getCustomerFacade() {
		return customerFacade;
	}

	/**
	 * @param customerFacade the customerFacade to set
	 */
	public void setCustomerFacade(CustomerFacade customerFacade) {
		this.customerFacade = customerFacade;
	}

	/**
	 * @return the supplierFacade
	 */
	public SupplierFacade getSupplierFacade() {
		return supplierFacade;
	}

	/**
	 * @param supplierFacade the supplierFacade to set
	 */
	public void setSupplierFacade(SupplierFacade supplierFacade) {
		this.supplierFacade = supplierFacade;
	}

	/**
	 * @return the allCustomers
	 */
	public List<Customer> getAllCustomers() {
		return allCustomers;
	}

	/**
	 * @param allCustomers the allCustomers to set
	 */
	public void setAllCustomers(List<Customer> allCustomers) {
		this.allCustomers = allCustomers;
	}

	/**
	 * @return the allSuppliers
	 */
	public List<Supplier> getAllSuppliers() {
		return allSuppliers;
	}

	/**
	 * @param allSuppliers the allSuppliers to set
	 */
	public void setAllSuppliers(List<Supplier> allSuppliers) {
		this.allSuppliers = allSuppliers;
	}

	private String getStringDateFromDate(Date date) {
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(date);
	}

}
