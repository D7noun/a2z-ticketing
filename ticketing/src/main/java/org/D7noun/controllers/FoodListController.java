package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.D7noun.facades.FoodFacade;
import org.D7noun.models.items.Food;

@Named
@ViewScoped
public class FoodListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private FoodFacade foodFacade;
	private List<Food> allFoods = new ArrayList<Food>();

	///////////////////////////
	///////////////////////////
	///////////////////////////
	private Food foodForDelete;
	///////////////////////////
	///////////////////////////
	///////////////////////////

	@PostConstruct
	public void init() {
		try {
			allFoods = foodFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: FoodListController.init");
		}
	}

	public void delete(Food food) {
		foodFacade.remove(food);
		allFoods = foodFacade.findAll();
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the foodFacade
	 */
	public FoodFacade getFoodFacade() {
		return foodFacade;
	}

	/**
	 * @param foodFacade the foodFacade to set
	 */
	public void setFoodFacade(FoodFacade foodFacade) {
		this.foodFacade = foodFacade;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allFoods
	 */
	public List<Food> getAllFoods() {
		return allFoods;
	}

	/**
	 * @param allFoods the allFoods to set
	 */
	public void setAllFoods(List<Food> allFoods) {
		this.allFoods = allFoods;
	}

	/**
	 * @return the foodForDelete
	 */
	public Food getFoodForDelete() {
		return foodForDelete;
	}

	/**
	 * @param foodForDelete the foodForDelete to set
	 */
	public void setFoodForDelete(Food foodForDelete) {
		this.foodForDelete = foodForDelete;
	}

}
