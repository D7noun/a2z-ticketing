package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.D7noun.facades.HotelFacade;
import org.D7noun.models.items.Hotel;

@Named
@ViewScoped
public class HotelListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private HotelFacade hotelFacade;
	private List<Hotel> allHotels = new ArrayList<Hotel>();

	///////////////////////////
	///////////////////////////
	///////////////////////////
	private Hotel hotelForDelete;
	///////////////////////////
	///////////////////////////
	///////////////////////////

	@PostConstruct
	public void init() {
		try {
			allHotels = hotelFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: HotelListController.init");
		}
	}

	public void delete(Hotel hotel) {
		hotelFacade.remove(hotel);
		allHotels = hotelFacade.findAll();
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the hotelFacade
	 */
	public HotelFacade getHotelFacade() {
		return hotelFacade;
	}

	/**
	 * @param hotelFacade the hotelFacade to set
	 */
	public void setHotelFacade(HotelFacade hotelFacade) {
		this.hotelFacade = hotelFacade;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allHotels
	 */
	public List<Hotel> getAllHotels() {
		return allHotels;
	}

	/**
	 * @param allHotels the allHotels to set
	 */
	public void setAllHotels(List<Hotel> allHotels) {
		this.allHotels = allHotels;
	}

	/**
	 * @return the hotelForDelete
	 */
	public Hotel getHotelForDelete() {
		return hotelForDelete;
	}

	/**
	 * @param hotelForDelete the hotelForDelete to set
	 */
	public void setHotelForDelete(Hotel hotelForDelete) {
		this.hotelForDelete = hotelForDelete;
	}

}
