package org.D7noun.controllers;

import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.dto.ItemDto;
import org.D7noun.facades.CompanyConfigurationFacade;
import org.D7noun.facades.CurrencyConfigurationFacade;
import org.D7noun.facades.CustomerFacade;
import org.D7noun.facades.SalesPurchaseFacade;
import org.D7noun.facades.SupplierFacade;
import org.D7noun.facades.TicketFacade;
import org.D7noun.facades.VoucherDataFacade;
import org.D7noun.facades.VoucherFacade;
import org.D7noun.models.CompanyConfiguration.CompanyConfigurationTypes;
import org.D7noun.models.SalesPurchase;
import org.D7noun.models.SalesPurchase.SP;
import org.D7noun.models.Voucher;
import org.D7noun.models.Voucher.VoucherType;
import org.D7noun.models.VoucherData;
import org.D7noun.models.VoucherData.DC;
import org.D7noun.models.accounts.Account;
import org.D7noun.models.accounts.Account.Currency;
import org.D7noun.models.accounts.Customer;
import org.D7noun.models.accounts.Supplier;
import org.D7noun.models.items.Ticket;
import org.D7noun.service.CashWordConverter;
import org.omnifaces.util.Faces;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;

@ManagedBean
@ViewScoped
public class TicketController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private TicketFacade ticketFacade;
	@EJB
	private CustomerFacade customerFacade;
	@EJB
	private SupplierFacade supplierFacade;
	@EJB
	private VoucherFacade voucherFacade;
	@EJB
	private VoucherDataFacade voucherDataFacade;
	@EJB
	private SalesPurchaseFacade salesPurchaseFacade;
	@EJB
	private CurrencyConfigurationFacade currencyConfigurationFacade;
	@EJB
	private CompanyConfigurationFacade companyConfigurationFacade;
	///////////////////////////////////////
	///////////////////////////////////////
//	private int 1507.5;
	private boolean isTicketRefund;
	//////////////////////////////////
	//////////////////////////////////

	private List<Customer> allCustomers = new ArrayList<Customer>();
	private List<Supplier> allSuppliers = new ArrayList<Supplier>();

	///////////////////////////////////////
	///////////////////////////////////////
	private Ticket ticket;

	private String id;

	private boolean canEdit = true;

	private int numberOfItems = 1;

	@PostConstruct
	public void init() {
		try {
			allCustomers = customerFacade.findAll();
			allSuppliers = supplierFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: TicketController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				if (id != null) {
					ticket = ticketFacade.find(Long.parseLong(id));
					if (ticket.getCancelationFees() != 0) {
						setTicketRefund(true);
					}
				} else {
					ticket = new Ticket();
				}
//				1507.5 = currencyConfigurationFacade.getLiraByDate(new Date());
//				if (1507.5 == 0) {
//					1507.5 = 1507;
//				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: TicketController.preRenderView");
		}
	}

	public void updateFromDiscountAmount() {
		double total = ticket.getSelling() - ticket.getDiscountAmount();
		int dicountPercentage = 100 - ((int) ((total * 100) / ticket.getSelling()));
		ticket.setTotal(total);
		ticket.setDiscountPercentage(dicountPercentage);
	}

	public void updateSellingAndTotal() {
		double selling = ticket.getFair() + ticket.getTax() + ticket.getProfit() + ticket.getInsurance();
		double total = (selling * (100 - ticket.getDiscountPercentage())) / 100;
		double discountAmount = total - selling;
		ticket.setDiscountAmount(discountAmount * -1);
		ticket.setSelling(selling);
		ticket.setTotal(total);
	}

	public void save() {
		try {
			boolean redirect = false;
			Voucher voucher = new Voucher();
			List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
			if (ticket.getId() == null || ticket.getId() <= 0) {
				redirect = true;
				///////////////////////////////
				if (isTicketRefund) {
					voucher.setVoucherType(VoucherType.RE);
				} else {
					voucher.setVoucherType(VoucherType.TI);
				}
				voucher.setVoucherDate(ticket.getDate());
				long maxVoucherNumber = voucherFacade.getMaxVoucherNumberByType(voucher.getVoucherType());
				voucher.setVoucherNumber(++maxVoucherNumber);
				ticket.setTicketPerVoucher(1);
				voucherDatas = initVoucherDatas(ticket);
				if (voucherDatas != null && !voucherDatas.isEmpty()) {
					for (VoucherData voucherData : voucherDatas) {
						voucher.addNewVoucherData(voucherData);
					}
					voucher.addNewItem(ticket);
				}
				long ticketNumber = Long.parseLong(ticket.getTicketNumber());
				for (int i = 1; i < numberOfItems; i++) {
					Ticket newTicket = new Ticket();
					ticketNumber++;
					newTicket.setDate(ticket.getDate());
					newTicket.setDateOfFlight(ticket.getDateOfFlight());
					newTicket.setDiscountAmount(ticket.getDiscountAmount());
					newTicket.setDiscountPercentage(ticket.getDiscountPercentage());
					newTicket.setTicketPerVoucher(i + 1);
					newTicket.setFair(ticket.getFair());
					newTicket.setFrom(ticket.getFrom());
					newTicket.setInsurance(ticket.getInsurance());
					newTicket.setNotes(ticket.getNotes());
					newTicket.setPassenger(ticket.getPassenger());
					newTicket.setRoute(ticket.getRoute());
					newTicket.setSalesMan(ticket.getSalesMan());
					newTicket.setSelling(ticket.getSelling());
					newTicket.setTax(ticket.getTax());
					newTicket.setTo(ticket.getTo());
					newTicket.setProfit(ticket.getProfit());
					newTicket.setTotal(ticket.getTotal());
					newTicket.setTicketNumber(String.valueOf(ticketNumber));
					voucherDatas = initVoucherDatas(newTicket);
					if (voucherDatas != null && !voucherDatas.isEmpty()) {
						for (VoucherData voucherData : voucherDatas) {
							voucher.addNewVoucherData(voucherData);
						}
						voucher.addNewItem(newTicket);
					}
				}
				voucher = voucherFacade.save(voucher);
			} else {
				voucher = voucherFacade.findVoucherByItem(ticket);
				List<VoucherData> deletedData = voucherDataFacade.findVoucherDataByItem(ticket);
				voucher.deleteVoucherData(deletedData);
				voucher.setVoucherDate(ticket.getDate());
				voucherDatas = initVoucherDatas(ticket);
				for (VoucherData voucherData : voucherDatas) {
					voucher.addNewVoucherData(voucherData);
				}
				voucher = voucherFacade.save(voucher);
				ticket = ticketFacade.save(ticket);
			}
			if (redirect) {
				Faces.redirect(GeneralUtility.getOutcomeURL("ticket-list"));
			}
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			e.printStackTrace();
			GeneralUtility.showErrorMessage(e);
			System.err.println("D7noun: TicketController.save");
		}
	}

	public List<VoucherData> initVoucherDatas(Ticket ticket) {
		List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
		try {

			double totalCustomer = ticket.getTotal();
			double totalSupplier = (ticket.getSelling() - ticket.getProfit());
			if (isTicketRefund) {
			//	Ticket oldTicket = ticketFacade.findTicketByTicketNumberNotRefund(ticket.getTicketNumber());
				//if (oldTicket != null) {
					totalCustomer = ticket.getCancelationFees();
							//oldTicket.getTotal() - ticket.getCancelationFees() - ticket.getProfit();
					totalSupplier = ticket.getCancelationFees();
					//= oldTicket.getSelling() - oldTicket.getProfit() - ticket.getCancelationFees();
				//}
			}
			VoucherData firstVoucherData = new VoucherData();
			VoucherData secondVoucherData = new VoucherData();
			VoucherData thirdVoucherData = new VoucherData();
			VoucherData fourthVoucherData = new VoucherData();
			firstVoucherData.setEntryDescription("Ticket Number: " + ticket.getTicketNumber());
			secondVoucherData.setEntryDescription("Ticket Number: " + ticket.getTicketNumber());
			thirdVoucherData.setEntryDescription("Ticket Number: " + ticket.getTicketNumber());
			fourthVoucherData.setEntryDescription("Ticket Number: " + ticket.getTicketNumber());
			firstVoucherData.setItem(ticket);
			secondVoucherData.setItem(ticket);
			thirdVoucherData.setItem(ticket);
			fourthVoucherData.setItem(ticket);
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			firstVoucherData.setDc(DC.D);
			firstVoucherData.setAccount(ticket.getFrom());
			firstVoucherData.setUsdEquivalent(totalCustomer);
			firstVoucherData.setLlEquivalent(totalCustomer * 1507.5);
			if (ticket.getFrom().getCurrency() == Currency.DOLAR) {
				firstVoucherData.setAcAmount(totalCustomer);
			} else {
				firstVoucherData.setAcAmount(totalCustomer * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchasePurchase = salesPurchaseFacade.getByTypeAndSP(SP.S, "Ticket");
			secondVoucherData.setDc(DC.C);
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null) {
				Account account = salesPurchasePurchase.getAccount();
				secondVoucherData.setAccount(account);
				secondVoucherData.setUsdEquivalent(totalCustomer);
				secondVoucherData.setLlEquivalent(totalCustomer * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					secondVoucherData.setAcAmount(totalCustomer);
				} else {
					secondVoucherData.setAcAmount(totalCustomer * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			thirdVoucherData.setDc(DC.C);
			thirdVoucherData.setAccount(ticket.getTo());
			thirdVoucherData.setUsdEquivalent(totalSupplier);
			thirdVoucherData.setLlEquivalent(totalSupplier * 1507.5);
			if (ticket.getTo().getCurrency() == Currency.DOLAR) {
				thirdVoucherData.setAcAmount(totalSupplier);
			} else {
				thirdVoucherData.setAcAmount(totalSupplier * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchaseSales = salesPurchaseFacade.getByTypeAndSP(SP.P, "Ticket");
			fourthVoucherData.setDc(DC.D);
			if (salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				Account account = salesPurchaseSales.getAccount();
				fourthVoucherData.setAccount(account);
				fourthVoucherData.setUsdEquivalent(totalSupplier);
				fourthVoucherData.setLlEquivalent(totalSupplier * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					fourthVoucherData.setAcAmount(totalSupplier);
				} else {
					fourthVoucherData.setAcAmount(totalSupplier * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			if (isTicketRefund) {
				firstVoucherData.setDc(DC.C);
				secondVoucherData.setDc(DC.D);
				thirdVoucherData.setDc(DC.D);
				fourthVoucherData.setDc(DC.C);
			}
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null
					&& salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				voucherDatas.add(firstVoucherData);
				voucherDatas.add(secondVoucherData);
				voucherDatas.add(thirdVoucherData);
				voucherDatas.add(fourthVoucherData);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: TicketController.initVoucherDatas");
		}
		return voucherDatas;
	}

	public String voucherHeaderId() {
		if (ticket == null || ticket.getVoucher() == null) {
			return "No Voucher";
		} else {
			return String.valueOf(ticket.getVoucher().getVoucherNumber());
		}
	}

	public void printItem() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			ServletOutputStream servletOutputStream = response.getOutputStream();

			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			InputStream is = classloader.getResourceAsStream("item.jrxml");
			JasperReport report = JasperCompileManager.compileReport(is);

			Map<String, Object> parameters = new HashMap<>();

			List<ItemDto> dtos = new ArrayList<>();

			ItemDto itemDto = new ItemDto();
			itemDto.setTax(String.valueOf(ticket.getTax()));
			itemDto.setAmount(String.valueOf(ticket.getTotal()));

			String description = "";
			description += "NAME: ";
			if (ticket.getPassenger() != null) {
				description += ticket.getPassenger() + "    /    ";
			} else {
				description += "EMPTY PASSENGER" + "    /    ";
			}
			description += "TKT: ";
			if (ticket.getTicketNumber() != null) {
				description += ticket.getTicketNumber() + "    /    ";
			} else {
				description += "NO TICKET NUMBER" + "    /    ";
			}
			description += "ISSUE DATE: ";
			if (ticket.getDate() != null) {
				description += getStringDateFromDate(ticket.getDate());
			} else {
				description += "NO DATE";
			}
			itemDto.setDescription(description);
			dtos.add(itemDto);

			String companyName = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.CompanyDescription);
			String addressLineOne = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.AddressLineOne);
			String addressLineTwo = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.AddressLineTwo);
			String addressLineThree = companyConfigurationFacade
					.getValueByKey(CompanyConfigurationTypes.AddressLineThree);
			String voucherNumber = companyConfigurationFacade
					.getValueByKey(CompanyConfigurationTypes.MOFRegistrationNo);
			if (companyName != null) {
				parameters.put("companyName", companyName);
			} else {
				parameters.put("companyName", "NO COMPANY NAME");
			}
			if (companyName != null) {
				parameters.put("address", addressLineOne);
			} else {
				parameters.put("address", "NO ADDRESS LINE ONE");
			}
			if (companyName != null) {
				parameters.put("phoneNumber", addressLineTwo);
			} else {
				parameters.put("phoneNumber", "NO ADDRESS LINE TWO");
			}
			if (companyName != null) {
				parameters.put("email", addressLineThree);
			} else {
				parameters.put("email", "NO ADDRESS LINE THREE");
			}
			if (companyName != null) {
				parameters.put("voucherNumber", voucherNumber);
			} else {
				parameters.put("voucherNumber", "NO MOF REGISTRATION NO");
			}
			///////////////////////////////
			///////////////////////////////
			///////////////////////////////
			parameters.put("customerName",
					ticket.getFrom() == null ? "NO CUSTOMER NAME" : ticket.getFrom().displayName());
			parameters.put("customerFinNumber", ticket.getFrom().getFinancialNumber() == null ? "NO FINANCIAL NUMBER"
					: ticket.getFrom().getFinancialNumber());
			parameters.put("customerPhoneNumber",
					ticket.getFrom().getPhoneNumber() == null ? "NO PHONE NUMBER" : ticket.getFrom().getPhoneNumber());
			parameters.put("customerAddress",
					ticket.getFrom().getTitle() == null ? "NO TITLE" : ticket.getFrom().getTitle());
			parameters.put("invoiceNumber", ticket.displayName());
			parameters.put("invoiceDate",
					ticket.getDate() == null ? "NO INVOICE DATE" : getStringDateFromDate(ticket.getDate()));
			parameters.put("salesMan", ticket.getSalesMan() == null ? "NO SALES MAN" : ticket.getSalesMan());
			parameters.put("branch", "1");
			parameters.put("tickets", String.valueOf(ticket.getTotal()));
			parameters.put("taxes", String.valueOf(ticket.getTax()));
			parameters.put("vat", String.valueOf(ticket.getTax()));
			parameters.put("insur", String.valueOf(ticket.getInsurance()));
			parameters.put("netUSD", String.valueOf(ticket.getTotal()));
			parameters.put("netLL", String.valueOf(ticket.getTotal() * 1507.5));
			parameters.put("only", CashWordConverter.doubleConvert(ticket.getTotal()));
			parameters.put("voucherDatas", dtos);

			byte[] bytes = null;
			bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource(1));

			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);

			servletOutputStream.write(bytes, 0, bytes.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the ticketFacade
	 */
	public TicketFacade getTicketFacade() {
		return ticketFacade;
	}

	/**
	 * @param ticketFacade the ticketFacade to set
	 */
	public void setTicketFacade(TicketFacade ticketFacade) {
		this.ticketFacade = ticketFacade;
	}

	/**
	 * @return the ticket
	 */
	public Ticket getTicket() {
		return ticket;
	}

	/**
	 * @param ticket the ticket to set
	 */
	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the customerFacade
	 */
	public CustomerFacade getCustomerFacade() {
		return customerFacade;
	}

	/**
	 * @param customerFacade the customerFacade to set
	 */
	public void setCustomerFacade(CustomerFacade customerFacade) {
		this.customerFacade = customerFacade;
	}

	/**
	 * @return the supplierFacade
	 */
	public SupplierFacade getSupplierFacade() {
		return supplierFacade;
	}

	/**
	 * @param supplierFacade the supplierFacade to set
	 */
	public void setSupplierFacade(SupplierFacade supplierFacade) {
		this.supplierFacade = supplierFacade;
	}

	/**
	 * @return the allCustomers
	 */
	public List<Customer> getAllCustomers() {
		return allCustomers;
	}

	/**
	 * @param allCustomers the allCustomers to set
	 */
	public void setAllCustomers(List<Customer> allCustomers) {
		this.allCustomers = allCustomers;
	}

	/**
	 * @return the allSuppliers
	 */
	public List<Supplier> getAllSuppliers() {
		return allSuppliers;
	}

	/**
	 * @param allSuppliers the allSuppliers to set
	 */
	public void setAllSuppliers(List<Supplier> allSuppliers) {
		this.allSuppliers = allSuppliers;
	}

	/**
	 * @return the numberOfItems
	 */
	public int getNumberOfItems() {
		return numberOfItems;
	}

	/**
	 * @param numberOfItems the numberOfItems to set
	 */
	public void setNumberOfItems(int numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	private String getStringDateFromDate(Date date) {
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(date);
	}

	public boolean isTicketRefund() {
		return isTicketRefund;
	}

	public void setTicketRefund(boolean isTicketRefund) {
		this.isTicketRefund = isTicketRefund;
	}

}
