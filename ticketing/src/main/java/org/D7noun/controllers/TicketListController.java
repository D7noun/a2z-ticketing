package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Named;

import org.D7noun.facades.TicketFacade;
import org.D7noun.facades.VoucherDataFacade;
import org.D7noun.models.VoucherData;
import org.D7noun.models.items.Ticket;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Ajax;

@Named
@ViewScoped
public class TicketListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private TicketFacade ticketFacade;
	@EJB
	private VoucherDataFacade voucherDataFacade;
	private List<Ticket> allTickets = new ArrayList<Ticket>();
	///////////////////////////////////////
	private Date fromDate;
	private Date toDate;

	///////////////////////////
	///////////////////////////
	///////////////////////////
	private Ticket ticketForDelete;
	///////////////////////////
	///////////////////////////
	///////////////////////////

	@PostConstruct
	public void init() {
		try {
			fromDate = new Date();
			toDate = new Date();
			allTickets = ticketFacade.findBetweenTwoDates(fromDate, toDate);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: TicketListController.init");
		}
	}

	public void search() {
		try {
			allTickets = ticketFacade.findBetweenTwoDates(fromDate, toDate);
			Ajax.oncomplete("PF('itemVar').filter()");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: LineTicketListController.search");
		}
	}

	public void delete(Ticket ticket) {
		List<VoucherData> voucherDatas = voucherDataFacade.findVoucherDataByItem(ticket);
		voucherDatas.forEach(vd -> {
			vd.setVoucher(null);
			vd.setItem(null);
		});
		voucherDataFacade.remove(voucherDatas);
		ticket.setVoucher(null);
		ticketFacade.remove(ticket);
		search();
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the ticketFacade
	 */
	public TicketFacade getTicketFacade() {
		return ticketFacade;
	}

	/**
	 * @param ticketFacade the ticketFacade to set
	 */
	public void setTicketFacade(TicketFacade ticketFacade) {
		this.ticketFacade = ticketFacade;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allTickets
	 */
	public List<Ticket> getAllTickets() {
		return allTickets;
	}

	/**
	 * @param allTickets the allTickets to set
	 */
	public void setAllTickets(List<Ticket> allTickets) {
		this.allTickets = allTickets;
	}

	/**
	 * @return the ticketForDelete
	 */
	public Ticket getTicketForDelete() {
		return ticketForDelete;
	}

	/**
	 * @param ticketForDelete the ticketForDelete to set
	 */
	public void setTicketForDelete(Ticket ticketForDelete) {
		this.ticketForDelete = ticketForDelete;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

}
