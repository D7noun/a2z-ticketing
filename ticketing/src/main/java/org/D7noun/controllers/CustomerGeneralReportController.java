package org.D7noun.controllers;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.dto.CustomerGeneralReportDto;
import org.D7noun.facades.AccountFacade;
import org.D7noun.facades.ItemFacade;
import org.D7noun.models.VoucherData;
import org.D7noun.models.VoucherData.DC;
import org.D7noun.models.accounts.Account;
import org.D7noun.models.accounts.Customer;
import org.D7noun.service.CustomerGeneralReportService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.omnifaces.util.Ajax;
import org.omnifaces.util.Faces;

@ManagedBean
@ViewScoped
public class CustomerGeneralReportController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private CustomerGeneralReportService customerGeneralReportService;
	@EJB
	private AccountFacade accountFacade;
	@EJB
	private ItemFacade itemFacade;
	private List<CustomerGeneralReportDto> reportData = new ArrayList<CustomerGeneralReportDto>();
	private Map<Account, Double> reportMap = new HashMap<>();
	private List<VoucherData> voucherDatas = new ArrayList<VoucherData>();

	private Date fromDate;
	private Date toDate;

	@PostConstruct
	public void init() {
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.MONTH, 0);
			fromDate = calendar.getTime();
			toDate = new Date();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CustomerGeneralReportController.init");
		}
	}

	public void search() {
		try {
			reportMap = new HashMap<>();
			reportData = new ArrayList<>();
			if (fromDate == null) {
				Calendar calendar = Calendar.getInstance();
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.set(Calendar.MONTH, 0);
				fromDate = calendar.getTime();
			}
			if (toDate == null) {
				toDate = new Date();
			}
			voucherDatas = customerGeneralReportService.getVoucherDatasBetweenDates(fromDate, toDate);
			Iterator<VoucherData> iterator = voucherDatas.iterator();
			while (iterator.hasNext()) {
				VoucherData voucherData = iterator.next();
				Account account = voucherData.getAccount();
				double amount = 0;
				if (reportMap.containsKey(account)) {
					amount = reportMap.get(account);
					if (voucherData.getDc() == DC.D) {
						amount += voucherData.getAcAmount();
					} else {
						amount -= voucherData.getAcAmount();
					}
				} else {
					if (voucherData.getDc() == DC.D) {
						amount = voucherData.getAcAmount();
					} else {
						amount = voucherData.getAcAmount() * -1;
					}
				}
				reportMap.put(account, amount);
			}

			for (Map.Entry<Account, Double> entry : reportMap.entrySet()) {
				reportData.add(new CustomerGeneralReportDto((Customer) entry.getKey(), entry.getValue()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CustomerGeneralReportController.search");
		}
		Ajax.oncomplete("PF('itemVar').filter()");
	}

	public void exportToExcel() {

		try {

			// Create a Workbook
			Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for
													// generating `.xls` file

			/*
			 * CreationHelper helps us create instances of various things like DataFormat,
			 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
			 */
			CreationHelper createHelper = workbook.getCreationHelper();

			// Create a Sheet
			Sheet sheet = workbook.createSheet("Customers");

			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());

			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setAlignment(HorizontalAlignment.RIGHT);

			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
			dateCellStyle.setAlignment(HorizontalAlignment.RIGHT);

			Row descriptionRow = sheet.createRow(0);

			Cell descReportText = descriptionRow.createCell(0);
			descReportText.setCellValue(GeneralUtility.getResourceBundleString("customer_general_report"));
			Cell descFromDateText = descriptionRow.createCell(1);
			descFromDateText.setCellValue(GeneralUtility.getResourceBundleString("from_date"));
			Cell descFromDate = descriptionRow.createCell(2);
			descFromDate.setCellValue(fromDate);
			descFromDate.setCellStyle(dateCellStyle);
			Cell descToDateText = descriptionRow.createCell(3);
			descToDateText.setCellValue(GeneralUtility.getResourceBundleString("to_date"));
			Cell descToDate = descriptionRow.createCell(4);
			descToDate.setCellValue(toDate);
			descToDate.setCellStyle(dateCellStyle);

			// Create a Row
			Row headerRow = sheet.createRow(2);
			Cell headerCustomer = headerRow.createCell(0);
			Cell headerValue = headerRow.createCell(1);
			Cell headerCRDR = headerRow.createCell(2);

			headerCustomer.setCellValue(GeneralUtility.getResourceBundleString("customer"));
			headerValue.setCellValue(GeneralUtility.getResourceBundleString("amount"));
			headerCRDR.setCellValue("DC");

			headerCustomer.setCellStyle(headerCellStyle);
			headerValue.setCellStyle(dateCellStyle);
			headerCRDR.setCellStyle(headerCellStyle);

			//////////////////////////////

			// Create Other rows and cells with employees data
			int rowNum = 3;
			for (CustomerGeneralReportDto customerGeneralReportDto : reportData) {
				Row row = sheet.createRow(rowNum++);

				Cell customer = row.createCell(0);
				Cell value = row.createCell(1);
				Cell crdr = row.createCell(2);

				customer.setCellValue(customerGeneralReportDto.getCustomer().displayName());
				value.setCellValue(customerGeneralReportDto.getPrice());
				crdr.setCellValue(customerGeneralReportDto.getDC());

			}

			// Resize all columns to fit the content size
			for (int i = 0; i < reportData.size(); i++) {
				sheet.autoSizeColumn(i);
			}

			sheet.setRightToLeft(true);

			// Write the output to a file
			ByteArrayOutputStream fileOut = new ByteArrayOutputStream();
			workbook.write(fileOut);

			Faces.sendFile(fileOut.toByteArray(), "customers_report.xlsx", true);

			fileOut.close();
			// Closing the workbook
			workbook.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: exportToExcel");
		}

	}

	/**
	 * @return the customerGeneralReportService
	 */
	public CustomerGeneralReportService getCustomerGeneralReportService() {
		return customerGeneralReportService;
	}

	/**
	 * @param customerGeneralReportService the customerGeneralReportService to set
	 */
	public void setCustomerGeneralReportService(CustomerGeneralReportService customerGeneralReportService) {
		this.customerGeneralReportService = customerGeneralReportService;
	}

	/**
	 * @return the accountFacade
	 */
	public AccountFacade getAccountFacade() {
		return accountFacade;
	}

	/**
	 * @param accountFacade the accountFacade to set
	 */
	public void setAccountFacade(AccountFacade accountFacade) {
		this.accountFacade = accountFacade;
	}

	/**
	 * @return the itemFacade
	 */
	public ItemFacade getItemFacade() {
		return itemFacade;
	}

	/**
	 * @param itemFacade the itemFacade to set
	 */
	public void setItemFacade(ItemFacade itemFacade) {
		this.itemFacade = itemFacade;
	}

	/**
	 * @return the reportData
	 */
	public List<CustomerGeneralReportDto> getReportData() {
		return reportData;
	}

	/**
	 * @param reportData the reportData to set
	 */
	public void setReportData(List<CustomerGeneralReportDto> reportData) {
		this.reportData = reportData;
	}

	/**
	 * @return the reportMap
	 */
	public Map<Account, Double> getReportMap() {
		return reportMap;
	}

	/**
	 * @param reportMap the reportMap to set
	 */
	public void setReportMap(Map<Account, Double> reportMap) {
		this.reportMap = reportMap;
	}

	/**
	 * @return the voucherDatas
	 */
	public List<VoucherData> getVoucherDatas() {
		return voucherDatas;
	}

	/**
	 * @param voucherDatas the voucherDatas to set
	 */
	public void setVoucherDatas(List<VoucherData> voucherDatas) {
		this.voucherDatas = voucherDatas;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}