package org.D7noun.controllers;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.dto.AccountReportDto;
import org.D7noun.dto.AccountReportDto.CRDR;
import org.D7noun.facades.AccountFacade;
import org.D7noun.facades.ItemFacade;
import org.D7noun.facades.VoucherFacade;
import org.D7noun.models.Voucher.VoucherType;
import org.D7noun.models.VoucherData;
import org.D7noun.models.VoucherData.DC;
import org.D7noun.models.accounts.Account;
import org.D7noun.models.accounts.Account.Currency;
import org.D7noun.service.AccountReportService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.omnifaces.util.Ajax;
import org.omnifaces.util.Faces;

@ManagedBean
@ViewScoped
public class AccountReportController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private AccountReportService accountReportService;
	@EJB
	private AccountFacade accountFacade;
	@EJB
	private ItemFacade itemFacade;
	@EJB
	private VoucherFacade voucherFacade;
	private List<AccountReportDto> reportData = new ArrayList<AccountReportDto>();
	private List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
	private List<Account> allAccounts = new ArrayList<Account>();

	private Account account;
	private Date fromDate;
	private Date toDate;
	private Currency currency;
	private double previousBalance = 0;
	private AccountReportDto previousAccountReport = new AccountReportDto();
	////////////////////////////////////
	////////////////////////////////////
	private double totalDr;
	private double totalCr;
	private CRDR totalCRDr;
	////////////////////////////////////
	////////////////////////////////////

	@PostConstruct
	public void init() {
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.MONTH, 0);
			fromDate = calendar.getTime();
			toDate = new Date();
			allAccounts = accountFacade.findAll();
			currency = Currency.DOLAR;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountReportController.init");
		}
	}

	public void search() {
		try {

			totalDr = 0;
			totalCr = 0;
			previousBalance = 0;
			reportData = new ArrayList<>();
			previousAccountReport = new AccountReportDto();
			List<VoucherData> previouseVouchers = accountReportService.getVouchersByAccountBeforeDate(account,
					fromDate);
			for (VoucherData voucherData : previouseVouchers) {
				if (currency == Currency.DOLAR) {
					if (voucherData.getDc() == DC.D) {
						previousBalance += voucherData.getUsdEquivalent();
					} else {
						previousBalance -= voucherData.getUsdEquivalent();
					}
				} else {
					if (voucherData.getDc() == DC.D) {
						previousBalance += voucherData.getLlEquivalent();
					} else {
						previousBalance -= voucherData.getLlEquivalent();
					}
				}
			}
			previousAccountReport.setVoucherInfo(GeneralUtility.getResourceBundleString("previous_balance"));
			if (previousBalance > 0) {
				previousAccountReport.setCrdr(CRDR.DR);
				previousAccountReport.setDebit(previousBalance);
				previousAccountReport.setBalance(previousBalance);
				totalDr += previousAccountReport.getDebit();
			} else {
				previousAccountReport.setCrdr(CRDR.CR);
				previousAccountReport.setCredit(previousBalance * -1);
				previousAccountReport.setBalance(previousBalance * -1);
				totalCr += previousAccountReport.getCredit();
			}
			if (account != null) {
				voucherDatas = accountReportService.findVouchersByAccountsAndDates(account, fromDate, toDate);
				Iterator<VoucherData> iterator = voucherDatas.iterator();
				while (iterator.hasNext()) {
					VoucherData voucherData = iterator.next();
					AccountReportDto accountReportDto = new AccountReportDto();
					/////////////////////////////////
					/////////////////////////////////
					accountReportDto.setVoucherDate(voucherData.getVoucherDate());
					accountReportDto.setEntryDescription(voucherData.getEntryDescription());
					accountReportDto.setVoucherNumber(voucherData.getVoucherNumber());
					accountReportDto.setVoucherType(voucherData.getVoucherType());
					accountReportDto
							.setVoucherInfo(voucherData.getVoucherType().name() + " " + voucherData.getVoucherNumber());

					if (currency == Currency.DOLAR) {
						if (voucherData.getDc() == DC.D) {
							accountReportDto.setDebit(voucherData.getUsdEquivalent());
						} else {
							accountReportDto.setCredit(voucherData.getUsdEquivalent() * -1);
						}
					} else {
						if (voucherData.getDc() == DC.D) {
							accountReportDto.setDebit(voucherData.getLlEquivalent());
						} else {
							accountReportDto.setCredit(voucherData.getLlEquivalent() * -1);
						}
					}

					/////////////////////////////////
					/////////////////////////////////
					reportData.add(accountReportDto);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountReportController.search");
		}
		Collections.sort(reportData, new Comparator<AccountReportDto>() {
			@Override
			public int compare(AccountReportDto o1, AccountReportDto o2) {
				if (o1.getVoucherDate() == null) {
					return 1;
				}
				if (o2.getVoucherDate() == null) {
					return -1;
				}
				return o1.getVoucherDate().compareTo(o2.getVoucherDate());
			}
		});

		double balance = previousBalance;
		double balanceView = 0;
		for (AccountReportDto accountReportDto : reportData) {
			if (accountReportDto.getDebit() > 0) {
				balance = balance + accountReportDto.getDebit();
				if (balance < 0) {
					balanceView = balance * -1;
					accountReportDto.setCrdr(CRDR.CR);
				} else {
					balanceView = balance;
					accountReportDto.setCrdr(CRDR.DR);
				}
				accountReportDto.setBalance(balanceView);
				totalDr += accountReportDto.getDebit();
			} else {
				balance = balance - accountReportDto.getCredit();
				if (balance < 0) {
					balanceView = balance * -1;
					accountReportDto.setCrdr(CRDR.CR);
				} else {
					balanceView = balance;
					accountReportDto.setCrdr(CRDR.DR);
				}
				accountReportDto.setBalance(balanceView);
				totalCr += accountReportDto.getCredit();
			}
		}
		Ajax.oncomplete("PF('itemVar').filter()");
		Ajax.update("itemsDataTable");
	}

	public void exportToExcel() {

		try {

			// Create a Workbook
			Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for
													// generating `.xls` file

			/*
			 * CreationHelper helps us create instances of various things like DataFormat,
			 * Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way
			 */
			CreationHelper createHelper = workbook.getCreationHelper();

			// Create a Sheet
			Sheet sheet = workbook.createSheet("Accounts");

			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());

			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setAlignment(HorizontalAlignment.RIGHT);

			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
			dateCellStyle.setAlignment(HorizontalAlignment.RIGHT);

			Row descriptionRow = sheet.createRow(0);
			Cell descAccountName = descriptionRow.createCell(0);
			descAccountName.setCellValue(account.displayName());
			descAccountName.setCellStyle(headerCellStyle);

			Cell descFromDateText = descriptionRow.createCell(2);
			descFromDateText.setCellValue(GeneralUtility.getResourceBundleString("from_date"));
			Cell descFromDate = descriptionRow.createCell(3);
			descFromDate.setCellValue(fromDate);
			descFromDate.setCellStyle(dateCellStyle);
			Cell descToDateText = descriptionRow.createCell(4);
			descToDateText.setCellValue(GeneralUtility.getResourceBundleString("to_date"));
			Cell descToDate = descriptionRow.createCell(5);
			descToDate.setCellValue(toDate);
			descToDate.setCellStyle(dateCellStyle);

			// Create a Row
			Row headerRow = sheet.createRow(2);
			Cell headerVoucherInfo = headerRow.createCell(0);
			Cell headerDate = headerRow.createCell(1);
			Cell headerEntryDescription = headerRow.createCell(2);
			Cell headerDR = headerRow.createCell(3);
			Cell headerCR = headerRow.createCell(4);
			Cell headerBalance = headerRow.createCell(5);
			Cell headerType = headerRow.createCell(6);

			headerVoucherInfo.setCellValue(GeneralUtility.getResourceBundleString("voucher_info"));
			headerDate.setCellValue(GeneralUtility.getResourceBundleString("date"));
			headerEntryDescription.setCellValue(GeneralUtility.getResourceBundleString("entry_description"));
			headerDR.setCellValue("DR");
			headerCR.setCellValue("CR");
			headerBalance.setCellValue("Balance");
			headerType.setCellValue("CR/DR");

			headerVoucherInfo.setCellStyle(headerCellStyle);
			headerDate.setCellStyle(headerCellStyle);
			headerEntryDescription.setCellStyle(headerCellStyle);
			headerDR.setCellStyle(headerCellStyle);
			headerCR.setCellStyle(headerCellStyle);
			headerBalance.setCellStyle(headerCellStyle);
			headerType.setCellStyle(headerCellStyle);

			//////////////////////////////

			// Create Other rows and cells with employees data
			int rowNum = 3;
			for (AccountReportDto accountReportDto : reportData) {
				Row row = sheet.createRow(rowNum++);

				Cell voucherInfo = row.createCell(0);
				Cell date = row.createCell(1);
				Cell entryDecription = row.createCell(2);
				Cell dr = row.createCell(3);
				Cell cr = row.createCell(4);
				Cell balance = row.createCell(5);
				Cell crdr = row.createCell(6);

				voucherInfo.setCellValue(accountReportDto.getVoucherInfo());
				date.setCellValue(accountReportDto.getVoucherDate());
				date.setCellStyle(dateCellStyle);
				entryDecription.setCellValue(accountReportDto.getEntryDescription());
				dr.setCellValue(accountReportDto.getDebit());
				cr.setCellValue(accountReportDto.getCredit());
				balance.setCellValue(accountReportDto.getBalance());
				if (accountReportDto.getCrdr() != null) {
					crdr.setCellValue(accountReportDto.getCrdr().toString());
				} else {
					crdr.setCellValue("");
				}

			}

			// Resize all columns to fit the content size
			for (int i = 0; i < reportData.size(); i++) {
				sheet.autoSizeColumn(i);
			}

			sheet.setRightToLeft(true);

			// Write the output to a file
			ByteArrayOutputStream fileOut = new ByteArrayOutputStream();
			workbook.write(fileOut);

			Faces.sendFile(fileOut.toByteArray(), "accounts_report.xlsx", true);

			fileOut.close();
			// Closing the workbook
			workbook.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: exportToExcel");
		}

	}

	public long getVoucherId(long voucherNumber, VoucherType voucherType) {
		long voucherId = voucherFacade.getVoucherIdFromNumberAndType(voucherNumber, voucherType);
		return voucherId;
	}

	/**
	 * @return the reportData
	 */
	public List<AccountReportDto> getReportData() {
		return reportData;
	}

	/**
	 * @param reportData the reportData to set
	 */
	public void setReportData(List<AccountReportDto> reportData) {
		this.reportData = reportData;
	}

	/**
	 * @return the voucherDatas
	 */
	public List<VoucherData> getVoucherDatas() {
		return voucherDatas;
	}

	/**
	 * @param voucherDatas the voucherDatas to set
	 */
	public void setVoucherDatas(List<VoucherData> voucherDatas) {
		this.voucherDatas = voucherDatas;
	}

	/**
	 * @return the accountReportService
	 */
	public AccountReportService getAccountReportService() {
		return accountReportService;
	}

	/**
	 * @param accountReportService the accountReportService to set
	 */
	public void setAccountReportService(AccountReportService accountReportService) {
		this.accountReportService = accountReportService;
	}

	/**
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the currency
	 */
	public Currency getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	/**
	 * @return the allAccounts
	 */
	public List<Account> getAllAccounts() {
		return allAccounts;
	}

	/**
	 * @param allAccounts the allAccounts to set
	 */
	public void setAllAccounts(List<Account> allAccounts) {
		this.allAccounts = allAccounts;
	}

	public String getTotalDr() {
		return String.format("%.2f", totalDr);
	}

	public void setTotalDr(double totalDr) {
		this.totalDr = totalDr;
	}

	public String getTotalCr() {
		return String.format("%.2f", totalCr);
	}

	public void setTotalCr(double totalCr) {
		this.totalCr = totalCr;
	}

	public String getTotalBalance() {
		double balance = totalDr - totalCr;
		if (balance > 0) {
			setTotalCRDr(CRDR.DR);
			return String.format("%.2f", balance);
		} else {
			setTotalCRDr(CRDR.CR);
			return String.format("%.2f", -1 * balance);
		}
	}

	public CRDR getTotalCRDr() {
		return totalCRDr;
	}

	public void setTotalCRDr(CRDR totalCRDr) {
		this.totalCRDr = totalCRDr;
	}

	public double getPreviousBalance() {
		return previousBalance;
	}

	public void setPreviousBalance(double previousBalance) {
		this.previousBalance = previousBalance;
	}

	public AccountReportDto getPreviousAccountReport() {
		return previousAccountReport;
	}

	public void setPreviousAccountReport(AccountReportDto previousAccountReport) {
		this.previousAccountReport = previousAccountReport;
	}

}