package org.D7noun.controllers;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.AccountConfigurationFacade;
import org.D7noun.facades.AccountFacade;
import org.D7noun.facades.AccountSerialFacade;
import org.D7noun.facades.SupplierFacade;
import org.D7noun.models.AccountConfiguration;
import org.D7noun.models.AccountSerial;
import org.D7noun.models.AccountConfiguration.AccountTypes;
import org.D7noun.models.AccountTree;
import org.D7noun.models.accounts.Bank;
import org.D7noun.models.accounts.Supplier;
import org.omnifaces.util.Faces;

@ManagedBean
@ViewScoped
public class SupplierController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private SupplierFacade supplierFacade;
	@EJB
	private AccountConfigurationFacade accountConfigurationFacade;
	@EJB
	private AccountFacade accountFacade;
	@EJB
	private AccountSerialFacade accountSerialFacade;
	private Supplier supplier;

	private String id;

	private boolean canEdit = true;

	@PostConstruct
	public void init() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: SupplierController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				if (id != null) {
					supplier = supplierFacade.find(Long.parseLong(id));
				} else {
					supplier = new Supplier();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: SupplierController.preRenderView");
		}
	}

	public void save() {
		try {
			boolean redirect = false;
			/////////////////////////
			/////////////////////////
			AccountConfiguration accountConfiguration = accountConfigurationFacade
					.getAccountConfigurationByAccountType(AccountTypes.Suppliers.toString());
			AccountTree accountTree = accountConfiguration.getAccountTree();
			supplier.setType(accountTree.getCode());
			/////////////////////////
			/////////////////////////
			if (supplier.getId() == null || supplier.getId() <= 0) {
				redirect = true;
				AccountSerial accountSerial = accountSerialFacade.getByAccountCodeAndCurrency(accountTree.getCode(),
						supplier.getCurrency());

				long accountId = 0;
				if (accountSerial == null) {
					accountSerial = new AccountSerial();
					accountSerial.setAccountCode(accountTree.getCode());
					accountSerial.setCurrency(supplier.getCurrency());
				} else {
					accountId = accountSerial.getValue();
				}
				boolean reservedId = true;
				while (reservedId) {
					Bank reserved = (Bank) accountFacade.getAccountByAccountIdAndCodeAndCurrency(accountTree.getCode(),
							++accountId, supplier.getCurrency());
					if (reserved == null) {
						reservedId = false;
					}
				}
				accountSerial.setValue(accountId);
				supplier.setAccountId(accountId);
				accountSerial = accountSerialFacade.save(accountSerial);
			}
			supplier = supplierFacade.save(supplier);
			if (redirect) {
				Faces.redirect(GeneralUtility.getOutcomeURL("supplier-card") + "?id=%s",
						Long.toString(supplier.getId()));
			}
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: SupplierController.save");
		}
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the supplierFacade
	 */
	public SupplierFacade getSupplierFacade() {
		return supplierFacade;
	}

	/**
	 * @param supplierFacade the supplierFacade to set
	 */
	public void setSupplierFacade(SupplierFacade supplierFacade) {
		this.supplierFacade = supplierFacade;
	}

	/**
	 * @return the supplier
	 */
	public Supplier getSupplier() {
		return supplier;
	}

	/**
	 * @param supplier the supplier to set
	 */
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

}
