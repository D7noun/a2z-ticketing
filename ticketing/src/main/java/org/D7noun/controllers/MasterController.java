package org.D7noun.controllers;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.CurrencyConfigurationFacade;
import org.D7noun.models.CurrencyArchive;
import org.omnifaces.util.Faces;

@ManagedBean
@SessionScoped
public class MasterController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6915019052885724860L;

	@EJB
	private CurrencyConfigurationFacade currencyConfigurationFacade;

	private boolean rtl = true;
	private Locale locale;

	private String currentUser;
	private String oldPassword;
	private String newPassword;
	private String confirmPassword;

	private String version;
	private String tranasctionConfirmUser;
	private int liraPerDolar;

	@PostConstruct
	private void init() {
		locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
		if (locale == null) {
			locale = new Locale("ar");
		}
		liraPerDolar = currencyConfigurationFacade.getLiraByDate(new Date());
		if (liraPerDolar == 0) {
			liraPerDolar = 1507;
		}
		FacesContext faceContext = FacesContext.getCurrentInstance();
		if (faceContext != null) {
			currentUser = faceContext.getExternalContext().getRemoteUser();
		} else {
			currentUser = "Unknown";
		}
	}

	public void acceptCurrency() {
		CurrencyArchive currencyArchive = new CurrencyArchive();
		currencyArchive.setDate(new Date());
		currencyArchive.setLiraPerDolar(liraPerDolar);
		currencyConfigurationFacade.save(currencyArchive);
	}

	public String logout() {
		try {
			Faces.logout();
			Faces.invalidateSession();
			return "home";
		} catch (ServletException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void indexRedirect() throws IOException {
		String path = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
				+ "/view/index.xhtml";
		Faces.redirect(path);
	}

	public String getOutcomeURL(String outcome) {
		return GeneralUtility.getOutcomeURL(outcome);
	}

	public String getLanguage() {
		return locale.getLanguage();
	}

	public void setLanguage(String language) {
		locale = new Locale(language);
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
	}

	public void switchLanguage(String language) {
		locale = new Locale(language);
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
		rtl = (language.equals("ar"));
	}

	/**
	 * @return the rtl
	 */
	public boolean isRtl() {
		return rtl;
	}

	/**
	 * @param rtl the rtl to set
	 */
	public void setRtl(boolean rtl) {
		this.rtl = rtl;
	}

	/**
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * @return the oldPassword
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * @param oldPassword the oldPassword to set
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * @param confirmPassword the confirmPassword to set
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the tranasctionConfirmUser
	 */
	public String getTranasctionConfirmUser() {
		return tranasctionConfirmUser;
	}

	/**
	 * @param tranasctionConfirmUser the tranasctionConfirmUser to set
	 */
	public void setTranasctionConfirmUser(String tranasctionConfirmUser) {
		this.tranasctionConfirmUser = tranasctionConfirmUser;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the liraPerDolar
	 */
	public int getLiraPerDolar() {
		return liraPerDolar;
	}

	/**
	 * @param liraPerDolar the liraPerDolar to set
	 */
	public void setLiraPerDolar(int liraPerDolar) {
		this.liraPerDolar = liraPerDolar;
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}

}
