package org.D7noun.controllers;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.service.ImportService;
import org.primefaces.event.FileUploadEvent;

@ManagedBean
@ViewScoped
public class ImportFromExcelController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private ImportService importService;

	@PostConstruct
	public void init() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void importTicketsFromExcel(FileUploadEvent event) {
		try {
			importService.importTicketFromExcel(event.getFile().getInputstream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void importBillFromExcel(FileUploadEvent event) {
		try {
			importService.importBillFromExcel(event.getFile().getInputstream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void importRvFromExcel(FileUploadEvent event) {
		try {
			importService.importRvFromExcel(event.getFile().getInputstream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void importPvFromExcel(FileUploadEvent event) {
		try {
			importService.importPvFromExcel(event.getFile().getInputstream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void importJvFromExcel(FileUploadEvent event) {
		try {
			importService.importJvFromExcel(event.getFile().getInputstream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
