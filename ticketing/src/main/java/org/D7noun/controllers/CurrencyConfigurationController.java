package org.D7noun.controllers;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.CurrencyConfigurationFacade;
import org.D7noun.models.CurrencyArchive;
import org.omnifaces.util.Faces;

@ManagedBean
@ViewScoped
public class CurrencyConfigurationController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private CurrencyConfigurationFacade currencyConfigurationFacade;

	private List<CurrencyArchive> currencyArchives = new ArrayList<CurrencyArchive>();

	private boolean canEdit = true;

	@PostConstruct
	public void init() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				currencyArchives = currencyConfigurationFacade.findAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void save() {
		try {
			currencyConfigurationFacade.save(currencyArchives);
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			GeneralUtility.showErrorMessage("save_error");
			e.printStackTrace();
		}
	}

	public boolean canEditCurrency(Date date) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			String currentDateString = formatter.format(new Date());
			Date currentDate = formatter.parse(currentDateString);
			if (!date.equals(currentDate)) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @return the currencyConfigurationFacade
	 */
	public CurrencyConfigurationFacade getCurrencyConfigurationFacade() {
		return currencyConfigurationFacade;
	}

	/**
	 * @param currencyConfigurationFacade the currencyConfigurationFacade to set
	 */
	public void setCurrencyConfigurationFacade(CurrencyConfigurationFacade currencyConfigurationFacade) {
		this.currencyConfigurationFacade = currencyConfigurationFacade;
	}

	/**
	 * @return the currencyArchives
	 */
	public List<CurrencyArchive> getCurrencyArchives() {
		return currencyArchives;
	}

	/**
	 * @param currencyArchives the currencyArchives to set
	 */
	public void setCurrencyArchives(List<CurrencyArchive> currencyArchives) {
		this.currencyArchives = currencyArchives;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}