package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.facades.VoucherFacade;
import org.D7noun.models.Voucher;
import org.D7noun.models.Voucher.VoucherType;
import org.omnifaces.util.Ajax;
import org.omnifaces.util.Faces;

@ManagedBean
@ViewScoped
public class VoucherListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private VoucherFacade voucherFacade;
	private List<Voucher> allVouchers = new ArrayList<Voucher>();
	private List<Voucher> selectedVouchers = new ArrayList<Voucher>();

	///////////////////////////////////////
	private Date fromDate;
	private Date toDate;
	private VoucherType voucherType;

	///////////////////////////
	///////////////////////////
	///////////////////////////

	public void init() {
		try {
			if (!Faces.isPostback()) {
				fromDate = new Date();
				toDate = new Date();
				allVouchers = voucherFacade.getAllBetweenTwoDates(fromDate, toDate);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void search() {
		try {
			if (voucherType != null) {
				allVouchers = voucherFacade.getAllBetweenTwoDatesByType(fromDate, toDate, voucherType);
			} else {
				allVouchers = voucherFacade.getAllBetweenTwoDates(fromDate, toDate);
			}
			Ajax.oncomplete("PF('itemVar').filter()");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void delete(Voucher voucher) {
		voucherFacade.remove(voucher);
		allVouchers = voucherFacade.getAllBetweenTwoDates(fromDate, toDate);
	}

	public void deleteList() {
		voucherFacade.remove(selectedVouchers);
		allVouchers = voucherFacade.getAllBetweenTwoDates(fromDate, toDate);
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the voucherFacade
	 */
	public VoucherFacade getVoucherFacade() {
		return voucherFacade;
	}

	/**
	 * @param voucherFacade the voucherFacade to set
	 */
	public void setVoucherFacade(VoucherFacade voucherFacade) {
		this.voucherFacade = voucherFacade;
	}

	/**
	 * @return the allVouchers
	 */
	public List<Voucher> getAllVouchers() {
		return allVouchers;
	}

	/**
	 * @param allVouchers the allVouchers to set
	 */
	public void setAllVouchers(List<Voucher> allVouchers) {
		this.allVouchers = allVouchers;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<Voucher> getSelectedVouchers() {
		return selectedVouchers;
	}

	public void setSelectedVouchers(List<Voucher> selectedVouchers) {
		this.selectedVouchers = selectedVouchers;
	}

	public VoucherType getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(VoucherType voucherType) {
		this.voucherType = voucherType;
	}

}
