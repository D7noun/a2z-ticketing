package org.D7noun.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.facades.CompanyConfigurationFacade;
import org.D7noun.models.CompanyConfiguration;

@ManagedBean
@ViewScoped
public class CompanyConfigurationController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private CompanyConfigurationFacade companyConfigurationFacade;
	private List<CompanyConfiguration> allCompanyConfiguration = new ArrayList<CompanyConfiguration>();

	@PostConstruct
	public void init() {
		try {
			allCompanyConfiguration = companyConfigurationFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: GeneralConfigurationController.init");
		}
	}

	public void save() {
		try {
			allCompanyConfiguration = companyConfigurationFacade.save(allCompanyConfiguration);
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			GeneralUtility.showErrorMessage(GeneralUtility.getResourceBundleString("save_error"));
			e.printStackTrace();
			System.err.println("D7noun: GeneralConfigurationController.save");
		}
	}

	public List<CompanyConfiguration> getAllCompanyConfiguration() {
		return allCompanyConfiguration;
	}

	public void setAllCompanyConfiguration(List<CompanyConfiguration> allCompanyConfiguration) {
		this.allCompanyConfiguration = allCompanyConfiguration;
	}

}