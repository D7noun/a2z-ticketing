package org.D7noun.controllers;

import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.dto.ItemDto;
import org.D7noun.facades.CompanyConfigurationFacade;
import org.D7noun.facades.CurrencyConfigurationFacade;
import org.D7noun.facades.CustomerFacade;
import org.D7noun.facades.SalesPurchaseFacade;
import org.D7noun.facades.SupplierFacade;
import org.D7noun.facades.VisaFacade;
import org.D7noun.facades.VoucherDataFacade;
import org.D7noun.facades.VoucherFacade;
import org.D7noun.models.CompanyConfiguration.CompanyConfigurationTypes;
import org.D7noun.models.SalesPurchase;
import org.D7noun.models.SalesPurchase.SP;
import org.D7noun.models.Voucher;
import org.D7noun.models.Voucher.VoucherType;
import org.D7noun.models.VoucherData;
import org.D7noun.models.VoucherData.DC;
import org.D7noun.models.accounts.Account;
import org.D7noun.models.accounts.Account.Currency;
import org.D7noun.models.accounts.Customer;
import org.D7noun.models.accounts.Supplier;
import org.D7noun.models.items.Visa;
import org.D7noun.service.CashWordConverter;
import org.omnifaces.util.Faces;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;

@ManagedBean
@ViewScoped
public class VisaController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private VisaFacade visaFacade;
	@EJB
	private CustomerFacade customerFacade;
	@EJB
	private SupplierFacade supplierFacade;
	@EJB
	private VoucherFacade voucherFacade;
	@EJB
	private VoucherDataFacade voucherDataFacade;
	@EJB
	private SalesPurchaseFacade salesPurchaseFacade;
	@EJB
	private CurrencyConfigurationFacade currencyConfigurationFacade;
	@EJB
	private CompanyConfigurationFacade companyConfigurationFacade;
	///////////////////////////////////////
	///////////////////////////////////////
//	private int 1507.5;
	//////////////////////////////////
	//////////////////////////////////

	private List<Customer> allCustomers = new ArrayList<Customer>();
	private List<Supplier> allSuppliers = new ArrayList<Supplier>();

	///////////////////////////////////////
	///////////////////////////////////////
	private Visa visa;

	private String id;

	private boolean canEdit = true;

	@PostConstruct
	public void init() {
		try {
			allCustomers = customerFacade.findAll();
			allSuppliers = supplierFacade.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VisaController.init");
		}
	}

	public void preRenderView() {
		try {
			if (!Faces.isPostback()) {
				if (id != null) {
					visa = visaFacade.find(Long.parseLong(id));
				} else {
					visa = new Visa();
					visa.setQuantity(1);
				}
//				1507.5 = currencyConfigurationFacade.getLiraByDate(new Date());
//				if (1507.5 == 0) {
//					1507.5 = 1507;
//				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VisaController.preRenderView");
		}
	}

	public void updateFromDiscountAmount() {
		double total = (visa.getSelling() - visa.getDiscountAmount()) * visa.getQuantity();
		int dicountPercentage = 100 - ((int) ((total * 100) / visa.getSelling()));
		visa.setTotal(total);
		visa.setDiscountPercentage(dicountPercentage);
	}

	public void updateSellingAndTotal() {
		double selling = (visa.getCost() + visa.getProfit() + visa.getTax()) * visa.getQuantity();
		double total = (selling * (100 - visa.getDiscountPercentage())) / 100;
		double discountAmount = total - selling;
		visa.setDiscountAmount(discountAmount * -1);
		visa.setSelling(selling);
		visa.setTotal(total);
	}

	public void save() {
		try {
			boolean redirect = false;
			Voucher voucher = new Voucher();
			List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
			if (visa.getId() == null || visa.getId() <= 0) {
				redirect = true;
				///////////////////////////////
				voucher.setVoucherType(VoucherType.VS);
				voucher.setVoucherDate(visa.getDate());
				long maxVoucherNumber = voucherFacade.getMaxVoucherNumberByType(voucher.getVoucherType());
				voucher.setVoucherNumber(++maxVoucherNumber);
				voucherDatas = initVoucherDatas(visa);
				if (voucherDatas != null && !voucherDatas.isEmpty()) {
					for (VoucherData voucherData : voucherDatas) {
						voucher.addNewVoucherData(voucherData);
					}
					voucher.addNewItem(visa);
				}
				voucher = voucherFacade.save(voucher);
			} else {
				voucher = voucherFacade.findVoucherByItem(visa);
				List<VoucherData> deletedData = voucherDataFacade.findVoucherDataByItem(visa);
				voucher.deleteVoucherData(deletedData);
				voucher.setVoucherDate(visa.getDate());
				voucherDatas = initVoucherDatas(visa);
				for (VoucherData voucherData : voucherDatas) {
					voucher.addNewVoucherData(voucherData);
				}
				voucher = voucherFacade.save(voucher);
				visa = visaFacade.save(visa);
			}
			if (redirect) {
				Faces.redirect(GeneralUtility.getOutcomeURL("visa-list"));
			}
			GeneralUtility.showSaveMessage();
		} catch (Exception e) {
			e.printStackTrace();
			GeneralUtility.showErrorMessage(e);
			System.err.println("D7noun: VisaController.save");
		}
	}

	public List<VoucherData> initVoucherDatas(Visa visa) {
		List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
		try {

			double totalCustomer = visa.getTotal();
			double totalSupplier = (visa.getSelling() - visa.getProfit() * visa.getQuantity());
			VoucherData firstVoucherData = new VoucherData();
			VoucherData secondVoucherData = new VoucherData();
			VoucherData thirdVoucherData = new VoucherData();
			VoucherData fourthVoucherData = new VoucherData();
			firstVoucherData.setItem(visa);
			secondVoucherData.setItem(visa);
			thirdVoucherData.setItem(visa);
			fourthVoucherData.setItem(visa);
			firstVoucherData.setEntryDescription(visa.getDescription());
			secondVoucherData.setEntryDescription(visa.getDescription());
			thirdVoucherData.setEntryDescription(visa.getDescription());
			fourthVoucherData.setEntryDescription(visa.getDescription());
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			firstVoucherData.setDc(DC.D);
			firstVoucherData.setAccount(visa.getFrom());
			firstVoucherData.setUsdEquivalent(totalCustomer);
			firstVoucherData.setLlEquivalent(totalCustomer * 1507.5);
			if (visa.getFrom().getCurrency() == Currency.DOLAR) {
				firstVoucherData.setAcAmount(totalCustomer);
			} else {
				firstVoucherData.setAcAmount(totalCustomer * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchasePurchase = salesPurchaseFacade.getByTypeAndSP(SP.S, "Visa");
			secondVoucherData.setDc(DC.C);
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null) {
				Account account = salesPurchasePurchase.getAccount();
				secondVoucherData.setAccount(account);
				secondVoucherData.setUsdEquivalent(totalCustomer);
				secondVoucherData.setLlEquivalent(totalCustomer * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					secondVoucherData.setAcAmount(totalCustomer);
				} else {
					secondVoucherData.setAcAmount(totalCustomer * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			thirdVoucherData.setDc(DC.C);
			thirdVoucherData.setAccount(visa.getTo());
			thirdVoucherData.setUsdEquivalent(totalSupplier);
			thirdVoucherData.setLlEquivalent(totalSupplier * 1507.5);
			if (visa.getTo().getCurrency() == Currency.DOLAR) {
				thirdVoucherData.setAcAmount(totalSupplier);
			} else {
				thirdVoucherData.setAcAmount(totalSupplier * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchaseSales = salesPurchaseFacade.getByTypeAndSP(SP.P, "Visa");
			fourthVoucherData.setDc(DC.D);
			if (salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				Account account = salesPurchaseSales.getAccount();
				fourthVoucherData.setAccount(account);
				fourthVoucherData.setUsdEquivalent(totalSupplier);
				fourthVoucherData.setLlEquivalent(totalSupplier * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					fourthVoucherData.setAcAmount(totalSupplier);
				} else {
					fourthVoucherData.setAcAmount(totalSupplier * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null
					&& salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				voucherDatas.add(firstVoucherData);
				voucherDatas.add(secondVoucherData);
				voucherDatas.add(thirdVoucherData);
				voucherDatas.add(fourthVoucherData);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VisaController.initVoucherDatas");
		}
		return voucherDatas;
	}

	public String voucherHeaderId() {
		if (visa == null || visa.getVoucher() == null) {
			return "No Voucher";
		} else {
			return String.valueOf(visa.getVoucher().getVoucherNumber());
		}
	}

	public void printItem() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			ServletOutputStream servletOutputStream = response.getOutputStream();

			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			InputStream is = classloader.getResourceAsStream("item.jrxml");
			JasperReport report = JasperCompileManager.compileReport(is);

			Map<String, Object> parameters = new HashMap<>();

			List<ItemDto> dtos = new ArrayList<>();

			ItemDto itemDto = new ItemDto();
			itemDto.setTax(String.valueOf(visa.getTax()));
			itemDto.setAmount(String.valueOf(visa.getTotal()));

			String description = "";
			if (visa.getDescription() != null) {
				description += visa.getDescription() + " / ";
			}
			description += "ISSUE DATE: ";
			if (visa.getDate() != null) {
				description += getStringDateFromDate(visa.getDate());
			} else {
				description += "NO DATE";
			}
			itemDto.setDescription(description);
			dtos.add(itemDto);

			String companyName = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.CompanyDescription);
			String addressLineOne = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.AddressLineOne);
			String addressLineTwo = companyConfigurationFacade.getValueByKey(CompanyConfigurationTypes.AddressLineTwo);
			String addressLineThree = companyConfigurationFacade
					.getValueByKey(CompanyConfigurationTypes.AddressLineThree);
			String voucherNumber = companyConfigurationFacade
					.getValueByKey(CompanyConfigurationTypes.MOFRegistrationNo);
			if (companyName != null) {
				parameters.put("companyName", companyName);
			} else {
				parameters.put("companyName", "NO COMPANY NAME");
			}
			if (companyName != null) {
				parameters.put("address", addressLineOne);
			} else {
				parameters.put("address", "NO ADDRESS LINE ONE");
			}
			if (companyName != null) {
				parameters.put("phoneNumber", addressLineTwo);
			} else {
				parameters.put("phoneNumber", "NO ADDRESS LINE TWO");
			}
			if (companyName != null) {
				parameters.put("email", addressLineThree);
			} else {
				parameters.put("email", "NO ADDRESS LINE THREE");
			}
			if (companyName != null) {
				parameters.put("voucherNumber", voucherNumber);
			} else {
				parameters.put("voucherNumber", "NO MOF REGISTRATION NO");
			}
			///////////////////////////////
			///////////////////////////////
			///////////////////////////////
			parameters.put("customerName", visa.getFrom() == null ? "NO CUSTOMER NAME" : visa.getFrom().displayName());
			parameters.put("customerFinNumber", visa.getFrom().getFinancialNumber() == null ? "NO FINANCIAL NUMBER"
					: visa.getFrom().getFinancialNumber());
			parameters.put("customerPhoneNumber",
					visa.getFrom().getPhoneNumber() == null ? "NO PHONE NUMBER" : visa.getFrom().getPhoneNumber());
			parameters.put("customerAddress",
					visa.getFrom().getTitle() == null ? "NO TITLE" : visa.getFrom().getTitle());
			parameters.put("invoiceNumber", visa.displayName());
			parameters.put("invoiceDate",
					visa.getDate() == null ? "NO INVOICE DATE" : getStringDateFromDate(visa.getDate()));
			parameters.put("salesMan", visa.getSalesMan() == null ? "NO SALES MAN" : visa.getSalesMan());
			parameters.put("branch", "1");
			parameters.put("visas", String.valueOf(visa.getTotal()));
			parameters.put("taxes", String.valueOf(visa.getTax()));
			parameters.put("vat", String.valueOf(visa.getTax()));
			parameters.put("insur", "NO INSURANCE");
			parameters.put("netUSD", String.valueOf(visa.getTotal()));
			parameters.put("netLL", String.valueOf(visa.getTotal() * 1507.5));
			parameters.put("only", CashWordConverter.doubleConvert(visa.getTotal()));
			parameters.put("voucherDatas", dtos);

			byte[] bytes = null;
			bytes = JasperRunManager.runReportToPdf(report, parameters, new JREmptyDataSource(1));

			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);

			servletOutputStream.write(bytes, 0, bytes.length);
			servletOutputStream.flush();
			servletOutputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * 
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 * 
	 * 
	 */

	/**
	 * @return the visaFacade
	 */
	public VisaFacade getVisaFacade() {
		return visaFacade;
	}

	/**
	 * @param visaFacade the visaFacade to set
	 */
	public void setVisaFacade(VisaFacade visaFacade) {
		this.visaFacade = visaFacade;
	}

	/**
	 * @return the visa
	 */
	public Visa getVisa() {
		return visa;
	}

	/**
	 * @param visa the visa to set
	 */
	public void setVisa(Visa visa) {
		this.visa = visa;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the canEdit
	 */
	public boolean isCanEdit() {
		return canEdit;
	}

	/**
	 * @param canEdit the canEdit to set
	 */
	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the customerFacade
	 */
	public CustomerFacade getCustomerFacade() {
		return customerFacade;
	}

	/**
	 * @param customerFacade the customerFacade to set
	 */
	public void setCustomerFacade(CustomerFacade customerFacade) {
		this.customerFacade = customerFacade;
	}

	/**
	 * @return the supplierFacade
	 */
	public SupplierFacade getSupplierFacade() {
		return supplierFacade;
	}

	/**
	 * @param supplierFacade the supplierFacade to set
	 */
	public void setSupplierFacade(SupplierFacade supplierFacade) {
		this.supplierFacade = supplierFacade;
	}

	/**
	 * @return the allCustomers
	 */
	public List<Customer> getAllCustomers() {
		return allCustomers;
	}

	/**
	 * @param allCustomers the allCustomers to set
	 */
	public void setAllCustomers(List<Customer> allCustomers) {
		this.allCustomers = allCustomers;
	}

	/**
	 * @return the allSuppliers
	 */
	public List<Supplier> getAllSuppliers() {
		return allSuppliers;
	}

	/**
	 * @param allSuppliers the allSuppliers to set
	 */
	public void setAllSuppliers(List<Supplier> allSuppliers) {
		this.allSuppliers = allSuppliers;
	}

	private String getStringDateFromDate(Date date) {
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(date);
	}

}
