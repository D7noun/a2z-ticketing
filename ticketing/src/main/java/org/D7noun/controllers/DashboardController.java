package org.D7noun.controllers;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.D7noun.models.accounts.Account.Currency;
import org.D7noun.service.DashboardService;

@Named
@ViewScoped
public class DashboardController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private DashboardService dashboardService;
	/**
	 * 
	 * 
	 * */
	private Date fromDate;
	private Date toDate;
	private Currency currency;

	/**
	 * 
	 * 
	 * */

	/**
	 * 
	 * 
	 * */

	@PostConstruct
	public void init() {
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.MONTH, 0);
			fromDate = calendar.getTime();
			toDate = new Date();
			currency = Currency.DOLAR;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: DashboardController.init");
		}
	}

	public void search() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: DashboardController.search");
		}
	}

	/*********
	 * 
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 * 
	 *********/

	/**
	 * @return the dashboardService
	 */
	public DashboardService getDashboardService() {
		return dashboardService;
	}

	/**
	 * @param dashboardService the dashboardService to set
	 */
	public void setDashboardService(DashboardService dashboardService) {
		this.dashboardService = dashboardService;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the currency
	 */
	public Currency getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

}
