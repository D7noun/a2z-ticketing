package org.D7noun.converter;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.D7noun.facades.AccountConfigurationFacade;
import org.D7noun.models.AccountConfiguration;

@ManagedBean
public class AccountConfigurationConverter implements Converter {

	/**
	 * 
	 */
	@EJB
	private AccountConfigurationFacade accountConfigurationFacade;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.isEmpty()) {
				return accountConfigurationFacade.find(Long.parseLong(value));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null && !value.equals("")) {
			AccountConfiguration accountConfiguration = (AccountConfiguration) value;
			return String.valueOf(accountConfiguration.getId());
		}
		return null;
	}

}
