package org.D7noun.converter;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.D7noun.facades.AccountTreeFacade;
import org.D7noun.models.AccountTree;

@ManagedBean
public class AccountTreeConverter implements Converter {

	/**
	 * 
	 */
	@EJB
	private AccountTreeFacade accountTreeFacade;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.isEmpty()) {
				return accountTreeFacade.find(Long.parseLong(value));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null && !value.equals("")) {
			AccountTree accountTree = (AccountTree) value;
			return String.valueOf(accountTree.getId());
		}
		return null;
	}

}
