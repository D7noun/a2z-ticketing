package org.D7noun.converter;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.D7noun.facades.AccountFacade;
import org.D7noun.models.accounts.Account;

@ManagedBean
public class AccountConverter implements Converter {

	/**
	 * 
	 */
	@EJB
	private AccountFacade accountFacade;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.isEmpty()) {
				return accountFacade.find(Long.parseLong(value));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null && !value.equals("")) {
			Account account = (Account) value;
			return String.valueOf(account.getId());
		}
		return null;
	}

}
