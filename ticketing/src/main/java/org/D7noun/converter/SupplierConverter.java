package org.D7noun.converter;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.D7noun.facades.SupplierFacade;
import org.D7noun.models.accounts.Supplier;

@ManagedBean
public class SupplierConverter implements Converter {

	/**
	 * 
	 */
	@EJB
	private SupplierFacade supplierFacade;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		try {
			if (value != null && !value.isEmpty()) {
				return supplierFacade.find(Long.parseLong(value));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null && !value.equals("")) {
			Supplier supplier = (Supplier) value;
			return String.valueOf(supplier.getId());
		}
		return null;
	}

}
