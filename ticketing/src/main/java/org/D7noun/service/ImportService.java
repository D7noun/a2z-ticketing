package org.D7noun.service;

import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.D7noun.facades.AccountConfigurationFacade;
import org.D7noun.facades.AccountFacade;
import org.D7noun.facades.AccountSerialFacade;
import org.D7noun.facades.CustomerFacade;
import org.D7noun.facades.SalesPurchaseFacade;
import org.D7noun.facades.SupplierFacade;
import org.D7noun.facades.TicketFacade;
import org.D7noun.facades.VoucherFacade;
import org.D7noun.models.SalesPurchase;
import org.D7noun.models.SalesPurchase.SP;
import org.D7noun.models.Voucher;
import org.D7noun.models.Voucher.VoucherType;
import org.D7noun.models.VoucherData;
import org.D7noun.models.VoucherData.DC;
import org.D7noun.models.accounts.Account;
import org.D7noun.models.accounts.Cash;
import org.D7noun.models.accounts.Account.Currency;
import org.D7noun.models.accounts.Bank;
import org.D7noun.models.accounts.Customer;
import org.D7noun.models.accounts.General;
import org.D7noun.models.accounts.Supplier;
import org.D7noun.models.items.Ticket;
import org.D7noun.models.items.Bill;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

@Stateless
public class ImportService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private TicketFacade ticketFacade;
	@EJB
	private AccountFacade accountFacade;
	@EJB
	private SupplierFacade supplierFacade;
	@EJB
	private CustomerFacade customerFacade;
	@EJB
	private VoucherFacade voucherFacade;
	@EJB
	private SalesPurchaseFacade salesPurchaseFacade;
	@EJB
	private AccountConfigurationFacade accountConfigurationFacade;
	@EJB
	private AccountSerialFacade accountSerialFacade;

	public void importTicketFromExcel(InputStream inputStream) {

		try {
			Workbook workbook = WorkbookFactory.create(inputStream);

			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();

			int rowCount = 0;
			int cellCount = 0;

			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {
				rowCount++;
				Voucher voucher = new Voucher();
				List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
				voucher.setVoucherType(VoucherType.TI);
				long maxVoucherNumber = voucherFacade.getMaxVoucherNumberByType(voucher.getVoucherType());
				voucher.setVoucherNumber(++maxVoucherNumber);
				Ticket ticket = new Ticket();
				ticket.setTicketPerVoucher(1);
				Row row = rowIterator.next();
				int lastColumn = Math.max(row.getLastCellNum(), 0);
				if (rowCount > 1) {
					cellCount = 0;
					for (int cn = 0; cn < lastColumn; cn++) {
						Cell cell = row.getCell(cn, MissingCellPolicy.RETURN_BLANK_AS_NULL);
						cellCount++;
						String cellValue = dataFormatter.formatCellValue(cell);
						switch (cellCount) {
						case 1:
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yy");
							ticket.setDate(simpleDateFormat.parse(cellValue));
							voucher.setVoucherDate(simpleDateFormat.parse(cellValue));
							break;
						case 2:
							Account supplier = accountFacade.getAccountByNameAndDtype(cellValue, "Supplier");
							ticket.setTo((Supplier) supplier);
							break;
						case 3:
							Account customer = accountFacade.getAccountByNameAndDtype(cellValue, "Customer");
							if (customer == null) {
								System.out.println();
							}
							ticket.setFrom((Customer) customer);
							break;
						case 4:
							ticket.setNotes(cellValue);
							break;
						case 5:
							ticket.setSalesMan(cellValue);
							break;
						case 6:
							ticket.setDiscountAmount(cellValue.isEmpty() ? 0 : Double.parseDouble(cellValue));
							break;
						case 7:
							ticket.setFair(cellValue.isEmpty() ? 0 : Double.parseDouble(cellValue));
							break;
						case 8:
							ticket.setTax(cellValue.isEmpty() ? 0 : Double.parseDouble(cellValue));
							break;
						case 9:
							ticket.setProfit(cellValue.isEmpty() ? 0 : Double.parseDouble(cellValue));
							break;
						case 10:
							ticket.setInsurance(cellValue.isEmpty() ? 0 : Double.parseDouble(cellValue));
							break;
						case 11:
							ticket.setCancelationFees(cellValue.isEmpty() ? 0 : Double.parseDouble(cellValue));
							break;
						case 12:
							ticket.setPassenger(cellValue);
							break;
						case 13:
							SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MM/dd/yy");
							ticket.setDateOfFlight(cellValue.isEmpty() ? null : simpleDateFormat1.parse(cellValue));
							break;
						case 14:
							ticket.setTicketNumber(cellValue);
							break;
						default:
							break;
						}
					}
					ticket = updateSellingAndTotal(ticket);
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					voucherDatas = initVoucherDatas(ticket);
					if (voucherDatas != null && !voucherDatas.isEmpty()) {
						for (VoucherData voucherData : voucherDatas) {
							voucher.addNewVoucherData(voucherData);
						}
						voucher.addNewItem(ticket);
					}
					voucher = voucherFacade.save(voucher);
				}
			}

			workbook.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void importBillFromExcel(InputStream inputStream) {

		try {
			Workbook workbook = WorkbookFactory.create(inputStream);

			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();

			int rowCount = 0;
			int cellCount = 0;

			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {
				rowCount++;
				Voucher voucher = new Voucher();
				List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
				voucher.setVoucherType(VoucherType.WB);
				long maxVoucherNumber = voucherFacade.getMaxVoucherNumberByType(voucher.getVoucherType());
				voucher.setVoucherNumber(++maxVoucherNumber);
				Bill bill = new Bill();
				Row row = rowIterator.next();
				int lastColumn = Math.max(row.getLastCellNum(), 0);
				if (rowCount > 1) {
					cellCount = 0;
					for (int cn = 0; cn < lastColumn; cn++) {
						Cell cell = row.getCell(cn, MissingCellPolicy.RETURN_BLANK_AS_NULL);
						cellCount++;
						String cellValue = dataFormatter.formatCellValue(cell);
						switch (cellCount) {
						case 1:
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yy");
							bill.setDate(simpleDateFormat.parse(cellValue));
							voucher.setVoucherDate(simpleDateFormat.parse(cellValue));
							break;
						case 2:
							Account supplier = accountFacade.getAccountByNameAndDtype(cellValue, "Supplier");
							bill.setTo((Supplier) supplier);
							break;
						case 3:
							Account customer = accountFacade.getAccountByNameAndDtype(cellValue, "Customer");
							if (customer == null) {
								System.out.println();
							}
							bill.setFrom((Customer) customer);
							break;
						case 4:
							bill.setNotes(cellValue);
							break;
						case 5:
							bill.setSalesMan(cellValue);
							break;
						case 7:
							bill.setFair(cellValue.isEmpty() ? 0 : Double.parseDouble(cellValue));
							break;
						case 8:
							bill.setTax(cellValue.isEmpty() ? 0 : Double.parseDouble(cellValue));
							break;
						case 9:
							bill.setProfit(cellValue.isEmpty() ? 0 : Double.parseDouble(cellValue));
							break;
						case 10:
							bill.setClearance(cellValue.isEmpty() ? 0 : Double.parseDouble(cellValue));
							break;
						case 14:
							bill.setNumber(cellValue.isEmpty() ? 0 : Long.parseLong(cellValue));
							break;
						case 15:
							bill.setDescription(cellValue);
							break;
						default:
							break;
						}
					}
					bill = updateBill(bill);
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					voucherDatas = initBillDatas(bill);
					if (voucherDatas != null && !voucherDatas.isEmpty()) {
						for (VoucherData voucherData : voucherDatas) {
							voucher.addNewVoucherData(voucherData);
						}
						voucher.addNewItem(bill);
					}
					voucher = voucherFacade.save(voucher);
				}
			}

			workbook.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private Ticket updateSellingAndTotal(Ticket ticket) {
		double selling = ticket.getFair() + ticket.getTax() + ticket.getProfit() + ticket.getInsurance();
		double total = selling - ticket.getDiscountAmount();
		int dicountPercentage = 100 - ((int) ((total * 100) / selling));
		ticket.setSelling(selling);
		ticket.setTotal(total);
		ticket.setDiscountPercentage(dicountPercentage);
		return ticket;
	}

	private Bill updateBill(Bill bill) {
		double supSelling = bill.getFair() + bill.getTax();
		double cusSelling = supSelling + bill.getProfit() + bill.getClearance();
		bill.setSupSelling(supSelling);
		bill.setCusSelling(cusSelling);
		return bill;
	}

	private List<VoucherData> initVoucherDatas(Ticket ticket) {
		List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
		try {

			double totalCustomer = ticket.getTotal();
			double totalSupplier = ticket.getSelling() - ticket.getProfit();
			VoucherData firstVoucherData = new VoucherData();
			VoucherData secondVoucherData = new VoucherData();
			VoucherData thirdVoucherData = new VoucherData();
			VoucherData fourthVoucherData = new VoucherData();
			firstVoucherData.setEntryDescription("Ticket Number: " + ticket.getTicketNumber());
			secondVoucherData.setEntryDescription("Ticket Number: " + ticket.getTicketNumber());
			thirdVoucherData.setEntryDescription("Ticket Number: " + ticket.getTicketNumber());
			fourthVoucherData.setEntryDescription("Ticket Number: " + ticket.getTicketNumber());
			firstVoucherData.setItem(ticket);
			secondVoucherData.setItem(ticket);
			thirdVoucherData.setItem(ticket);
			fourthVoucherData.setItem(ticket);
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			firstVoucherData.setDc(DC.D);
			firstVoucherData.setAccount(ticket.getFrom());
			firstVoucherData.setUsdEquivalent(totalCustomer);
			firstVoucherData.setLlEquivalent(totalCustomer * 1507.5);
			if (ticket.getFrom().getCurrency() == Currency.DOLAR) {
				firstVoucherData.setAcAmount(totalCustomer);
			} else {
				firstVoucherData.setAcAmount(totalCustomer * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchasePurchase = salesPurchaseFacade.getByTypeAndSP(SP.S, "Ticket");
			secondVoucherData.setDc(DC.C);
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null) {
				Account account = salesPurchasePurchase.getAccount();
				secondVoucherData.setAccount(account);
				secondVoucherData.setUsdEquivalent(totalCustomer);
				secondVoucherData.setLlEquivalent(totalCustomer * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					secondVoucherData.setAcAmount(totalCustomer);
				} else {
					secondVoucherData.setAcAmount(totalCustomer * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			thirdVoucherData.setDc(DC.C);
			thirdVoucherData.setAccount(ticket.getTo());
			thirdVoucherData.setUsdEquivalent(totalSupplier);
			thirdVoucherData.setLlEquivalent(totalSupplier * 1507.5);
			if (ticket.getTo().getCurrency() == Currency.DOLAR) {
				thirdVoucherData.setAcAmount(totalSupplier);
			} else {
				thirdVoucherData.setAcAmount(totalSupplier * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchaseSales = salesPurchaseFacade.getByTypeAndSP(SP.P, "Ticket");
			fourthVoucherData.setDc(DC.D);
			if (salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				Account account = salesPurchaseSales.getAccount();
				fourthVoucherData.setAccount(account);
				fourthVoucherData.setUsdEquivalent(totalSupplier);
				fourthVoucherData.setLlEquivalent(totalSupplier * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					fourthVoucherData.setAcAmount(totalSupplier);
				} else {
					fourthVoucherData.setAcAmount(totalSupplier * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null
					&& salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				voucherDatas.add(firstVoucherData);
				voucherDatas.add(secondVoucherData);
				voucherDatas.add(thirdVoucherData);
				voucherDatas.add(fourthVoucherData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return voucherDatas;
	}

	private List<VoucherData> initBillDatas(Bill bill) {
		List<VoucherData> voucherDatas = new ArrayList<VoucherData>();
		try {

			double totalCustomer = bill.getCusSelling();
			double totalSupplier = bill.getSupSelling();
			VoucherData firstVoucherData = new VoucherData();
			VoucherData secondVoucherData = new VoucherData();
			VoucherData thirdVoucherData = new VoucherData();
			VoucherData fourthVoucherData = new VoucherData();
			firstVoucherData.setEntryDescription(bill.getDescription());
			secondVoucherData.setEntryDescription(bill.getDescription());
			thirdVoucherData.setEntryDescription(bill.getDescription());
			fourthVoucherData.setEntryDescription(bill.getDescription());
			firstVoucherData.setItem(bill);
			secondVoucherData.setItem(bill);
			thirdVoucherData.setItem(bill);
			fourthVoucherData.setItem(bill);
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			firstVoucherData.setDc(DC.D);
			firstVoucherData.setAccount(bill.getFrom());
			firstVoucherData.setUsdEquivalent(totalCustomer);
			firstVoucherData.setLlEquivalent(totalCustomer * 1507.5);
			if (bill.getFrom().getCurrency() == Currency.DOLAR) {
				firstVoucherData.setAcAmount(totalCustomer);
			} else {
				firstVoucherData.setAcAmount(totalCustomer * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchasePurchase = salesPurchaseFacade.getByTypeAndSP(SP.S, "Bill");
			secondVoucherData.setDc(DC.C);
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null) {
				Account account = salesPurchasePurchase.getAccount();
				secondVoucherData.setAccount(account);
				secondVoucherData.setUsdEquivalent(totalCustomer);
				secondVoucherData.setLlEquivalent(totalCustomer * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					secondVoucherData.setAcAmount(totalCustomer);
				} else {
					secondVoucherData.setAcAmount(totalCustomer * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			thirdVoucherData.setDc(DC.C);
			thirdVoucherData.setAccount(bill.getTo());
			thirdVoucherData.setUsdEquivalent(totalSupplier);
			thirdVoucherData.setLlEquivalent(totalSupplier * 1507.5);
			if (bill.getTo().getCurrency() == Currency.DOLAR) {
				thirdVoucherData.setAcAmount(totalSupplier);
			} else {
				thirdVoucherData.setAcAmount(totalSupplier * 1507.5);
			}
			//////////////////////////////////////////////////
			SalesPurchase salesPurchaseSales = salesPurchaseFacade.getByTypeAndSP(SP.P, "Bill");
			fourthVoucherData.setDc(DC.D);
			if (salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				Account account = salesPurchaseSales.getAccount();
				fourthVoucherData.setAccount(account);
				fourthVoucherData.setUsdEquivalent(totalSupplier);
				fourthVoucherData.setLlEquivalent(totalSupplier * 1507.5);
				if (account.getCurrency() == Currency.DOLAR) {
					fourthVoucherData.setAcAmount(totalSupplier);
				} else {
					fourthVoucherData.setAcAmount(totalSupplier * 1507.5);
				}
			}
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			if (salesPurchasePurchase != null && salesPurchasePurchase.getAccount() != null
					&& salesPurchaseSales != null && salesPurchaseSales.getAccount() != null) {
				voucherDatas.add(firstVoucherData);
				voucherDatas.add(secondVoucherData);
				voucherDatas.add(thirdVoucherData);
				voucherDatas.add(fourthVoucherData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return voucherDatas;
	}

	public void importRvFromExcel(InputStream inputStream) {
		try {
			Workbook workbook = WorkbookFactory.create(inputStream);

			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();

			int rowCount = 0;
			int cellCount = 0;

			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {
				Voucher voucher = new Voucher();
				VoucherData oneVoucherData = new VoucherData();
				VoucherData twoVoucherData = new VoucherData();
				/////////////////////////////
				oneVoucherData.setDc(DC.C);
				twoVoucherData.setDc(DC.D);
				/////////////////////////////
				voucher.setVoucherType(VoucherType.RV);
				rowCount++;
				long maxVoucherNumber = voucherFacade.getMaxVoucherNumberByType(voucher.getVoucherType());
				voucher.setVoucherNumber(++maxVoucherNumber);
				Row row = rowIterator.next();
				int lastColumn = Math.max(row.getLastCellNum(), 0);
				if (rowCount > 1) {
					cellCount = 0;
					for (int cn = 0; cn < lastColumn; cn++) {
						Cell cell = row.getCell(cn, MissingCellPolicy.RETURN_BLANK_AS_NULL);
						cellCount++;
						String cellValue = dataFormatter.formatCellValue(cell);
						switch (cellCount) {
						case 1:
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yy");
							voucher.setVoucherDate(simpleDateFormat.parse(cellValue));
							break;
						case 2:
							Account customer = accountFacade.getAccountByNameAndDtype(cellValue, "Customer");
							if (customer != null) {
								oneVoucherData.setAccount((Customer) customer);
							} else {
								Account supplier = accountFacade.getAccountByNameAndDtype(cellValue, "Supplier");
								oneVoucherData.setAccount((Supplier) supplier);
							}
							break;
						case 3:
							Account cash = accountFacade.getAccountByNameAndDtype(cellValue, "Cash");
							if (cash != null) {
								twoVoucherData.setAccount((Cash) cash);
							} else {
								Account check = accountFacade.getAccountByNameAndDtype(cellValue, "Bank");
								twoVoucherData.setAccount((Bank) check);
								if (check == null) {
									Account general = accountFacade.getAccountByNameAndDtype(cellValue, "General");
									twoVoucherData.setAccount((General) general);
								}
							}
							if (twoVoucherData == null || twoVoucherData.getAccount() == null) {
								System.out.println("");
							}
							if (twoVoucherData.getAccount() == null) {
								System.out.println("asfasf");
							}
							break;
						case 4:
							double value = Double.parseDouble(cellValue);
							oneVoucherData.setAcAmount(value);
							if (oneVoucherData.getAccount().getCurrency() == Currency.DOLAR) {
								oneVoucherData.setUsdEquivalent(value);
								oneVoucherData.setLlEquivalent(value * 1507.5);
							} else {
								oneVoucherData.setUsdEquivalent(value / 1507.5);
								oneVoucherData.setLlEquivalent(value);
							}
							twoVoucherData.setAcAmount(value);
							if (twoVoucherData == null || twoVoucherData.getAccount() == null) {
								System.out.println("");
							}
							if (twoVoucherData.getAccount().getCurrency() == Currency.DOLAR) {
								twoVoucherData.setUsdEquivalent(value);
								twoVoucherData.setLlEquivalent(value * 1507.5);
							} else {
								twoVoucherData.setUsdEquivalent(value / 1507.5);
								twoVoucherData.setLlEquivalent(value);
							}
							break;
						case 5:
							if (cellValue != null && !cellValue.isEmpty()) {
								oneVoucherData.setEntryDescription(cellValue);
								twoVoucherData.setEntryDescription(cellValue);
							}
						default:
							break;
						}
					}
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					voucher.addNewVoucherData(oneVoucherData);
					voucher.addNewVoucherData(twoVoucherData);
					voucher = voucherFacade.save(voucher);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void importPvFromExcel(InputStream inputStream) {
		try {
			Workbook workbook = WorkbookFactory.create(inputStream);

			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();

			int rowCount = 0;
			int cellCount = 0;

			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {
				Voucher voucher = new Voucher();
				VoucherData oneVoucherData = new VoucherData();
				VoucherData twoVoucherData = new VoucherData();
				/////////////////////////////
				oneVoucherData.setDc(DC.D);
				twoVoucherData.setDc(DC.C);
				/////////////////////////////
				voucher.setVoucherType(VoucherType.PV);
				rowCount++;
				long maxVoucherNumber = voucherFacade.getMaxVoucherNumberByType(voucher.getVoucherType());
				voucher.setVoucherNumber(++maxVoucherNumber);
				Row row = rowIterator.next();
				int lastColumn = Math.max(row.getLastCellNum(), 0);
				if (rowCount > 1) {
					cellCount = 0;
					for (int cn = 0; cn < lastColumn; cn++) {
						Cell cell = row.getCell(cn, MissingCellPolicy.RETURN_BLANK_AS_NULL);
						cellCount++;
						String cellValue = dataFormatter.formatCellValue(cell);
						switch (cellCount) {
						case 1:
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yy");
							voucher.setVoucherDate(simpleDateFormat.parse(cellValue));
							break;
						case 2:
							Account customer = accountFacade.getAccountByNameAndDtype(cellValue, "Supplier");
							if (customer == null) {
								System.out.println("D7noun");
							}
							oneVoucherData.setAccount((Supplier) customer);
							break;
						case 3:
							Account cash = accountFacade.getAccountByNameAndDtype(cellValue, "Cash");
							if (cash != null) {
								twoVoucherData.setAccount((Cash) cash);
							} else {
								Account check = accountFacade.getAccountByNameAndDtype(cellValue, "Bank");
								twoVoucherData.setAccount((Bank) check);
								if (check == null) {
									Account general = accountFacade.getAccountByNameAndDtype(cellValue, "General");
									twoVoucherData.setAccount((General) general);
								}
							}
							if (twoVoucherData == null || twoVoucherData.getAccount() == null) {
								System.out.println("");
							}
							if (twoVoucherData.getAccount() == null) {
								System.out.println("asfasf");
							}
							break;
						case 4:
							double value = Double.parseDouble(cellValue);
							oneVoucherData.setAcAmount(value);
							if (oneVoucherData.getAccount().getCurrency() == Currency.DOLAR) {
								oneVoucherData.setUsdEquivalent(value);
								oneVoucherData.setLlEquivalent(value * 1507.5);
							} else {
								oneVoucherData.setUsdEquivalent(value / 1507.5);
								oneVoucherData.setLlEquivalent(value);
							}
							twoVoucherData.setAcAmount(value);
							if (twoVoucherData == null || twoVoucherData.getAccount() == null) {
								System.out.println("");
							}
							if (twoVoucherData.getAccount().getCurrency() == Currency.DOLAR) {
								twoVoucherData.setUsdEquivalent(value);
								twoVoucherData.setLlEquivalent(value * 1507.5);
							} else {
								twoVoucherData.setUsdEquivalent(value / 1507.5);
								twoVoucherData.setLlEquivalent(value);
							}
							break;
						case 5:
							if (cellValue != null && !cellValue.isEmpty()) {
								oneVoucherData.setEntryDescription(cellValue);
								twoVoucherData.setEntryDescription(cellValue);
							}

						default:
							break;
						}
					}
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					voucher.addNewVoucherData(oneVoucherData);
					voucher.addNewVoucherData(twoVoucherData);
					voucher = voucherFacade.save(voucher);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void importJvFromExcel(InputStream inputStream) {
		try {
			Workbook workbook = WorkbookFactory.create(inputStream);

			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();

			int rowCount = 0;
			int cellCount = 0;

			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {
				Voucher voucher = new Voucher();
				VoucherData oneVoucherData = new VoucherData();
				VoucherData twoVoucherData = new VoucherData();
				/////////////////////////////
				oneVoucherData.setDc(DC.D);
				twoVoucherData.setDc(DC.C);
				/////////////////////////////
				voucher.setVoucherType(VoucherType.JV);
				rowCount++;
				long maxVoucherNumber = voucherFacade.getMaxVoucherNumberByType(voucher.getVoucherType());
				voucher.setVoucherNumber(++maxVoucherNumber);
				Row row = rowIterator.next();
				int lastColumn = Math.max(row.getLastCellNum(), 0);
				if (rowCount > 1) {
					cellCount = 0;
					for (int cn = 0; cn < lastColumn; cn++) {
						Cell cell = row.getCell(cn, MissingCellPolicy.RETURN_BLANK_AS_NULL);
						cellCount++;
						String cellValue = dataFormatter.formatCellValue(cell);
						switch (cellCount) {
						case 1:
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yy");
							voucher.setVoucherDate(simpleDateFormat.parse(cellValue));
							break;
						case 2:
							Account general = accountFacade.getAccountByNameAndDtype(cellValue, "General");
							if (general != null) {
								oneVoucherData.setAccount((General) general);
								//System.out.println("D7noun");
							}else {
								Account check = accountFacade.getAccountByNameAndDtype(cellValue, "Bank");
								oneVoucherData.setAccount((Bank) check);
								if (check == null) {
									System.out.println("D7noun");
								}
								}
							//oneVoucherData.setAccount((General) general);
							break;
						case 3:
							Account cash = accountFacade.getAccountByNameAndDtype(cellValue, "Cash");
							if (cash != null) {
								twoVoucherData.setAccount((Cash) cash);
							} else {
								Account check = accountFacade.getAccountByNameAndDtype(cellValue, "Bank");
								twoVoucherData.setAccount((Bank) check);
								if (check == null) {
									Account g = accountFacade.getAccountByNameAndDtype(cellValue, "General");
									twoVoucherData.setAccount((General) g);
								}
							}
							if (twoVoucherData == null || twoVoucherData.getAccount() == null) {
								System.out.println("");
							}
							if (twoVoucherData.getAccount() == null) {
								System.out.println("asfasf");
							}
							break;
						case 4:
							double value = Double.parseDouble(cellValue);
							oneVoucherData.setAcAmount(value);
							if (oneVoucherData.getAccount().getCurrency() == Currency.DOLAR) {
								oneVoucherData.setUsdEquivalent(value);
								oneVoucherData.setLlEquivalent(value * 1507.5);
							} else {
								oneVoucherData.setUsdEquivalent(value / 1507.5);
								oneVoucherData.setLlEquivalent(value);
							}
							twoVoucherData.setAcAmount(value);
							if (twoVoucherData == null || twoVoucherData.getAccount() == null) {
								System.out.println("");
							}
							if (twoVoucherData.getAccount().getCurrency() == Currency.DOLAR) {
								twoVoucherData.setUsdEquivalent(value);
								twoVoucherData.setLlEquivalent(value * 1507.5);
							} else {
								twoVoucherData.setUsdEquivalent(value / 1507.5);
								twoVoucherData.setLlEquivalent(value);
							}
							break;
						case 5:
							if (cellValue != null && !cellValue.isEmpty()) {
								oneVoucherData.setEntryDescription(cellValue);
								twoVoucherData.setEntryDescription(cellValue);
							}

						default:
							break;
						}
					}
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					//////////////////////////////////////////////////
					voucher.addNewVoucherData(oneVoucherData);
					voucher.addNewVoucherData(twoVoucherData);
					voucher = voucherFacade.save(voucher);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
