package org.D7noun.service;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.D7noun.models.Voucher;
import org.D7noun.models.VoucherData;
import org.D7noun.models.accounts.Account;

@Stateless
@LocalBean
public class AccountReportService implements Serializable {

	private static final long serialVersionUID = -1L;

	@PersistenceContext(unitName = "Ticketing-pu")
	protected EntityManager em;

	@SuppressWarnings("unchecked")
	public List<VoucherData> findVouchersByAccountsAndDates(Account account, Date fromDate, Date toDate) {
		List<VoucherData> result = new ArrayList<VoucherData>();
		try {
			if (account != null) {
				Query query = this.em.createNamedQuery(Voucher.getVouchersByAccountAndDates, VoucherData.class);
				query.setParameter("account", account);
				if (fromDate == null) {
					fromDate = new Date();
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(fromDate);
					calendar.set(Calendar.DAY_OF_MONTH, 1);
					calendar.set(Calendar.MONTH, 0);
					fromDate = calendar.getTime();
				}
				if (toDate == null) {
					toDate = new Date();
				}
				query.setParameter("fromDate", formSimpleDate(fromDate));
				query.setParameter("toDate", formSimpleDate(toDate));
				result = query.getResultList();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountReportService.findVouchersByAccountsAndDates");
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<VoucherData> getVouchersByAccountBeforeDate(Account account, Date date) {
		List<VoucherData> result = new ArrayList<VoucherData>();
		try {
			if (account != null) {
				Query query = this.em.createNamedQuery(Voucher.getVouchersByAccountBeforeDate, VoucherData.class);
				query.setParameter("account", account);
				if (date == null) {
					date = new Date();
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(date);
					calendar.set(Calendar.DAY_OF_MONTH, 1);
					calendar.set(Calendar.MONTH, 0);
					date = calendar.getTime();
				}
				query.setParameter("date", formSimpleDate(date));
				result = query.getResultList();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountReportService.getVouchersByAccountBeforeDate");
		}
		return result;
	}

	private Date formSimpleDate(Date date) throws ParseException {
		if (date != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			String string = simpleDateFormat.format(date);
			return simpleDateFormat.parse(string);
		}
		return null;
	}

}