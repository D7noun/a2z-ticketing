package org.D7noun.abstraction;

import java.io.Serializable;
import java.util.Date;

import javax.faces.context.FacesContext;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@MappedSuperclass
public abstract class AbstractEntityWithoutId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdateDate;
	private String creator;
	private String editor;

	@Version
	@Column(name = "version")
	private int version;

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the lastUpdateDate
	 */
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	/**
	 * @param lastUpdateDate the lastUpdateDate to set
	 */
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	/**
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @param creator the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @return the editor
	 */
	public String getEditor() {
		return editor;
	}

	/**
	 * @param editor the editor to set
	 */
	public void setEditor(String editor) {
		this.editor = editor;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(int version) {
		this.version = version;
	}

	@PrePersist
	public void prePersist() {
		this.creationDate = new Date();
		this.lastUpdateDate = new Date();
		try {
			FacesContext faceContext = FacesContext.getCurrentInstance();
			if (faceContext != null) {
				String username = faceContext.getExternalContext().getRemoteUser();
				this.creator = username;
				this.editor = username;
			} else {
				this.creator = "Runtime";
				this.editor = "Runtime";
			}
		} catch (Exception e) {
			this.creator = "no user";
			this.editor = "no user";
			e.printStackTrace();
		}
	}

	@PreUpdate
	public void preUpdate() {
		this.lastUpdateDate = new Date();
		try {
			FacesContext faceContext = FacesContext.getCurrentInstance();
			if (faceContext != null) {
				String username = faceContext.getExternalContext().getRemoteUser();
				this.editor = username;
			} else {
				this.editor = "Runtime";
			}
		} catch (Exception e) {
			this.editor = "no user";
			e.printStackTrace();
		}
	}

}
