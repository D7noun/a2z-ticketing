package org.D7noun.dto;

import java.io.Serializable;
import java.text.DecimalFormat;

import org.D7noun.models.accounts.Customer;

public class CustomerGeneralReportDto implements Serializable {

	///////////////////////////
	public static final String getVoucherDatasBetweenDates = "CustomerGeneralQueryDto.getVoucherDatasBetweenDates";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Customer customer;
	private String DC;
	private double price;

	public CustomerGeneralReportDto() {
		super();
	}

	public CustomerGeneralReportDto(Customer customer, double price) {
		super();
		this.customer = customer;
		this.price = price;
		if (price < 0) {
			this.price = price * -1;
			this.DC = "CR";
		} else {
			this.price = price;
			this.DC = "DR";
		}
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the dC
	 */
	public String getDC() {
		return DC;
	}

	/**
	 * @param dC the dC to set
	 */
	public void setDC(String dC) {
		DC = dC;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(price));
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

}