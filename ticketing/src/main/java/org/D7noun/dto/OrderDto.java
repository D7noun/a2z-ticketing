package org.D7noun.dto;

import org.D7noun.models.VoucherData;

public class OrderDto {

	private String t;
	private String accountNo;
	private String accountName;
	private String description;
	private double acAmount;
	private double equivalentL;
	private double equivalentU;

	public OrderDto() {
		super();
	}

	public OrderDto(VoucherData voucherData) {
		this.t = voucherData.getDc().toString();
		this.accountNo = voucherData.getAccount().getAccountNumber();
		this.accountName = voucherData.getAccount().getName();
		this.description = voucherData.getEntryDescription();
		this.acAmount = voucherData.getAcAmount();
		this.equivalentL = voucherData.getLlEquivalent();
		this.equivalentU = voucherData.getUsdEquivalent();
	}

	public String getT() {
		return t;
	}

	public void setT(String t) {
		this.t = t;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getAcAmount() {
		return acAmount;
	}

	public void setAcAmount(double acAmount) {
		this.acAmount = acAmount;
	}

	public double getEquivalentL() {
		return equivalentL;
	}

	public void setEquivalentL(double equivalentL) {
		this.equivalentL = equivalentL;
	}

	public double getEquivalentU() {
		return equivalentU;
	}

	public void setEquivalentU(double equivalentU) {
		this.equivalentU = equivalentU;
	}

}
