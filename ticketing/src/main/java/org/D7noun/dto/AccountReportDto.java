package org.D7noun.dto;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

import org.D7noun.models.Voucher.VoucherType;
import org.D7noun.models.items.Item;

public class AccountReportDto implements Serializable {

	public enum AccountType {
		CUSTOMER, SUPPLIER, BANK, CASH, GENERAL;
	}

	public enum CRDR {
		CR, DR;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	private Item item;

	private long voucherNumber;
	private VoucherType voucherType;
	private Date voucherDate;
	private String voucherInfo;
	private String entryDescription;
	private double debit;
	private double credit;
	private double balance;
	private CRDR crdr;

	public CRDR getCrdr() {
		return crdr;
	}

	public void setCrdr(CRDR crdr) {
		this.crdr = crdr;
	}

	/**
	 * 
	 */
	public AccountReportDto() {
		super();
	}

	/**
	 * @return the voucherDate
	 */
	public Date getVoucherDate() {
		return voucherDate;
	}

	/**
	 * @param voucherDate the voucherDate to set
	 */
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	/**
	 * @return the voucherInfo
	 */
	public String getVoucherInfo() {
		return voucherInfo;
	}

	/**
	 * @param voucherInfo the voucherInfo to set
	 */
	public void setVoucherInfo(String voucherInfo) {
		this.voucherInfo = voucherInfo;
	}

	/**
	 * @return the entryDescription
	 */
	public String getEntryDescription() {
		return entryDescription;
	}

	/**
	 * @param entryDescription the entryDescription to set
	 */
	public void setEntryDescription(String entryDescription) {
		this.entryDescription = entryDescription;
	}

	/**
	 * @return the debit
	 */
	public double getDebit() {
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(debit));
	}

	/**
	 * @param debit the debit to set
	 */
	public void setDebit(double debit) {
		this.debit = debit;
	}

	/**
	 * @return the credit
	 */
	public double getCredit() {
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		if (credit > 0) {
			return Double.parseDouble(decimalFormat.format(credit));
		} else {
			return Double.parseDouble(decimalFormat.format(-1 * credit));
		}
	}

	/**
	 * @param credit the credit to set
	 */
	public void setCredit(double credit) {
		this.credit = credit;
	}

	/**
	 * @return the balance
	 */
	public double getBalance() {
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(balance));
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(double balance) {
		this.balance = balance;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the item
	 */
	public Item getItem() {
		return item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(Item item) {
		this.item = item;
	}

	/**
	 * @return the voucherNumber
	 */
	public long getVoucherNumber() {
		return voucherNumber;
	}

	/**
	 * @param voucherNumber the voucherNumber to set
	 */
	public void setVoucherNumber(long voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	/**
	 * @return the voucherType
	 */
	public VoucherType getVoucherType() {
		return voucherType;
	}

	/**
	 * @param voucherType the voucherType to set
	 */
	public void setVoucherType(VoucherType voucherType) {
		this.voucherType = voucherType;
	}

}