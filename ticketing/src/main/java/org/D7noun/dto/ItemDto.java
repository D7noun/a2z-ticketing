package org.D7noun.dto;

public class ItemDto {

	private String description;
	private String tax;
	private String amount;

	public ItemDto() {
		super();
	}

	public ItemDto(String description, String tax, String amount) {
		super();
		this.description = description;
		this.tax = tax;
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

}
