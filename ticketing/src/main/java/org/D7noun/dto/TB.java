package org.D7noun.dto;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Comparator;

public class TB implements Serializable, Comparator<TB> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String code;
	private String name;

	private double debit;
	private double credit;

	public TB() {
		super();
	}

	public TB(String code, String name, double debit, double credit) {
		super();
		this.code = code;
		this.name = name;
		this.debit = debit;
		this.credit = credit;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the debit
	 */
	public double getDebit() {
		if (debit < 0) {
			debit = debit * -1;
		}
		return debit;
	}

	public String getDebitStr() {
		if (debit < 0) {
			debit = debit * -1;
		}
		DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
		String result = decimalFormat.format(debit);
		if (result.equals(".00")) {
			return "0.00";
		}
		return result;
	}

//	public String getDebitStr() {
//		return String.format("%.02f", getDebit());
//	}

	/**
	 * @param debit the debit to set
	 */
	public void setDebit(double debit) {
		this.debit = debit;
	}

	/**
	 * @return the credit
	 */
	public double getCredit() {
		if (credit < 0) {
			credit = credit * -1;
		}
		return credit;
	}

	public String getCreditStr() {
		if (credit < 0) {
			credit = credit * -1;
		}
		DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
		String result = decimalFormat.format(credit);
		if (result.equals(".00")) {
			return "0.00";
		}
		return result;
	}

//	public String getCreditStr() {
//		return String.format("%.02f", getCredit());
//	}

	/**
	 * @param credit the credit to set
	 */
	public void setCredit(double credit) {
		this.credit = credit;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public double getBalance() {
		return debit - credit;
	}

	public String getBalanceStr() {
		DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
		double balance = getBalance();
		String result = decimalFormat.format(balance);
		if (balance <= 0) {
			result = decimalFormat.format(balance * -1);
		}
		if (result.equals(".00")) {
			return "0.00";
		}
		return result;
	}

//	public String getBalanceStr() {
//		if (getBalance() > 0) {
//			return String.format("%.02f", getBalance());
//		} else {
//			return String.format("%.02f", -1 * getBalance());
//		}
//	}

	public String getCrDr() {
		if (getBalance() >= 0) {
			return "DR";
		}
		return "CR";
	}

	@Override
	public int compare(TB o1, TB o2) {
		return o1.getCode().compareTo(o2.getCode());
	}

}
