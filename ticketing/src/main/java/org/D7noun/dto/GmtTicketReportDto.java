package org.D7noun.dto;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

public class GmtTicketReportDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nbr;
	private Date date;
	private double fairIn; // ten percent
	private double fairOut; // six percent
	private double fairPenalty;
	private double cancelationFeesIn;
	private double cancelationFeesOut;
	private double customerCommission;
	private double totalCommission;
	private double tax;
	private double commissionCutRefund;
	private double voidAmount;
	private double totalNet;
	private double myCommission;

	/**
	 * @return the nbr
	 */
	public String getNbr() {
		return nbr;
	}

	/**
	 * @param nbr the nbr to set
	 */
	public void setNbr(String nbr) {
		this.nbr = nbr;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the fairIn
	 */
	public double getFairIn() {
		return fairIn;
	}

	/**
	 * @param fairIn the fairIn to set
	 */
	public void setFairIn(double fairIn) {
		this.fairIn = fairIn;
	}

	/**
	 * @return the fairOut
	 */
	public double getFairOut() {
		return fairOut;
	}

	/**
	 * @param fairOut the fairOut to set
	 */
	public void setFairOut(double fairOut) {
		this.fairOut = fairOut;
	}

	/**
	 * @return the fairPenalty
	 */
	public double getFairPenalty() {
		return fairPenalty;
	}

	/**
	 * @param fairPenalty the fairPenalty to set
	 */
	public void setFairPenalty(double fairPenalty) {
		this.fairPenalty = fairPenalty;
	}

	/**
	 * @return the cancelationFeesIn
	 */
	public double getCancelationFeesIn() {
		return cancelationFeesIn;
	}

	/**
	 * @param cancelationFeesIn the cancelationFeesIn to set
	 */
	public void setCancelationFeesIn(double cancelationFeesIn) {
		this.cancelationFeesIn = cancelationFeesIn;
	}

	/**
	 * @return the cancelationFeesOut
	 */
	public double getCancelationFeesOut() {
		return cancelationFeesOut;
	}

	/**
	 * @param cancelationFeesOut the cancelationFeesOut to set
	 */
	public void setCancelationFeesOut(double cancelationFeesOut) {
		this.cancelationFeesOut = cancelationFeesOut;
	}

	/**
	 * @return the customerCommission
	 */
	public double getCustomerCommission() {
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(customerCommission));
	}

	/**
	 * @param customerCommission the customerCommission to set
	 */
	public void setCustomerCommission(double customerCommission) {
		this.customerCommission = customerCommission;
	}

	/**
	 * @return the totalCommission
	 */
	public double getTotalCommission() {
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(totalCommission));
	}

	/**
	 * @param totalCommission the totalCommission to set
	 */
	public void setTotalCommission(double totalCommission) {
		this.totalCommission = totalCommission;
	}

	/**
	 * @return the tax
	 */
	public double getTax() {
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(tax));
	}

	/**
	 * @param tax the tax to set
	 */
	public void setTax(double tax) {
		this.tax = tax;
	}

	/**
	 * @return the commissionCutRefund
	 */
	public double getCommissionCutRefund() {
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(commissionCutRefund));
	}

	/**
	 * @param commissionCutRefund the commissionCutRefund to set
	 */
	public void setCommissionCutRefund(double commissionCutRefund) {
		this.commissionCutRefund = commissionCutRefund;
	}

	/**
	 * @return the voidAmount
	 */
	public double getVoidAmount() {
		return voidAmount;
	}

	/**
	 * @param voidAmount the voidAmount to set
	 */
	public void setVoidAmount(double voidAmount) {
		this.voidAmount = voidAmount;
	}

	/**
	 * @return the totalNet
	 */
	public double getTotalNet() {
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(totalNet));
	}

	/**
	 * @param totalNet the totalNet to set
	 */
	public void setTotalNet(double totalNet) {
		this.totalNet = totalNet;
	}

	/**
	 * @return the myCommission
	 */
	public double getMyCommission() {
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(myCommission));
	}

	/**
	 * @param myCommission the myCommission to set
	 */
	public void setMyCommission(double myCommission) {
		this.myCommission = myCommission;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
