package org.D7noun.dto;

import java.io.Serializable;

public class TBReport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String code;
	private String name;
	private String debit;
	private String credit;
	private String balance;
	private String crdr;

	public TBReport() {
		super();
	}

	public TBReport(String code, String name, String debit, String credit, String balance, String crdr) {
		super();
		this.code = code;
		this.name = name;
		this.debit = debit;
		this.credit = credit;
		this.balance = balance;
		this.crdr = crdr;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDebit() {
		return debit;
	}

	public void setDebit(String debit) {
		this.debit = debit;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getCrdr() {
		return crdr;
	}

	public void setCrdr(String crdr) {
		this.crdr = crdr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
