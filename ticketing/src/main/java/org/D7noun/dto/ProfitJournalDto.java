package org.D7noun.dto;

import java.io.Serializable;

public class ProfitJournalDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int month;
	private double amount;

	public ProfitJournalDto() {
		super();
	}

	public ProfitJournalDto(int month, double amount) {
		super();
		this.month = month;
		this.amount = amount;
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
