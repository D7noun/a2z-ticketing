package org.D7noun.facades;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.AccountTree;

@Stateless
public class AccountTreeFacade extends AbstractFacade<AccountTree> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountTreeFacade() {
		super(AccountTree.class);
	}

	@SuppressWarnings("unchecked")
	public AccountTree getByCode(String code) {
		try {
			Query query = this.em.createNamedQuery(AccountTree.getByCode, AccountTree.class);
			query.setParameter("code", code);
			List<AccountTree> result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountTreeFacade.getByCode");
		}
		return null;
	}

	@Override
	public List<AccountTree> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AccountTree> cq = cb.createQuery(AccountTree.class);
		Root<AccountTree> root = cq.from(AccountTree.class);
		cq.orderBy(cb.asc(root.get("code")));
		cq.select(root);
		return em.createQuery(cq).getResultList();
	}
}
