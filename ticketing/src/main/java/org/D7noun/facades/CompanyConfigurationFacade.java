package org.D7noun.facades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.Query;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.CompanyConfiguration;
import org.D7noun.models.CompanyConfiguration.CompanyConfigurationTypes;

@Stateful
@LocalBean
public class CompanyConfigurationFacade extends AbstractFacade<CompanyConfiguration> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CompanyConfigurationFacade() {
		super(CompanyConfiguration.class);
	}

	@SuppressWarnings("unchecked")
	public String getValueByKey(CompanyConfigurationTypes key) {
		try {
			Query query = this.em.createNamedQuery(CompanyConfiguration.getValueByKey, String.class);
			query.setParameter("key", key);
			List<String> result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CompanyConfigurationFacade.getValueByKey");
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	public CompanyConfiguration getObjectByKey(CompanyConfigurationTypes key) {
		try {
			Query query = this.em.createNamedQuery(CompanyConfiguration.getObjectByKey, CompanyConfiguration.class);
			query.setParameter("key", key);
			List<CompanyConfiguration> result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CompanyConfigurationFacade.getObjectByKey");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<CompanyConfigurationTypes> getAllKeys() {
		List<CompanyConfigurationTypes> result = new ArrayList<CompanyConfigurationTypes>();
		try {
			Query query = this.em.createNamedQuery(CompanyConfiguration.getAllKeys, CompanyConfigurationTypes.class);
			result = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: CompanyConfigurationFacade.getAllKeys");
		}
		return result;
	}
}
