package org.D7noun.facades;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.Voucher;
import org.D7noun.models.Voucher.VoucherType;
import org.D7noun.models.VoucherData;
import org.D7noun.models.items.Item;

@Stateful
@LocalBean
public class VoucherFacade extends AbstractFacade<Voucher> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private AccountTreeFacade accountTreeFacade;

	public VoucherFacade() {
		super(Voucher.class);
	}

	public void removeVoucherData(Voucher voucher, VoucherData voucherData) {
		try {
			voucher.getVoucherDatas().remove(voucherData);
			if (this.em.contains(voucherData)) {
				this.em.remove(voucherData);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VoucherController.removeVoucherData");
		}
	}

	@SuppressWarnings("unchecked")
	public long getMaxVoucherNumberByType(VoucherType voucherType) {
		try {
			Query query = this.em.createNamedQuery(Voucher.getMaxVoucherNumberByType, Long.class);
			query.setParameter("voucherType", voucherType);
			List<Long> list = query.getResultList();
			if (list != null && !list.isEmpty()) {
				if (list.get(0) == null) {
					return 0;
				}
				return list.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VoucherFacade.getMaxVoucherNumberByType");
		}
		return 0;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Voucher saveVoucher(Voucher voucher, List<VoucherData> voucherDatas) {
		try {
			for (VoucherData voucherData : voucherDatas) {
				voucher.addNewVoucherData(voucherData);
			}
			return this.em.merge(voucher);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Voucher> getAllBetweenTwoDates(Date fromDate, Date toDate) {
		List<Voucher> result = new ArrayList<Voucher>();
		try {
			Query query = this.em.createNamedQuery(Voucher.getAllBetweenTwoDates, Voucher.class);
			query.setParameter("fromDate", formSimpleDate(fromDate));
			query.setParameter("toDate", formSimpleDate(toDate));
			result = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VoucherFacade.getAllBetweenTwoDates");
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Voucher> getAllBetweenTwoDatesByType(Date fromDate, Date toDate, VoucherType voucherType) {
		List<Voucher> result = new ArrayList<Voucher>();
		try {
			Query query = this.em.createNamedQuery(Voucher.getAllBetweenTwoDatesByType, Voucher.class);
			query.setParameter("fromDate", formSimpleDate(fromDate));
			query.setParameter("toDate", formSimpleDate(toDate));
			query.setParameter("voucherType", voucherType);
			result = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VoucherFacade.getAllBetweenTwoDatesByType");
		}
		return result;
	}

	private Date formSimpleDate(Date date) throws ParseException {
		if (date != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			String string = simpleDateFormat.format(date);
			return simpleDateFormat.parse(string);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public long getVoucherIdFromNumberAndType(long voucherNumber, VoucherType voucherType) {
		try {
			Query query = this.em.createNamedQuery(Voucher.getVoucherIdFromNumberAndType);
			query.setParameter("voucherNumber", voucherNumber);
			query.setParameter("voucherType", voucherType);
			List<Long> result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VoucherFacade.getVoucherIdFromNumberAndType");
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	public Voucher findVoucherByItem(Item item) {
		List<Voucher> result = new ArrayList<>();
		try {
			Query query = this.em.createNamedQuery(Voucher.findVoucherByItem, Voucher.class);
			query.setParameter("item", item);
			result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}