package org.D7noun.facades;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.accounts.Supplier;

@Stateful
@LocalBean
public class SupplierFacade extends AbstractFacade<Supplier> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SupplierFacade() {
		super(Supplier.class);
	}

}
