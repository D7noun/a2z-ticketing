package org.D7noun.facades;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.items.Hotel;

@Stateful
@LocalBean
public class HotelFacade extends AbstractFacade<Hotel> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HotelFacade() {
		super(Hotel.class);
	}

}
