package org.D7noun.facades;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.accounts.General;

@Stateful
@LocalBean
public class GeneralFacade extends AbstractFacade<General> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GeneralFacade() {
		super(General.class);
	}

}
