package org.D7noun.facades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.AccountConfiguration;

@Stateless
public class AccountConfigurationFacade extends AbstractFacade<AccountConfiguration> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountConfigurationFacade() {
		super(AccountConfiguration.class);
	}

	@SuppressWarnings("unchecked")
	public AccountConfiguration getAccountConfigurationByAccountType(String accountType) {
		try {
			Query query = this.em.createNamedQuery(AccountConfiguration.getAccountConfigurationByAccountType,
					AccountConfiguration.class);
			query.setParameter("accountType", accountType);
			List<AccountConfiguration> result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: ACcountConfigurationFacade.getAccountConfigurationByAccountType");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<String> getAllAccountConfigurationTypes() {
		List<String> result = new ArrayList<String>();
		try {
			Query query = this.em.createNamedQuery(AccountConfiguration.getAllAccountConfigurationTypes, String.class);
			result = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountConfigurationFacade.getAllAccountConfigurationTypes");
		}
		return result;
	}

	@Override
	public List<AccountConfiguration> findAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AccountConfiguration> cq = cb.createQuery(AccountConfiguration.class);
		Root<AccountConfiguration> root = cq.from(AccountConfiguration.class);
		cq.orderBy(cb.asc(root.get("accountType")));
		cq.select(root);
		return em.createQuery(cq).getResultList();
	}

}