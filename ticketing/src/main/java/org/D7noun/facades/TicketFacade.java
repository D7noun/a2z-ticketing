package org.D7noun.facades;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.items.Ticket;

@Stateful
@LocalBean
public class TicketFacade extends AbstractFacade<Ticket> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TicketFacade() {
		super(Ticket.class);
	}

	@SuppressWarnings("unchecked")
	public Ticket findTicketByTicketNumber(String ticketNumber) {
		List<Ticket> result = new ArrayList<Ticket>();
		try {
			Query query = this.em.createNamedQuery(Ticket.findTicketByTicketNumber, Ticket.class);
			query.setParameter("ticketNumber", ticketNumber);
			result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public Ticket findTicketByTicketNumberNotRefund(String ticketNumber) {
		List<Ticket> result = new ArrayList<Ticket>();
		try {
			Query query = this.em.createNamedQuery(Ticket.findTicketByTicketNumberNotRefund, Ticket.class);
			query.setParameter("ticketNumber", ticketNumber);
			result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Ticket> findBetweenTwoDates(Date fromDate, Date toDate) {
		List<Ticket> result = new ArrayList<Ticket>();
		try {
			if (fromDate == null) {
				fromDate = new Date();
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(fromDate);
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.set(Calendar.MONTH, 0);
				fromDate = calendar.getTime();
			}
			if (toDate == null) {
				toDate = new Date();
			}
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Ticket> cq = cb.createQuery(Ticket.class);
			Root<Ticket> root = cq.from(Ticket.class);
			cq.select(root).where(cb.lessThanOrEqualTo(root.<Date>get("date"), formSimpleDate(toDate)),
					cb.greaterThanOrEqualTo(root.<Date>get("date"), formSimpleDate(fromDate)));
			result = em.createQuery(cq).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private Date formSimpleDate(Date date) throws ParseException {
		if (date != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			String string = simpleDateFormat.format(date);
			return simpleDateFormat.parse(string);
		}
		return null;
	}

}
