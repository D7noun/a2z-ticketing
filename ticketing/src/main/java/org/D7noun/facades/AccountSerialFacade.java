package org.D7noun.facades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.Query;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.AccountSerial;
import org.D7noun.models.accounts.Account.Currency;

@Stateful
@LocalBean
public class AccountSerialFacade extends AbstractFacade<AccountSerial> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountSerialFacade() {
		super(AccountSerial.class);
	}

	@SuppressWarnings("unchecked")
	public AccountSerial getByAccountCodeAndCurrency(String accountCode, Currency currency) {
		List<AccountSerial> result = new ArrayList<AccountSerial>();
		try {
			Query query = this.em.createNamedQuery(AccountSerial.getByAccountCodeAndCurrency, AccountSerial.class);
			query.setParameter("accountCode", accountCode);
			query.setParameter("currency", currency);
			result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccoutnSerialFacade.getByAccountCodeAndCurrency");
		}
		return null;
	}
}