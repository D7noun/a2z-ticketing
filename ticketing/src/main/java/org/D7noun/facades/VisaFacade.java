package org.D7noun.facades;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.items.Visa;

@Stateful
@LocalBean
public class VisaFacade extends AbstractFacade<Visa> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VisaFacade() {
		super(Visa.class);
	}

}
