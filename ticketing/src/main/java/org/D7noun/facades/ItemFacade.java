package org.D7noun.facades;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.items.Item;

@Stateful
@LocalBean
public class ItemFacade extends AbstractFacade<Item> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ItemFacade() {
		super(Item.class);
	}

}
