package org.D7noun.facades;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.items.Bill;

@Stateful
@LocalBean
public class BillFacade extends AbstractFacade<Bill> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BillFacade() {
		super(Bill.class);
	}

}
