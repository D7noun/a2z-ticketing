package org.D7noun.facades;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.items.Transportation;

@Stateful
@LocalBean
public class TransportationFacade extends AbstractFacade<Transportation> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransportationFacade() {
		super(Transportation.class);
	}

}
