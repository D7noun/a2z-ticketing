package org.D7noun.facades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.D7noun.models.AccountConfiguration;
import org.D7noun.models.AccountConfiguration.AccountTypes;
import org.D7noun.models.CompanyConfiguration;
import org.D7noun.models.CompanyConfiguration.CompanyConfigurationTypes;
import org.D7noun.models.SalesPurchase;
import org.D7noun.models.SalesPurchase.SP;
import org.D7noun.models.SalesPurchase.SalesPurchaseType;

@Singleton
@Startup
public class SingeltonFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	private AccountConfigurationFacade accountConfigurationFacade;
	@EJB
	private SalesPurchaseFacade salesPurchaseFacade;
	@EJB
	private CompanyConfigurationFacade companyConfigurationFacade;

	@PostConstruct
	public void init() {
		try {
			List<String> accountConfigurationTypes = accountConfigurationFacade.getAllAccountConfigurationTypes();
			if (!accountConfigurationTypes.contains(AccountTypes.Customers.toString())) {
				AccountConfiguration accountConfiguration = new AccountConfiguration();
				accountConfiguration.setAccountType(AccountTypes.Customers.toString());
				accountConfigurationFacade.save(accountConfiguration);
			}
			if (!accountConfigurationTypes.contains(AccountTypes.Suppliers.toString())) {
				AccountConfiguration accountConfiguration = new AccountConfiguration();
				accountConfiguration.setAccountType(AccountTypes.Suppliers.toString());
				accountConfigurationFacade.save(accountConfiguration);
			}
			if (!accountConfigurationTypes.contains(AccountTypes.Cashs.toString())) {
				AccountConfiguration accountConfiguration = new AccountConfiguration();
				accountConfiguration.setAccountType(AccountTypes.Cashs.toString());
				accountConfigurationFacade.save(accountConfiguration);
			}
			if (!accountConfigurationTypes.contains(AccountTypes.Banks.toString())) {
				AccountConfiguration accountConfiguration = new AccountConfiguration();
				accountConfiguration.setAccountType(AccountTypes.Banks.toString());
				accountConfigurationFacade.save(accountConfiguration);
			}
			List<String> allSalesPurchase = salesPurchaseFacade.getAllTypes();
			if (!allSalesPurchase.contains(SalesPurchaseType.Ticket.toString())) {
				SalesPurchase salesPurchase1 = new SalesPurchase();
				salesPurchase1.setType(SalesPurchaseType.Ticket.toString());
				salesPurchase1.setSp(SP.S);
				salesPurchaseFacade.save(salesPurchase1);
				SalesPurchase salesPurchase2 = new SalesPurchase();
				salesPurchase2.setType(SalesPurchaseType.Ticket.toString());
				salesPurchase2.setSp(SP.P);
				salesPurchaseFacade.save(salesPurchase2);
			}
			if (!allSalesPurchase.contains(SalesPurchaseType.Food.toString())) {
				SalesPurchase salesPurchase1 = new SalesPurchase();
				salesPurchase1.setType(SalesPurchaseType.Food.toString());
				salesPurchase1.setSp(SP.S);
				salesPurchaseFacade.save(salesPurchase1);
				SalesPurchase salesPurchase2 = new SalesPurchase();
				salesPurchase2.setType(SalesPurchaseType.Food.toString());
				salesPurchase2.setSp(SP.P);
				salesPurchaseFacade.save(salesPurchase2);
			}
			if (!allSalesPurchase.contains(SalesPurchaseType.Hotel.toString())) {
				SalesPurchase salesPurchase1 = new SalesPurchase();
				salesPurchase1.setType(SalesPurchaseType.Hotel.toString());
				salesPurchase1.setSp(SP.S);
				salesPurchaseFacade.save(salesPurchase1);
				SalesPurchase salesPurchase2 = new SalesPurchase();
				salesPurchase2.setType(SalesPurchaseType.Hotel.toString());
				salesPurchase2.setSp(SP.P);
				salesPurchaseFacade.save(salesPurchase2);
			}
			if (!allSalesPurchase.contains(SalesPurchaseType.Transportation.toString())) {
				SalesPurchase salesPurchase1 = new SalesPurchase();
				salesPurchase1.setType(SalesPurchaseType.Transportation.toString());
				salesPurchase1.setSp(SP.S);
				salesPurchaseFacade.save(salesPurchase1);
				SalesPurchase salesPurchase2 = new SalesPurchase();
				salesPurchase2.setType(SalesPurchaseType.Transportation.toString());
				salesPurchase2.setSp(SP.P);
				salesPurchaseFacade.save(salesPurchase2);
			}
			if (!allSalesPurchase.contains(SalesPurchaseType.Visa.toString())) {
				SalesPurchase salesPurchase1 = new SalesPurchase();
				salesPurchase1.setType(SalesPurchaseType.Visa.toString());
				salesPurchase1.setSp(SP.S);
				salesPurchaseFacade.save(salesPurchase1);
				SalesPurchase salesPurchase2 = new SalesPurchase();
				salesPurchase2.setType(SalesPurchaseType.Visa.toString());
				salesPurchase2.setSp(SP.P);
				salesPurchaseFacade.save(salesPurchase2);
			}
			if (!allSalesPurchase.contains(SalesPurchaseType.Bill.toString())) {
				SalesPurchase salesPurchase1 = new SalesPurchase();
				salesPurchase1.setType(SalesPurchaseType.Bill.toString());
				salesPurchase1.setSp(SP.S);
				salesPurchaseFacade.save(salesPurchase1);
				SalesPurchase salesPurchase2 = new SalesPurchase();
				salesPurchase2.setType(SalesPurchaseType.Bill.toString());
				salesPurchase2.setSp(SP.P);
				salesPurchaseFacade.save(salesPurchase2);
			}
			/////////////////////////////
			/////////////////////////////
			/////////////////////////////
			List<CompanyConfiguration> companyConfigurations = companyConfigurationFacade.findAll();
			if (companyConfigurations == null || companyConfigurations.isEmpty()) {
				companyConfigurations = new ArrayList<CompanyConfiguration>();
				for (CompanyConfigurationTypes companyConfigurationTypes : CompanyConfigurationTypes.values()) {
					companyConfigurations.add(new CompanyConfiguration(companyConfigurationTypes));
				}
				companyConfigurationFacade.save(companyConfigurations);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: SingletonFacade.init");
		}
	}

}
