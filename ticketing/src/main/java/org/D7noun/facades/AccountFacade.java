package org.D7noun.facades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.Query;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.accounts.Account;
import org.D7noun.models.accounts.Account.Currency;

@Stateful
@LocalBean
public class AccountFacade extends AbstractFacade<Account> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountFacade() {
		super(Account.class);
	}

	@SuppressWarnings("unchecked")
	public long getMaxAccountIdByType(String dtype) {
		try {
			Query query = this.em.createNamedQuery(Account.getMaxAccountIdByType, Long.class);
			query.setParameter("dtype", dtype);
			List<Long> list = query.getResultList();
			if (list != null && !list.isEmpty()) {
				if (list.get(0) == null) {
					return 0;
				}
				return list.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountFacade.getMaxAccountIdByType");
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	public Account getAccountByAccountIdAndDtypeAndCurrency(String dtype, long accountId, Currency currency) {
		List<Account> result = new ArrayList<Account>();
		try {
			Query query = this.em.createNamedQuery(Account.getAccountByAccountIdAndDtypeAndCurrency, Account.class);
			query.setParameter("dtype", dtype);
			query.setParameter("accountId", accountId);
			query.setParameter("currency", currency);
			result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountFacade.getAccountByAccountIdAndDtypeAndCurrency");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public long getMaxAccountIdByCode(String code) {
		try {
			Query query = this.em.createNamedQuery(Account.getMaxAccountIdByCode, Long.class);
			query.setParameter("type", code);
			List<Long> list = query.getResultList();
			if (list != null && !list.isEmpty()) {
				if (list.get(0) == null) {
					return 0;
				}
				return list.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountFacade.getMaxAccountIdByCode");
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	public Account getAccountByAccountIdAndCodeAndCurrency(String code, long accountId, Currency currency) {
		List<Account> result = new ArrayList<Account>();
		try {
			Query query = this.em.createNamedQuery(Account.getAccountByAccountIdAndCodeAndCurrency, Account.class);
			query.setParameter("type", code);
			query.setParameter("accountId", accountId);
			query.setParameter("currency", currency);
			result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: AccountFacade.getAccountByAccountIdAndCodeAndCurrency");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public Account getAccountByNameAndDtype(String name, String dtype) {
		List<Account> result = new ArrayList<Account>();
		try {
			Query query = this.em.createNamedQuery(Account.getAccountByNameAndDtype, Account.class);
			query.setParameter("name", name);
			query.setParameter("dtype", dtype);
			result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}