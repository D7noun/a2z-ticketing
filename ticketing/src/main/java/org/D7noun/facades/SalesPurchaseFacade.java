package org.D7noun.facades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.Query;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.SalesPurchase;
import org.D7noun.models.SalesPurchase.SP;

@Stateful
@LocalBean
public class SalesPurchaseFacade extends AbstractFacade<SalesPurchase> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SalesPurchaseFacade() {
		super(SalesPurchase.class);
	}

	@SuppressWarnings("unchecked")
	public List<SalesPurchase> getByType(SP sp) {
		List<SalesPurchase> result = new ArrayList<SalesPurchase>();
		try {
			Query query = this.em.createNamedQuery(SalesPurchase.getBySP, SalesPurchase.class);
			query.setParameter("sp", sp);
			result = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: SalesPurchase.getBytype");
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public SalesPurchase getByTypeAndSP(SP sp, String type) {
		List<SalesPurchase> result = new ArrayList<SalesPurchase>();
		try {
			Query query = this.em.createNamedQuery(SalesPurchase.getByTypeAndSP, SalesPurchase.class);
			query.setParameter("sp", sp);
			query.setParameter("type", type);
			result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: SalesPurchase.getByTypeAndSP");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<String> getAllTypes() {
		List<String> result = new ArrayList<String>();
		try {
			Query query = this.em.createNamedQuery(SalesPurchase.getAllTypes, String.class);
			result = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: SalesPurchase.getAllTypes");
		}
		return result;
	}

}
