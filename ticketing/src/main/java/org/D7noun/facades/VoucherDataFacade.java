package org.D7noun.facades;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.VoucherData;
import org.D7noun.models.items.Item;

@Stateful
@LocalBean
public class VoucherDataFacade extends AbstractFacade<VoucherData> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VoucherDataFacade() {
		super(VoucherData.class);
	}

	public List<VoucherData> findVoucherDataByItem(Item item) {
		List<VoucherData> result = new ArrayList<VoucherData>();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<VoucherData> cq = cb.createQuery(VoucherData.class);
			Root<VoucherData> root = cq.from(VoucherData.class);
			cq.select(root).where(cb.equal(root.get("item"), item));
			result = em.createQuery(cq).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<VoucherData> findBetweenTwoDates(Date fromDate, Date toDate) {
		List<VoucherData> result = new ArrayList<VoucherData>();
		try {
			Query query = this.em.createNamedQuery(VoucherData.findBetweenTwoDates, VoucherData.class);
			if (fromDate == null) {
				fromDate = new Date();
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(fromDate);
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.set(Calendar.MONTH, 0);
				fromDate = calendar.getTime();
			}
			if (toDate == null) {
				toDate = new Date();
			}
			query.setParameter("fromDate", formSimpleDate(fromDate));
			query.setParameter("toDate", formSimpleDate(toDate));
			result = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VoucherDataFacade.findBetweenTwoDates");
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<VoucherData> findBetweenTwoDatesAndTwoType(String fromType, String toType, Date fromDate, Date toDate) {
		List<VoucherData> result = new ArrayList<VoucherData>();
		try {
			Query query = this.em.createNamedQuery(VoucherData.findBetweenTwoDatesAndTwoTypes, VoucherData.class);
			if (fromDate == null) {
				fromDate = new Date();
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(fromDate);
				calendar.set(Calendar.DAY_OF_MONTH, 1);
				calendar.set(Calendar.MONTH, 0);
				fromDate = calendar.getTime();
			}
			if (toDate == null) {
				toDate = new Date();
			}
			query.setParameter("fromDate", formSimpleDate(fromDate));
			query.setParameter("toDate", formSimpleDate(toDate));
			query.setParameter("fromType", fromType);
			query.setParameter("toType", toType);
			result = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("D7noun: VoucherDataFacade.findBetweenTwoDatesAndTwoType");
		}
		return result;
	}

	public List<VoucherData> getAllByYear(String year) {
		List<VoucherData> result = new ArrayList<VoucherData>();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<VoucherData> cq = cb.createQuery(VoucherData.class);
			Root<VoucherData> root = cq.from(VoucherData.class);
			cq.select(root).where(
					cb.equal(cb.function("TO_CHAR", String.class, root.get("voucherDate"), cb.literal("yyyy")), year));
			result = em.createQuery(cq).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private Date formSimpleDate(Date date) throws ParseException {
		if (date != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			String string = simpleDateFormat.format(date);
			return simpleDateFormat.parse(string);
		}
		return null;
	}

}