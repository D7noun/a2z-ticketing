package org.D7noun.facades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.CurrencyArchive;

@Stateless
public class CurrencyConfigurationFacade extends AbstractFacade<CurrencyArchive> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CurrencyConfigurationFacade() {
		super(CurrencyArchive.class);
	}

	@SuppressWarnings("unchecked")
	public int getLiraByDate(Date date) {
		try {
			Query query = this.em.createNamedQuery(CurrencyArchive.getLiraByDate, Integer.class);
			query.setParameter("date", date);
			List<Integer> result = query.getResultList();
			if (result != null && !result.isEmpty()) {
				return result.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}