package org.D7noun.facades;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.items.Food;

@Stateful
@LocalBean
public class FoodFacade extends AbstractFacade<Food> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FoodFacade() {
		super(Food.class);
	}

}
