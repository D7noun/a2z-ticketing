package org.D7noun.facades;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.accounts.Customer;

@Stateful
@LocalBean
public class CustomerFacade extends AbstractFacade<Customer> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerFacade() {
		super(Customer.class);
	}

}
