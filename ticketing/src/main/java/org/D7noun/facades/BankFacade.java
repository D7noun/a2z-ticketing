package org.D7noun.facades;

import java.io.Serializable;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import org.D7noun.abstraction.AbstractFacade;
import org.D7noun.models.accounts.Bank;

@Stateful
@LocalBean
public class BankFacade extends AbstractFacade<Bank> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BankFacade() {
		super(Bank.class);
	}

}
