package org.D7noun.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.D7noun.abstraction.AbstractEntity;

@Entity
@NamedQueries({
		@NamedQuery(name = AccountTree.getByCode, query = "SELECT ac FROM AccountTree ac WHERE ac.code = :code") })
public class AccountTree extends AbstractEntity implements Serializable {

	public static final String getByCode = "AccountTree.getByCode";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String englishName;
	private String arabicName;
	private String code;

	public String getDisplayName() {
		String display = code;
		if (englishName != null && !englishName.isEmpty()) {
			display += " // " + englishName;
		}
		if (arabicName != null && !arabicName.isEmpty()) {
			display += " // " + arabicName;
		}
		return display;
	}

	public String getName() {
		String display = "";
		if (englishName != null && !englishName.isEmpty()) {
			display += englishName;
			if (arabicName != null && !arabicName.isEmpty()) {
				display += " // " + arabicName;
			}
		} else {
			if (arabicName != null && !arabicName.isEmpty()) {
				display += arabicName;
			}
		}
		return display;
	}

	/**
	 * @return the englishName
	 */
	public String getEnglishName() {
		return englishName;
	}

	/**
	 * @param englishName the englishName to set
	 */
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	/**
	 * @return the arabicName
	 */
	public String getArabicName() {
		return arabicName;
	}

	/**
	 * @param arabicName the arabicName to set
	 */
	public void setArabicName(String arabicName) {
		this.arabicName = arabicName;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the getbycode
	 */
	public static String getGetbycode() {
		return getByCode;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
