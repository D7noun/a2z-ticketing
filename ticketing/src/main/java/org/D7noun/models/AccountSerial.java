package org.D7noun.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.D7noun.abstraction.AbstractEntity;
import org.D7noun.models.accounts.Account.Currency;

@Entity
@NamedQueries({
		@NamedQuery(name = AccountSerial.getByAccountCodeAndCurrency, query = "SELECT accs FROM AccountSerial accs WHERE accs.accountCode = :accountCode AND accs.currency = :currency") })
public class AccountSerial extends AbstractEntity {

	public static final String getByAccountCodeAndCurrency = "getByAccountCodeAndCurrency";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Enumerated(EnumType.STRING)
	private Currency currency;
	private String accountCode;
	private long value;

	/**
	 * @return the accountCode
	 */
	public String getAccountCode() {
		return accountCode;
	}

	/**
	 * @param accountCode the accountCode to set
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the currency
	 */
	public Currency getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	/**
	 * @return the value
	 */
	public long getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(long value) {
		this.value = value;
	}

	/**
	 * @return the getbyaccountcodeandcurrency
	 */
	public static String getGetbyaccountcodeandcurrency() {
		return getByAccountCodeAndCurrency;
	}

}
