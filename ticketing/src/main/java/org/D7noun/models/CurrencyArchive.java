package org.D7noun.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.D7noun.abstraction.AbstractEntity;

@Entity
@NamedQueries(@NamedQuery(name = CurrencyArchive.getLiraByDate, query = "SELECT ca.liraPerDolar FROM CurrencyArchive ca WHERE ca.date = :date"))
public class CurrencyArchive extends AbstractEntity implements Serializable {

	public static final String getLiraByDate = "getLiraByDate";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	private Date date;
	private int liraPerDolar;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the liraPerDolar
	 */
	public int getLiraPerDolar() {
		return liraPerDolar;
	}

	/**
	 * @param liraPerDolar the liraPerDolar to set
	 */
	public void setLiraPerDolar(int liraPerDolar) {
		this.liraPerDolar = liraPerDolar;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}