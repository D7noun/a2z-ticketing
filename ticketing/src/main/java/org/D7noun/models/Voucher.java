package org.D7noun.models;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.D7noun.abstraction.AbstractEntity;
import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.dto.CustomerGeneralReportDto;
import org.D7noun.models.VoucherData.DC;
import org.D7noun.models.items.Item;

@Entity
@NamedQueries({
		@NamedQuery(name = Voucher.getMaxVoucherNumberByType, query = "SELECT max(v.voucherNumber) FROM Voucher v WHERE v.voucherType = :voucherType"),
		@NamedQuery(name = Voucher.getVouchersByAccountAndDates, query = "SELECT vd FROM VoucherData vd WHERE vd.account = :account AND vd.voucherDate >= :fromDate AND vd.voucherDate <= :toDate ORDER BY vd.creationDate asc"),
		@NamedQuery(name = Voucher.getVouchersByAccountBeforeDate, query = "SELECT vd FROM VoucherData vd WHERE vd.account = :account AND vd.voucherDate < :date"),
		@NamedQuery(name = CustomerGeneralReportDto.getVoucherDatasBetweenDates, query = "SELECT vd FROM VoucherData vd WHERE vd.voucherDate >= :fromDate AND vd.voucherDate <= :toDate AND vd.account.dtype = 'Customer'"),
		@NamedQuery(name = Voucher.getAllVoucherDatasBetweenTwoDates, query = "SELECT vd FROM VoucherData vd WHERE vd.voucherDate >= :fromDate AND vd.voucherDate <= :toDate"),
		@NamedQuery(name = Voucher.getAllBetweenTwoDates, query = "SELECT v FROM Voucher v WHERE v.voucherDate >= :fromDate AND v.voucherDate <= :toDate ORDER BY v.voucherDate"),
		@NamedQuery(name = Voucher.getAllBetweenTwoDatesByType, query = "SELECT v FROM Voucher v WHERE v.voucherDate >= :fromDate AND v.voucherDate <= :toDate AND v.voucherType = :voucherType ORDER BY v.voucherDate"),
		@NamedQuery(name = Voucher.getVoucherIdFromNumberAndType, query = "SELECT v.id FROM Voucher v WHERE v.voucherNumber = :voucherNumber AND v.voucherType = :voucherType"),
		@NamedQuery(name = Voucher.findVoucherByItem, query = "SELECT i.voucher FROM Item i WHERE i = :item ") })
public class Voucher extends AbstractEntity implements Serializable {

	public static final String getMaxVoucherNumberByType = "Voucher.getMaxVoucherNumberByType";
	public static final String getVouchersByAccountAndDates = "Voucher.getVouchersByAccountAndDates";
	public static final String getVouchersByAccountBeforeDate = "Voucher.getVouchersByAccountBeforeDate";
	public static final String getAllVoucherDatasBetweenTwoDates = "Voucher.getAllVoucherDatasBetweenTwoDates";
	public static final String getAllBetweenTwoDates = "Voucher.getAllBetweenTwoDates";
	public static final String getAllBetweenTwoDatesByType = "Voucher.getAllBetweenTwoDatesByType";
	public static final String getVoucherIdFromNumberAndType = "Voucher.getVoucherIdFromNumberAndType";
	public static final String findVoucherByItem = "Voucher.findVoucherByItem";

	public enum VoucherType {
		JV("JV"), PV("PV"), RV("RV"), RE("RE"), TI("TI"), WB("WB"), VS("VS"), FS("FS"), HS("HS"), TS("TS");

		private String value;

		private VoucherType(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Enumerated(EnumType.STRING)
	private VoucherType voucherType;

	private long voucherNumber;

	@OrderBy
	@Temporal(TemporalType.DATE)
	private Date voucherDate;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "voucher_id")
	private List<Item> items = new ArrayList<Item>();

	@OneToMany(mappedBy = "voucher", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private List<VoucherData> voucherDatas = new ArrayList<VoucherData>();

	/**
	 * 
	 * Voucher Data
	 * 
	 */

	public String voucherCode() {
		if (voucherNumber != 0 && voucherType != null) {
			return voucherType + " " + voucherNumber;
		}
		return GeneralUtility.getResourceBundleString("check_voucher_saved");
	}

	public double totalDrAC() {
		double sum = 0;
		for (VoucherData voucherData : voucherDatas) {
			if (voucherData.getDc() == DC.D) {
				sum += voucherData.getAcAmount();
			}
		}
		return sum;
	}

	public double totalCrAC() {
		double sum = 0;
		for (VoucherData voucherData : voucherDatas) {
			if (voucherData.getDc() == DC.C) {
				sum += voucherData.getAcAmount();
			}
		}
		return sum;
	}

	public double balanceAC() {
		return totalDrAC() - totalCrAC();
	}

	/////////////////////////

	public double totalDrLL() {
		double sum = 0;
		for (VoucherData voucherData : voucherDatas) {
			if (voucherData.getDc() == DC.D) {
				sum += voucherData.getLlEquivalent();
			}
		}
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(sum));
	}

	public double totalCrLL() {
		double sum = 0;
		for (VoucherData voucherData : voucherDatas) {
			if (voucherData.getDc() == DC.C) {
				sum += voucherData.getLlEquivalent();
			}
		}
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(sum));
	}

	public double balanceLL() {
		return totalDrLL() - totalCrLL();
	}

	/////////////////////////

	public double totalDrUSD() {
		double sum = 0;
		for (VoucherData voucherData : voucherDatas) {
			if (voucherData.getDc() == DC.D) {
				sum += voucherData.getUsdEquivalent();
			}
		}
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(sum));
	}

	public double totalCrUSD() {
		double sum = 0;
		for (VoucherData voucherData : voucherDatas) {
			if (voucherData.getDc() == DC.C) {
				sum += voucherData.getUsdEquivalent();
			}
		}
		DecimalFormat decimalFormat = new DecimalFormat("##.00");
		return Double.parseDouble(decimalFormat.format(sum));
	}

	public double balanceUSD() {
		return totalDrUSD() - totalCrUSD();
	}
	///////////////////

	public Voucher addNewVoucherData(VoucherData voucherData) {
		if (voucherData != null) {
			voucherData.setVoucher(this);
			voucherData.setVoucherDate(this.voucherDate);
			voucherData.setVoucherNumber(this.voucherNumber);
			voucherData.setVoucherType(this.voucherType);
			this.getVoucherDatas().add(voucherData);
		}
		return this;
	}

	public Voucher deleteVoucherData(List<VoucherData> voucherDatas) {
		if (voucherDatas != null) {
			this.getVoucherDatas().removeAll(voucherDatas);
		}
		return this;
	}

	public Voucher addNewItem(Item item) {
		if (item != null) {
			item.setVoucherDate(this.getVoucherDate());
			item.setVoucherNumber(this.voucherNumber);
			item.setVoucherType(this.voucherType);
			item.setVoucher(this);
			this.getItems().add(item);
		}
		return this;
	}

	public Voucher removeItem(Item item) {
		if (item != null) {
			item.setVoucher(null);
		}
		return this;
	}

	/**
	 * 
	 * D7noun: GETTERS&SETTERS
	 * 
	 */

	/**
	 * @return the voucherType
	 */
	public VoucherType getVoucherType() {
		return voucherType;
	}

	/**
	 * @param voucherType the voucherType to set
	 */
	public void setVoucherType(VoucherType voucherType) {
		this.voucherType = voucherType;
	}

	/**
	 * @return the voucherDate
	 */
	public Date getVoucherDate() {
		return voucherDate;
	}

	/**
	 * @param voucherDate the voucherDate to set
	 */
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the voucherDatas
	 */
	public List<VoucherData> getVoucherDatas() {
		return voucherDatas;
	}

	/**
	 * @param voucherDatas the voucherDatas to set
	 */
	public void setVoucherDatas(List<VoucherData> voucherDatas) {
		this.voucherDatas = voucherDatas;
	}

	/**
	 * @return the voucherNumber
	 */
	public long getVoucherNumber() {
		return voucherNumber;
	}

	/**
	 * @param voucherNumber the voucherNumber to set
	 */
	public void setVoucherNumber(long voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getTicketNumberByVoucher() {
		for (VoucherData voucherData : voucherDatas) {
			if (voucherData.getEntryDescription() == null
					&& voucherData.getEntryDescription().contains("Ticket Number: ")) {
				return voucherData.getEntryDescription().split("Ticket Number: ")[1];
			}
		}
		return "";
	}

	@PrePersist
	@PreUpdate
	public void preupdate() {
		this.getVoucherDatas().forEach(vd -> {
			vd.setVoucherDate(voucherDate);
			vd.setVoucherNumber(voucherNumber);
			vd.setVoucherType(voucherType);
		});
	}
}