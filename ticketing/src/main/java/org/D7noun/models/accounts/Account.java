package org.D7noun.models.accounts;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.D7noun.abstraction.AbstractEntity;
import org.D7noun.abstraction.GeneralUtility;

@Entity
@DiscriminatorColumn(name = "dtype", discriminatorType = DiscriminatorType.STRING)
@NamedQueries({
		@NamedQuery(name = Account.getMaxAccountIdByType, query = "SELECT max(a.accountId) FROM Account a WHERE a.dtype = :dtype"),
		@NamedQuery(name = Account.getAccountByAccountIdAndDtypeAndCurrency, query = "SELECT a FROM Account a WHERE a.dtype = :dtype AND a.accountId = :accountId AND a.currency = :currency"),
		@NamedQuery(name = Account.getMaxAccountIdByCode, query = "SELECT max(a.accountId) FROM Account a WHERE a.type = :type"),
		@NamedQuery(name = Account.getAccountByAccountIdAndCodeAndCurrency, query = "SELECT a FROM Account a WHERE a.type = :type AND a.accountId = :accountId AND a.currency = :currency"),
		@NamedQuery(name = Account.getAccountByNameAndDtype, query = "SELECT a FROM Account a WHERE a.name = :name AND a.dtype = :dtype") })
public class Account extends AbstractEntity implements Serializable {

	public static final String getMaxAccountIdByType = "Account.getMaxAccountIdByType";
	public static final String getAccountByAccountIdAndDtypeAndCurrency = "Account.getAccountByAccountIdAndDtypeAndCurrency";
	public static final String getMaxAccountIdByCode = "Account.getMaxAccountIdByCode";
	public static final String getAccountByAccountIdAndCodeAndCurrency = "Account.getAccountByAccountIdAndCodeAndCurrency";
	public static final String getAccountByNameAndDtype = "Account.getAccountByNameAndDtype";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum Currency {
		DOLAR, LIRA;
	}

	private long accountId;

	private String name;

	@Column(name = "dtype", insertable = false, updatable = false)
	private String dtype;

	private String type;

	@Enumerated(EnumType.STRING)
	private Currency currency;

	private String notes;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAccountNumber() {
		if (id != null && id > 0) {
			String accountNumber = type + "/" + accountId;
			if (currency == Currency.DOLAR) {
				accountNumber += "/" + 1;
			} else {
				accountNumber += "/" + 0;
			}
			return accountNumber;
		} else {
			return GeneralUtility.getResourceBundleString("cannot_get_account_number");
		}
	}

	public String displayName() {
		String n = "";
		if (name != null) {
			n = name + " -- ";
		}
		return n + getAccountNumber();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the currency
	 */
	public Currency getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the accountId
	 */
	public long getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the dtype
	 */
	public String getDtype() {
		return dtype;
	}

	/**
	 * @param dtype the dtype to set
	 */
	public void setDtype(String dtype) {
		this.dtype = dtype;
	}

}