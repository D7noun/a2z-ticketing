package org.D7noun.models.accounts;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Supplier")
public class Supplier extends Account {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int tva;
	private String financialNumber;
	private String title;
	private String email;
	private String phoneNumber;

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the tva
	 */
	public int getTva() {
		return tva;
	}

	/**
	 * @param tva the tva to set
	 */
	public void setTva(int tva) {
		this.tva = tva;
	}

	/**
	 * @return the financialNumber
	 */
	public String getFinancialNumber() {
		return financialNumber;
	}

	/**
	 * @param financialNumber the financialNumber to set
	 */
	public void setFinancialNumber(String financialNumber) {
		this.financialNumber = financialNumber;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

}
