package org.D7noun.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.D7noun.abstraction.AbstractEntity;
import org.D7noun.abstraction.GeneralUtility;
import org.D7noun.models.accounts.Account;

@Entity
@NamedQueries({ @NamedQuery(name = SalesPurchase.getBySP, query = "SELECT sp FROM SalesPurchase sp WHERE sp.sp = :sp"),
		@NamedQuery(name = SalesPurchase.getByTypeAndSP, query = "SELECT sp from SalesPurchase sp WhERE sp.sp = :sp AND sp.type = :type"),
		@NamedQuery(name = SalesPurchase.getAllTypes, query = "SELECT sp.type FROM SalesPurchase sp") })
public class SalesPurchase extends AbstractEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String getBySP = "SalesPurchase.getBySP";
	public static final String getByTypeAndSP = "SalesPurchase.getByTypeAndSP";
	public static final String getAllTypes = "SalesPurchase.getAllTypes";

	public enum SalesPurchaseType {
		Ticket("ticket"), Hotel("hotel"), Food("food"), Transportation("transportation"), Visa("visa"), Bill("bill");

		private String type;

		private SalesPurchaseType(String type) {
			this.type = type;
		}

		public String getValue() {
			return type;
		}
	}

	public enum SP {
		S, P;
	}

	private String type;

	@ManyToOne
	private Account account;

	@Enumerated(EnumType.STRING)
	private SP sp;

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the sp
	 */
	public SP getSp() {
		return sp;
	}

	/**
	 * @param sp the sp to set
	 */
	public void setSp(SP sp) {
		this.sp = sp;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String typeDisplayName() {
		String s = "";
		s += GeneralUtility.getResourceBundleString(type);
		if (sp == SP.P) {
			s += " " + GeneralUtility.getResourceBundleString("purchase");
		} else {
			s += " " + GeneralUtility.getResourceBundleString("sales");
		}
		return s;
	}

	/**
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

}
