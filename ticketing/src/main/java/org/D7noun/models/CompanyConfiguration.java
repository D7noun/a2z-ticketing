package org.D7noun.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
		@NamedQuery(name = CompanyConfiguration.getValueByKey, query = "SELECT gc.value FROM CompanyConfiguration gc WHERE gc.key = :key"),
		@NamedQuery(name = CompanyConfiguration.getObjectByKey, query = "SELECT gc FROM CompanyConfiguration gc WHERE gc.key = :key"),
		@NamedQuery(name = CompanyConfiguration.getAllKeys, query = "SELECT gc.key FROM CompanyConfiguration gc ORDER BY gc.key asc") })
public class CompanyConfiguration implements Serializable {

	public enum CompanyConfigurationTypes {
		CompanyFolderName("Company Folder Name"), CompanyDescription("Company Description"),
		CompanyTitleInForms("Company Title In Forms"), AddressLineOne("Address Line One"),
		AddressLineTwo("Address Line Two"), AddressLineThree("Address Line Three"),
		VATRegistrationNo("VAT Registration No"), VATRegistrationDate("VAT Registration Date"),
		VATDecree140Date("VAT Decree 140 Date"), MOFRegistrationNo("MOF Registration No"), Language("Language"),
		AccountingYear("Accounting Year"), AccessPassword("Access Password");

		private String value;

		private CompanyConfigurationTypes(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

	}

	public static final String getValueByKey = "CompanyConfiguration.getValueByKey";
	public static final String getObjectByKey = "CompanyConfiguration.getObjectByKey";
	public static final String getAllKeys = "CompanyConfiguration.getAllKeys";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "id", updatable = false, nullable = false)
	protected Long id;

	@Enumerated(EnumType.STRING)
	private CompanyConfigurationTypes key;
	private String value;

	public CompanyConfiguration() {
		super();
	}

	public CompanyConfiguration(CompanyConfigurationTypes key) {
		this.key = key;
		this.value = "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CompanyConfigurationTypes getKey() {
		return key;
	}

	public void setKey(CompanyConfigurationTypes key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
