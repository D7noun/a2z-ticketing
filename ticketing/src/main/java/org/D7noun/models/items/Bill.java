package org.D7noun.models.items;

import javax.persistence.Entity;

@Entity
public class Bill extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long number;
	private double fair;
	private double tax;
	private double profit;
	private double clearance;
	private double supSelling;
	private double cusSelling;

	public Bill() {
		setDtype("Bill");
	}

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

	public double getFair() {
		return fair;
	}

	public void setFair(double fair) {
		this.fair = fair;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getProfit() {
		return profit;
	}

	public void setProfit(double profit) {
		this.profit = profit;
	}

	public double getClearance() {
		return clearance;
	}

	public void setClearance(double clearance) {
		this.clearance = clearance;
	}

	public double getSupSelling() {
		return supSelling;
	}

	public void setSupSelling(double supSelling) {
		this.supSelling = supSelling;
	}

	public double getCusSelling() {
		return cusSelling;
	}

	public void setCusSelling(double cusSelling) {
		this.cusSelling = cusSelling;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
