package org.D7noun.models.items;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({
		@NamedQuery(name = Ticket.findTicketByTicketNumber, query = "SELECT t FROM Ticket t WHERE t.ticketNumber = :ticketNumber"),
		@NamedQuery(name = Ticket.findTicketByTicketNumberNotRefund, query = "SELECT t FROM Ticket t WHERE t.ticketNumber = :ticketNumber and t.cancelationFees =0") })
public class Ticket extends Item {

	public static final String findTicketByTicketNumber = "ticket.getTicketByTicketNumber";
	public static final String findTicketByTicketNumberNotRefund = "ticket.findTicketByTicketNumberNotRefund";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String ticketNumber;

	private String route;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateOfFlight;

	@OrderBy
	private int ticketPerVoucher;
	private String passenger;
	private double fair;
	private double tax;
	private double profit;
	private double insurance;
	private double cancelationFees;
	private double selling; // fair + tax + profit + insurance
	private double total; // selling - discount

	public Ticket() {
		setDtype("Ticket");
	}

	public String displayName() {
		String display = "";
		if (getVoucherType() == null) {
			return "";
		}
		display += getVoucherType() + " " + getVoucherNumber() + "( " + ticketPerVoucher + " )";
		return display;
	}

	/**
	 * @return the ticketNumber
	 */
	public String getTicketNumber() {
		return ticketNumber;
	}

	/**
	 * @param ticketNumber the ticketNumber to set
	 */
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	/**
	 * @return the route
	 */
	public String getRoute() {
		return route;
	}

	/**
	 * @param route the route to set
	 */
	public void setRoute(String route) {
		this.route = route;
	}

	/**
	 * @return the dateOfFlight
	 */
	public Date getDateOfFlight() {
		return dateOfFlight;
	}

	/**
	 * @param dateOfFlight the dateOfFlight to set
	 */
	public void setDateOfFlight(Date dateOfFlight) {
		this.dateOfFlight = dateOfFlight;
	}

	/**
	 * @return the fair
	 */
	public double getFair() {
		return fair;
	}

	/**
	 * @param fair the fair to set
	 */
	public void setFair(double fair) {
		this.fair = fair;
	}

	/**
	 * @return the tax
	 */
	public double getTax() {
		return tax;
	}

	/**
	 * @param tax the tax to set
	 */
	public void setTax(double tax) {
		this.tax = tax;
	}

	/**
	 * @return the profit
	 */
	public double getProfit() {
		return profit;
	}

	/**
	 * @param profit the profit to set
	 */
	public void setProfit(double profit) {
		this.profit = profit;
	}

	/**
	 * @return the insurance
	 */
	public double getInsurance() {
		return insurance;
	}

	/**
	 * @param insurance the insurance to set
	 */
	public void setInsurance(double insurance) {
		this.insurance = insurance;
	}

	/**
	 * @return the selling
	 */
	public double getSelling() {
		return selling;
	}

	/**
	 * @param selling the selling to set
	 */
	public void setSelling(double selling) {
		this.selling = selling;
	}

	/**
	 * @return the total
	 */
	public double getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(double total) {
		this.total = total;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the passenger
	 */
	public String getPassenger() {
		return passenger;
	}

	/**
	 * @param passenger the passenger to set
	 */
	public void setPassenger(String passenger) {
		this.passenger = passenger;
	}

	/**
	 * @return the ticketPerVoucher
	 */
	public int getTicketPerVoucher() {
		return ticketPerVoucher;
	}

	/**
	 * @param ticketPerVoucher the ticketPerVoucher to set
	 */
	public void setTicketPerVoucher(int ticketPerVoucher) {
		this.ticketPerVoucher = ticketPerVoucher;
	}

	public double getCancelationFees() {
		return cancelationFees;
	}

	public void setCancelationFees(double cancelationFees) {
		this.cancelationFees = cancelationFees;
	}

}
