package org.D7noun.models.items;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.D7noun.abstraction.AbstractEntity;
import org.D7noun.models.Voucher;
import org.D7noun.models.Voucher.VoucherType;
import org.D7noun.models.accounts.Customer;
import org.D7noun.models.accounts.Supplier;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Item extends AbstractEntity implements Serializable {

	public static final String getByVoucherData = "Item.getByVoucherData";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	private Customer from;
	@ManyToOne
	private Supplier to;
	/////////////////////

	private String description;
	/////////////////////
	@Temporal(TemporalType.DATE)
	private Date date;
	/////////////////////

	private String dtype;
	private String code;

	private double discountPercentage;
	private double discountAmount;

	private String notes;
	private String salesMan; // logged in user

//	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinColumn(name = "item_id")
//	private List<VoucherData> voucherDatas = new ArrayList<VoucherData>();

	////////////////////////////////
	////////////////////////////////
	////////////////////////////////
	////////////////////////////////
	@ManyToOne(fetch = FetchType.LAZY)
	private Voucher voucher;
	// These Fields are set when adding new voucher data. they help with lazy
	// loading of VoucherData
	@Temporal(TemporalType.DATE)
	private Date voucherDate;
	private long voucherNumber;
	@Enumerated(EnumType.STRING)
	private VoucherType voucherType;

	////////////////// //////////////
	////////////////////////////////
	////////////////////////////////
	////////////////////////////////

	public String displayName() {
		String display = "";
		if (getVoucherType() == null) {
			return "";
		}
		display += getVoucherType() + " " + getVoucherNumber();
		return display;
	}

//	public Item addNewVoucherData(VoucherData voucherData) {
//		if (voucherData != null) {
//			this.getVoucherDatas().add(voucherData);
//		}
//		return this;
//	}
//
//	public Item removeVoucherData(List<VoucherData> voucherDatas) {
//		if (voucherDatas != null) {
//			this.getVoucherDatas().removeAll(voucherDatas);
//		}
//		return this;
//	}

	/**
	 * @return the from
	 */
	public Customer getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Customer from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Supplier getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Supplier to) {
		this.to = to;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the salesMan
	 */
	public String getSalesMan() {
		return salesMan;
	}

	/**
	 * @param salesMan the salesMan to set
	 */
	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the voucher
	 */
	public Voucher getVoucher() {
		return voucher;
	}

	/**
	 * @param voucher the voucher to set
	 */
	public void setVoucher(Voucher voucher) {
		this.voucher = voucher;
	}

	/**
	 * @return the dtype
	 */
	public String getDtype() {
		return dtype;
	}

	/**
	 * @param dtype the dtype to set
	 */
	public void setDtype(String dtype) {
		this.dtype = dtype;
	}

	/**
	 * @return the discountPercentage
	 */
	public double getDiscountPercentage() {
		return discountPercentage;
	}

	/**
	 * @param discountPercentage the discountPercentage to set
	 */
	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	/**
	 * @return the discountAmount
	 */
	public double getDiscountAmount() {
		return discountAmount;
	}

	/**
	 * @param discountAmount the discountAmount to set
	 */
	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	/**
	 * @return the voucherDate
	 */
	public Date getVoucherDate() {
		return voucherDate;
	}

	/**
	 * @param voucherDate the voucherDate to set
	 */
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	/**
	 * @return the voucherNumber
	 */
	public long getVoucherNumber() {
		return voucherNumber;
	}

	/**
	 * @param voucherNumber the voucherNumber to set
	 */
	public void setVoucherNumber(long voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	/**
	 * @return the voucherType
	 */
	public VoucherType getVoucherType() {
		return voucherType;
	}

	/**
	 * @param voucherType the voucherType to set
	 */
	public void setVoucherType(VoucherType voucherType) {
		this.voucherType = voucherType;
	}

	/**
	 * @return the getbyvoucherdata
	 */
	public static String getGetbyvoucherdata() {
		return getByVoucherData;
	}

	public String getVoucherInfo() {
		return voucherType + " " + voucherNumber;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

//	public List<VoucherData> getVoucherDatas() {
//		return voucherDatas;
//	}
//
//	public void setVoucherDatas(List<VoucherData> voucherDatas) {
//		this.voucherDatas = voucherDatas;
//	}

}