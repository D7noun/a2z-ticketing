package org.D7noun.models.items;

import javax.persistence.Entity;

@Entity
public class Visa extends Item {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double cost;
	private double profit;
	private double tax;
	private int quantity;
	private double selling; // cost + profit + tax
	private double total; // selling * quantity

	public Visa() {
		setDtype("Visa");
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the tax
	 */
	public double getTax() {
		return tax;
	}

	/**
	 * @param tax the tax to set
	 */
	public void setTax(double tax) {
		this.tax = tax;
	}

	/**
	 * @return the cost
	 */
	public double getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(double cost) {
		this.cost = cost;
	}

	/**
	 * @return the profit
	 */
	public double getProfit() {
		return profit;
	}

	/**
	 * @param profit the profit to set
	 */
	public void setProfit(double profit) {
		this.profit = profit;
	}

	/**
	 * @return the selling
	 */
	public double getSelling() {
		return selling;
	}

	/**
	 * @param selling the selling to set
	 */
	public void setSelling(double selling) {
		this.selling = selling;
	}

	/**
	 * @return the total
	 */
	public double getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(double total) {
		this.total = total;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
