package org.D7noun.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.D7noun.abstraction.AbstractEntity;
import org.D7noun.models.Voucher.VoucherType;
import org.D7noun.models.accounts.Account;
import org.D7noun.models.items.Item;

@Entity
@NamedQueries({
		@NamedQuery(name = VoucherData.findVoucherDataByItem, query = "SELECT vd FROM VoucherData vd WHERE vd.item = :item"),
		@NamedQuery(name = VoucherData.findBetweenTwoDates, query = "SELECT vd FROM VoucherData vd WHERE vd.voucherDate >= :fromDate AND vd.voucherDate <= :toDate"),
		@NamedQuery(name = VoucherData.findBetweenTwoDatesAndTwoTypes, query = "SELECT vd FROM VoucherData vd WHERE vd.voucherDate >= :fromDate AND vd.voucherDate <= :toDate AND vd.account.type >= :fromType AND vd.account.type <= :toType ") })
public class VoucherData extends AbstractEntity implements Serializable {

	public static final String findVoucherDataByItem = "VoucherData.findVoucherDataByItem";
	public static final String findBetweenTwoDates = "VoucherData.findBetweenTwoDates";
	public static final String findBetweenTwoDatesAndTwoTypes = "VoucherData.findBetweenTwoDatesAndTwoTypes";

	public enum DC {
		D, C;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VoucherData() {
		super();
		account = new Account();
	}

	@Enumerated(EnumType.STRING)
	private DC dc;

	@ManyToOne
	private Account account;

	private String entryDescription;

	private double acAmount;
	private double llEquivalent;
	private double usdEquivalent;

	////////////////////////////////
	////////////////////////////////
	////////////////////////////////
	////////////////////////////////
	@ManyToOne(fetch = FetchType.LAZY)
	private Voucher voucher;
	@ManyToOne
	private Item item;
	// These Fields are set when adding new voucher data. they help with lazy
	// loading of VoucherData
	@Temporal(TemporalType.DATE)
	private Date voucherDate;
	private long voucherNumber;
	@Enumerated(EnumType.STRING)
	private VoucherType voucherType;

	////////////////////////////////
	////////////////////////////////
	////////////////////////////////
	////////////////////////////////
	/**
	 * @return the dc
	 */
	public DC getDc() {
		if (dc == null) {
			if (voucher != null && voucher.getVoucherDatas().size() == 1) {
				if (voucher.getVoucherType() == VoucherType.PV) {
					return DC.D;
				}
				if (voucher.getVoucherType() == VoucherType.RV) {
					return DC.C;
				}
			}
		}
		return dc;
	}

	/**
	 * @param dc the dc to set
	 */
	public void setDc(DC dc) {
		this.dc = dc;
	}

	/**
	 * @return the entryDescription
	 */
	public String getEntryDescription() {
		return entryDescription;
	}

	/**
	 * @param entryDescription the entryDescription to set
	 */
	public void setEntryDescription(String entryDescription) {
		this.entryDescription = entryDescription;
	}

	/**
	 * @return the acAmount
	 */
	public double getAcAmount() {
		return acAmount;
	}

	/**
	 * @param acAmount the acAmount to set
	 */
	public void setAcAmount(double acAmount) {
		this.acAmount = acAmount;
	}

	/**
	 * @return the llEquivalent
	 */
	public double getLlEquivalent() {
		return llEquivalent;
	}

	/**
	 * @param llEquivalent the llEquivalent to set
	 */
	public void setLlEquivalent(double llEquivalent) {
		this.llEquivalent = llEquivalent;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the usdEquivalent
	 */
	public double getUsdEquivalent() {
		return usdEquivalent;
	}

	/**
	 * @param usdEquivalent the usdEquivalent to set
	 */
	public void setUsdEquivalent(double usdEquivalent) {
		this.usdEquivalent = usdEquivalent;
	}

	/**
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * @return the voucher
	 */
	public Voucher getVoucher() {
		return voucher;
	}

	/**
	 * @param voucher the voucher to set
	 */
	public void setVoucher(Voucher voucher) {
		this.voucher = voucher;
	}

	/**
	 * @return the voucherDate
	 */
	public Date getVoucherDate() {
		return voucherDate;
	}

	/**
	 * @param voucherDate the voucherDate to set
	 */
	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	/**
	 * @return the voucherNumber
	 */
	public long getVoucherNumber() {
		return voucherNumber;
	}

	/**
	 * @param voucherNumber the voucherNumber to set
	 */
	public void setVoucherNumber(long voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	/**
	 * @return the voucherType
	 */
	public VoucherType getVoucherType() {
		return voucherType;
	}

	/**
	 * @param voucherType the voucherType to set
	 */
	public void setVoucherType(VoucherType voucherType) {
		this.voucherType = voucherType;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

}
