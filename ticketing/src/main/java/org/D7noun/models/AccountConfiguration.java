package org.D7noun.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import org.D7noun.abstraction.AbstractEntity;

@Entity
@NamedQueries({
		@NamedQuery(name = AccountConfiguration.getAccountConfigurationByAccountType, query = "SELECT ac FROM AccountConfiguration ac WHERE ac.accountType = :accountType"),
		@NamedQuery(name = AccountConfiguration.getAllAccountConfigurationTypes, query = "SELECT ac.accountType FROM AccountConfiguration ac ORDER BY ac.accountType asc") })
public class AccountConfiguration extends AbstractEntity implements Serializable {

	public enum AccountTypes {
		Customers("Customers"), Suppliers("Suppliers"), Cashs("Cashs"), Banks("Banks");

		private String accountType;

		private AccountTypes(String accountType) {
			this.accountType = accountType;
		}

		public String getValue() {
			return accountType;
		}
	}

	public static final String getAccountConfigurationByAccountType = "getAccountConfigurationByAccountType";
	public static final String getAllAccountConfigurationTypes = "getAllAccountConfigurationTypes";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String accountType;

	@OneToOne
	private AccountTree accountTree;

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the accountType
	 */
	public String getAccountType() {
		return accountType;
	}

	/**
	 * @return the accountTree
	 */
	public AccountTree getAccountTree() {
		return accountTree;
	}

	/**
	 * @param accountType the accountType to set
	 */
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	/**
	 * @param accountTree the accountTree to set
	 */
	public void setAccountTree(AccountTree accountTree) {
		this.accountTree = accountTree;
	}

	/**
	 * @return the getaccountconfigurationbyaccounttype
	 */
	public static String getGetaccountconfigurationbyaccounttype() {
		return getAccountConfigurationByAccountType;
	}

	/**
	 * @return the getallaccountconfigurationtypes
	 */
	public static String getGetallaccountconfigurationtypes() {
		return getAllAccountConfigurationTypes;
	}

	public String displayName() {
		String displayName = "";
		if (accountType != null) {
			displayName += accountType + " : ";
		}
		if (accountTree != null) {
			displayName += accountTree.getDisplayName();
		}
		return displayName;
	}

}
