$(document).ready(function() {
	enterTabEvent();
})

var generateMessage = function(title, text, severity) {
	// ptype : warning , success , error , info

	var type;
	if (severity.indexOf("WARN") != -1) {
		type = "warning";
	} else if (severity.indexOf("ERROR") != -1
			|| severity.indexOf("FATAL") != -1) {
		type = "error";
	} else if (severity.indexOf("INFO") != -1) {
		type = "info";
	} else {
		type = "success";
	}
	if (title == text)
		title = "";

	$.toast({
		heading : title,
		text : text,
		icon : type,
		showHideTransition : 'slide',
		hideAfter : 8000
	})
}

function enterTabEvent() {
	$('input').on("keypress", function(e) {
		/* ENTER PRESSED */
		if (e.keyCode == 13) {
			/* FOCUS ELEMENT */
			var inputs = $(this).parents("form").eq(0).find(":input");
			var idx = inputs.index(this);

			if (idx == inputs.length - 1) {
				inputs[0].select()
			} else {
				inputs[idx + 1].focus(); // handles submit buttons
				inputs[idx + 1].select();
			}
			return false;
		}
	});
}